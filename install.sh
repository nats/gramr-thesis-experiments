#!/usr/bin/env bash
# Installs gramr and s2tagger. If you cannot use docker to run the experiments
# for any reason, use this script to install the software directly on your
# machine.
#
# Prerequisites:
#   - git
#   - python3 >= 3.7
#   - sbt

set -e

DEPSDIR=deps
if [ -e ${DEPSDIR} ]
then
    echo Directory ${DEPSDIR} already exists.
    read -p "Delete and reinstall all dependencies? "
    if [[ ! $REPLY =~ ^[Yy] ]]
    then
        exit 1
    fi
    rm -rf ${DEPSDIR}
fi

mkdir -p ${DEPSDIR}

./install-s2tagger.sh ${DEPSDIR}
./install-gramr.sh ${DEPSDIR}
./install-elmo.sh ${DEPSDIR}

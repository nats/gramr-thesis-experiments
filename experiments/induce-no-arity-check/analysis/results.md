# Results

Disabling the arity check increases grammar induction time by a factor of ~3, which is still feasible.

The number of (unfiltered) templates roughly doubles. After filtering, this difference largely disappears
(10% increase).

There is no notable difference between the full and as grammars.

The increase in coverage is modest (2 percentage points).

## Condition: arityCheck=true grammar=full

Total run time including EM: ~ 4.5h

```
split-1 took 26 minutes, 28 seconds and 163 milliseconds
Average best token-level coverage: 0.8404426535118121
Average best split quality       : 0.6282311094032881
split-2 took 53 seconds and 581 milliseconds

Lexicalized lexical entry count: 144411
Delexicalized template count: 1240728
Delexicalized lexeme count  : 474904

Filtered template count: 19435
Filtered lexeme count  : 10350
```

## Condition: arityCheck=false grammar=full

Total run time including EM: ~ 12.5h

```
split-1 took 2 hours, 14 minutes, 25 seconds and 833 milliseconds
Average best token-level coverage: 0.8597584737120386
Average best split quality       : 0.6407330351318212
split-2 took 2 minutes, 29 seconds and 36 milliseconds

Lexicalized lexical entry count: 299870
Delexicalized template count: 2369193
Delexicalized lexeme count  : 493477

Filtered template count: 21396
Filtered lexeme count  : 10303
```

## Condition: arityCheck=true grammar=as

```
21:28:44.903 [main] INFO  gramr.util.progressbar.ProgressBar - split-1 took 32 minutes, 17 seconds and 236 milliseconds
21:28:48.357 [main] INFO  script - Average best token-level coverage: 0.8404508933833587
21:28:48.358 [main] INFO  script - Average best split quality       : 0.6285018722288471
21:30:00.076 [main] INFO  gramr.util.progressbar.ProgressBar - split-2 took 1 minute, 11 seconds and 701 milliseconds

21:30:00.641 [main] INFO  script - Lexicalized lexical entry count: 145266
21:30:20.722 [main] INFO  script - Delexicalized template count: 1242364
21:30:20.743 [main] INFO  script - Delexicalized lexeme count  : 474774

02:42:21.544 [main] INFO  script - Filtered template count: 19459
02:42:21.547 [main] INFO  script - Filtered lexeme count  : 10355
```

## Condition: arityCheck=false grammar=as

```
23:16:08.368 [main] INFO  gramr.util.progressbar.ProgressBar - split-1 took 2 hours, 19 minutes, 28 seconds and 77 milliseconds
23:16:11.660 [main] INFO  script - Average best token-level coverage: 0.8596608844414394
23:16:11.661 [main] INFO  script - Average best split quality       : 0.6401445415085281
23:18:56.889 [main] INFO  gramr.util.progressbar.ProgressBar - split-2 took 2 minutes, 45 seconds and 221 milliseconds

23:18:58.498 [main] INFO  script - Lexicalized lexical entry count: 311161
23:19:37.257 [main] INFO  script - Delexicalized template count: 2444831
23:19:37.278 [main] INFO  script - Delexicalized lexeme count  : 492426

11:14:31.703 [main] INFO  script - Filtered template count: 21228
11:14:31.706 [main] INFO  script - Filtered lexeme count  : 10287
```

# Induce: No Arity Check

Is grammar induction 

These scripts run grammar induction without arity checking (e.g. without enforcing the restriction that induced grammar
items have an arity less than or equal to their syntactic category).

This generates a whole lot more noise, but also potentially improves coverage, as there may be cases where the syntactic
arity is inappropriate. As such, it presents first a computational challenge and second one of noise reduction.

The experiment is primarily intended to answer the questions:

1. Does the first splitting pass complete in a manageable time (e.g. <=24 hours)?
2. Is the second splitting pass fast enough to be run repeatedly in EM?
3. Does the EM phase filter the lexicon sufficiently for us to assume that the excess noise has been addressed?

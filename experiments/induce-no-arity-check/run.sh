#!/usr/bin/env bash
# Performs the supertagging step of the pipeline.
# Trains the supertagger and then predicts tags on the train, dev and test sets.
set -e

ARITY_CHECK=$1

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../.."

GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
DATA=${ROOT_DIR}/data
SCRIPTS_PATH=${SCRIPT_DIR}/../scripts
PATH_SUFFIX="${ARITY_CHECK}"
OUTPUT_DIR="${SCRIPT_DIR}/output/arityCheck.${PATH_SUFFIX}"

java -Xmx48g -jar ${GRAMR} ${SCRIPTS_PATH}/largeScaleInduction.sc \
    --arityCheck ${ARITY_CHECK} \
    --corpus ${DATA}/processed/training/amr-release-1.0-training-proxy.txt \
    --output ${OUTPUT_DIR} \
    --em false

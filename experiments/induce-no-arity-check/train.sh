#!/usr/bin/env bash
set -e

ARITY_CHECK=$1
GRAMMAR=$2

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../.."

GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
DATA=${ROOT_DIR}/data
SCRIPTS_PATH=${SCRIPT_DIR}/../scripts
OUTPUT_DIR=${SCRIPT_DIR}/output/${ARITY_CHECK}/${GRAMMAR}
LEX_DIR=${ROOT_DIR}/data/lexicon/no-arity-check/${ARITY_CHECK}/${GRAMMAR}
TAG_DIR=${ROOT_DIR}/data/tags/no-arity-check/${ARITY_CHECK}/${GRAMMAR}

if [[ $ARITY_CHECK == "ac" ]]; then
  echo "Arity Check On"
  ARITY_CHECK_BOOL=true
else
  echo "Arity Check Off"
  ARITY_CHECK_BOOL=false
fi
echo "Grammar: ${GRAMMAR}"

java -Xmx48g -jar ${GRAMR} ${SCRIPTS_PATH}/parsing.sc \
    --grammar ${GRAMMAR} \
    --arityCheck ${ARITY_CHECK_BOOL} \
    --train ${DATA}/processed/training/amr-release-1.0-training-proxy.txt \
    --validate ${DATA}/processed/dev/amr-release-1.0-dev-proxy.txt \
    --output ${OUTPUT_DIR} \
    --lexicon ${LEX_DIR} \
    --tags ${TAG_DIR} \
    --beam 15 \
    --oracleBeam 20

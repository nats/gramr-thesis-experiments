#!/usr/bin/env bash
# Generate slurm scripts for individual experimental conditions

set -e

COUNTS=(true false)

for COUNT in ${COUNTS[@]}; do
  ARITY_CHECK=$COUNT envsubst '$ARITY_CHECK' < template_slurm_job.sh > run_slurm_$COUNT.sh
done

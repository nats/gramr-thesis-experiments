#!/usr/bin/env bash
# Generate slurm scripts for individual experimental conditions

set -e

function gen {
  size=$1
  layers=$2
  SIZE=${size} LAYERS=$layers envsubst '$SIZE $LAYERS' < template_slurm_job.sh > run_slurm_size=${size}.layers=${layers}.sh
}

gen 50 1
gen 100 1
gen 200 1
gen 50 3
gen 100 3
gen 200 3
gen 50 7
gen 100 7
gen 200 7

#!/usr/bin/env bash
#
# Usage:
#   run.sh SIZE LAYERS
set -euo pipefail

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../../.."
venv="${ROOT_DIR}/deps/s2tagger-venv"
data="${ROOT_DIR}/data"
lexicon="${SCRIPT_DIR}/../output/induce"

size=$1
layers=$2

output="${SCRIPT_DIR}/output/size=${size}.layers=${layers}"
config="${output}/config.yaml"

mkdir -p ${output}
DATA="$(realpath --relative-to=${output} ${data})" \
LEXICON="$(realpath --relative-to=${output} ${lexicon})" \
SIZE=$size LAYERS=$layers \
envsubst '$DATA $LEXICON $SIZE $LAYERS' < "${SCRIPT_DIR}/config.template.yaml" > "${config}"


source "${venv}/bin/activate"

echo ========================================
echo Condition: size=$size layers=$layers
echo ========================================
echo


# Train
echo ----------------------------------------
echo Training
echo ----------------------------------------
python -m s2tagger train -c ${config} -m ${output}/train --val 0.1
# Predict val + test
echo ----------------------------------------
echo Predicting val
echo ----------------------------------------
python -m s2tagger predict -c ${config} -m ${output}/train --data-set val -o ${output}/val_tags_pred.txt
echo ----------------------------------------
echo Predicting val
echo ----------------------------------------
python -m s2tagger predict -c ${config} -m ${output}/train --data-set test -o ${output}/test_tags_pred.txt
# Jackknife
echo ----------------------------------------
echo Jackknifing / predicting train
echo ----------------------------------------
python -m s2tagger jackknife -c ${config} -m ${output}/jackknife -o ${output}/train_tags_pred.txt --splits 4

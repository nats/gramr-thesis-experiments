#!/usr/bin/env bash
set -euo pipefail

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../.."

GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
DATA=${ROOT_DIR}/data
SCRIPTS_PATH=${SCRIPT_DIR}/../scripts
OUTPUT_DIR="${SCRIPT_DIR}/output/induce"
LEXICON_DIR="${DATA}/lexicon/parsing"

mkdir -p "${OUTPUT_DIR}"

java -Xmx48g -jar ${GRAMR} ${SCRIPTS_PATH}/supertaggerExtractTestSentences.sc \
    --test ${DATA}/processed/dev/amr-release-1.0-dev-proxy.txt \
    --out "${OUTPUT_DIR}/dev_sentences.txt"

java -Xmx48g -jar ${GRAMR} ${SCRIPTS_PATH}/supertaggerExtractTestSentences.sc \
    --test ${DATA}/processed/test/amr-release-1.0-test-proxy.txt \
    --out "${OUTPUT_DIR}/test_sentences.txt"

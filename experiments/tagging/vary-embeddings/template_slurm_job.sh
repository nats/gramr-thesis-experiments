#!/bin/bash
#SBATCH --job-name=$CONFIG
#SBATCH --time=0-06:00:00

#SBATCH --partition=std
#SBATCH --no-requeue
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --export=NONE

set -e

source /sw/batch/init.sh

module load python/3.8.5

./run.sh $CONFIG.yaml

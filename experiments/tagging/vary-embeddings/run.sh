#!/usr/bin/env bash
#
# Usage:
#   run.sh CONFIG
set -euo pipefail

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../../.."

config="$1"
venv="${ROOT_DIR}/deps/s2tagger-venv"
data="${ROOT_DIR}/data"
output="${SCRIPT_DIR}/output/$(basename ${config} .yaml)"

mkdir -p "${output}"

source "${venv}/bin/activate"

# Train
echo ----------------------------------------
echo Training
echo ----------------------------------------
python -m s2tagger train -c ${config} -m ${output}/train --val 0.1
# Predict val + test
echo ----------------------------------------
echo Predicting val
echo ----------------------------------------
python -m s2tagger predict -c ${config} -m ${output}/train --data-set val -o ${output}/val_tags_pred.txt
echo ----------------------------------------
echo Predicting val
echo ----------------------------------------
python -m s2tagger predict -c ${config} -m ${output}/train --data-set test -o ${output}/test_tags_pred.txt
# Jackknife
echo ----------------------------------------
echo Jackknifing / predicting train
echo ----------------------------------------
python -m s2tagger jackknife -c ${config} -m ${output}/jackknife -o ${output}/train_tags_pred.txt --splits 4

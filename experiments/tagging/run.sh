#!/usr/bin/env bash
# Performs the supertagging step of the pipeline.
# Trains the supertagger and then predicts tags on the train, dev and test sets.
#
# Usage:
#   run.sh LEXICON
#
# where LEXICON is the path to the lexicon files under data/lexicon
set -e

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../.."

LEXICON_PATH=$1

DATA="${ROOT_DIR}/data"
IN_DIR="${DATA}/lexicon/${LEXICON_PATH}"
OUT_DIR="${DATA}/tags/${LEXICON_PATH}"
CONFIG="${OUT_DIR}/config.yaml"

mkdir -p ${OUT_DIR}
DATA=$DATA LEXICON=$IN_DIR envsubst '$DATA $LEXICON' < "${SCRIPT_DIR}/config.template.yaml" > "${CONFIG}"

source ${ROOT_DIR}/deps/venv/bin/activate

# Train
echo Training
python -m s2tagger train -c ${CONFIG} -m ${OUT_DIR} --val 0.1
# Predict val + test
echo Predicting val
python -m s2tagger predict -c ${CONFIG} -m ${OUT_DIR} --data-set val -o ${OUT_DIR}/val_tags_pred.txt
python -m s2tagger predict -c ${CONFIG} -m ${OUT_DIR} --data-set test -o ${OUT_DIR}/test_tags_pred.txt
# Jackknife
echo Jackknifing / predicting train
python -m s2tagger jackknife -c ${CONFIG} -o ${OUT_DIR}/train_tags_pred.txt --splits 4

#!/usr/bin/env bash
# Generate slurm scripts for individual experimental conditions

set -e

params=(none syncat)

for param in ${params[@]}; do
  CONFIG=${param} envsubst '$CONFIG' < template_slurm_job.sh > run_slurm_${param}.sh
done

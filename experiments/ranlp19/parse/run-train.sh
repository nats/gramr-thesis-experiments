#!/usr/bin/env bash
set -e

SCRIPT_DIR=$(dirname "$0")
SCRIPTS_PATH="${SCRIPT_DIR}/../scripts"

DATAROOT=${DATA}/processed
TRAIN=${DATAROOT}/training/amr-release-1.0-training-proxy.txt
VAL=${DATAROOT}/dev/amr-release-1.0-dev-proxy.txt
LEX_DIR=${DATA}/lexicon
TAG_DIR=${SCRIPT_DIR}/../data/tag/output

OUTDIR=${DATA}/model
mkdir -p $OUTDIR

GRAMR_OPTS="--iterations 4 --logDir ${OUTDIR} --lexDir ${LEX_DIR} --tagDir ${TAG_DIR} --oracleBeamSize 20 --update early+cost --beamSize 15 --parallelism 16"
java -Xmx48g -cp $GRAMR gramr.experiments.RunScript ${SCRIPTS_PATH}/trainOracle.sc --train $TRAIN --validate $VAL $GRAMR_OPTS | tee gramr_train.log

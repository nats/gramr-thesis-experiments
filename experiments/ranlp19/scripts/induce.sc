import $file.commonSettings
import $file.grammar.ccgaski
import java.io.{BufferedWriter, File, FileWriter, PrintWriter}

import gramr.experiments._
import gramr.graph.AdjacencyMeaningGraph
import gramr.jobs.ops._
import gramr.util.Logging
import gramr.experiments.ScriptSupport._
import commonSettings._
import gramr.ccg.{DelexedLexicon, Derivation}
import gramr.jobs.Job
import gramr.split.{PatternBasedSplitGenerator, SentenceSplittingAlgorithm, SplitChartFilter, SplitGenerator}
import gramr.split.em.{DelexicalizedEstimator, DelexicalizedModel}
import gramr.split.labelling.{CompleteNodeAlignments, LabelAlignments}
import gramr.tagging.Layer

import scala.collection.mutable.ArrayBuffer
import scala.util.Random

type MR = AdjacencyMeaningGraph

type ExampleDerivationPair = (Example[MR], (String, Derivation[Unit]))


@main
def main(
  corpus: String = "../../data/processed/training/amr-release-1.0-training-proxy.txt",
  alignments: String = "jamr+isi",
  samples: Option[Int] = None,
  maxItemCount: Option[Int] = None,
  maxUnalignedNodes: Option[Int] = None,
  maxModifieeDepth: Int = 0,
  emIterations: Int = 100,
  logDir: String = "logs/test_$DATE_$TIME",
  seed: Int = 1,
  parallelism: Option[Int] = None
): Unit = {

  val logPath: File = Logging.setupLogging(logDir)
  val lexDir = logPath

  implicit val random: Random = new Random(seed)

  val actualParallelism = parallelism.getOrElse(Runtime.getRuntime.availableProcessors())

  val examples = loadInductionData(corpus, alignments, samples)

  val keyExtractor = gramr.ccg.StoredLexicon.defaultKeyExtractor

  val splitFilter = gramr.split.filters.Connected() and
    gramr.split.filters.MatchSyntacticArity(gramr.split.filters.MatchSyntacticArity.LessOrEqual)

  def splitGenerator(example: Example[MR]): SplitGenerator[MR] = {
    val labellingFunction = LabelAlignments() andThen CompleteNodeAlignments(maxUnalignedNodes)
    new PatternBasedSplitGenerator(
      labellingFunction,
      ccgaski.ccgAskiRules(maxModifieeDepth = Some(maxModifieeDepth))
    )
  }

  val splittingAlgorithm = new gramr.split.RecursiveSplitting(splitGenerator, splitFilter, maxItemCount)

  def chartFilter(e: Example[MR]): SplitChartFilter[MR] = gramr.split.chartFilters.LowestCrossSection[MR](e.derivations)

  logger.info("Inducing lexicalized lexicon")

  val exampleDerivationPairs = examples.flatMap { example => example.indexedDerivations.map { der => (example, der) } }
  val filteredExampleDerivationPairs = splitPass1(exampleDerivationPairs, splittingAlgorithm, chartFilter, actualParallelism)

  logger.info("Splitting pass 2: Building delexicalized lexicon")

  val lexiconBuilder = new gramr.ccg.StoredLexicon.Builder[MR](keyExtractor = keyExtractor)

  Job(filteredExampleDerivationPairs, "split-2")
    .split(
      splitter = splittingAlgorithm,
      parallelism = actualParallelism,
      inOrder = false
    )
    .progressBar
    .flatten
    .filter(chartFilter)
    .addToLexicon(lexiconBuilder, eraseSyntacticAttributes = true)
    .run()

  val lexicalizedLexicon = lexiconBuilder.build

  logger.info(s"Lexicalized lexicon has ${lexicalizedLexicon.size} entries")

  logger.info("Delexicalizing")

  val delexedLexicon = gramr.graph.lexemes.delexicalizeLexicon(
    lexicalizedLexicon,
    maxLexNodes = 1,
    wildcardFillers = wildcardFillers)
  val lexemes = delexedLexicon.lexemes

  logger.info(s"Delexicalized lexicon has ${delexedLexicon.delexed.size} templates, ${delexedLexicon.lexemes.size} lexemes")

  logger.info(s"Estimating EM parameters")

  val indexedDerivations: Map[Example[MR], Seq[(String, Derivation[Unit])]] = filteredExampleDerivationPairs.groupBy(_._1).mapValues(_.map(_._2))

  val estimator = new DelexicalizedEstimator[MR, Example[MR]](
    filteredExampleDerivationPairs,
    lexicon = delexedLexicon.delexed,
    lexemes = delexedLexicon.lexemes,
    maxLexNodeCount = 1,
    sentenceSplitter = splittingAlgorithm,
    iterations = emIterations
  )
  val parameters = estimator.estimate

  logger.info("Filtering lexemes and templates according to EM parameters")

  var bestLexemeIds = Set.empty[Int]
  var bestTemplateIds = Set.empty[Int]
  for {
    example <- examples
    (derivationName, derivation) <- indexedDerivations(example)
  } {
    val chart = splittingAlgorithm.split(example, derivation, derivationName)
    val best = parameters.bestSplit(chart)
    for((entry, lexeme, template) <- best) {
      bestLexemeIds = bestLexemeIds + DelexicalizedModel.lexemeId(lexemes)(lexeme)
      bestTemplateIds = bestTemplateIds ++ parameters.lexEntryIds(chart)(entry, template)
    }
  }

  val filteredLexicon = new DelexedLexicon(
    delexedLexicon.delexed.filter(item => bestTemplateIds contains item.index),
    lexemes.filter { item => bestLexemeIds contains item.id }
  )

  val goldSyncats: Iterable[Vector[String]] = examples.map(_.ccgTags.map(_.map(_._1).headOption.getOrElse("UNK").toString))

  gramr.tagging.TaggedDataSet.fromSplitCharts(
    examples, indexedDerivations, splittingAlgorithm, parameters, lexemes
  )
    .addLayer(goldSyncats.map(tags => Layer("syncat", tags)))
    .save(new File(lexDir, "train_tags_gold.txt"))

  logger.info(s"Filtered template count: ${filteredLexicon.delexed.itemCount}")
  logger.info(s"Filtered lexeme count  : ${filteredLexicon.lexemes.size}")


  def writeTmplParameters(file: File = new File(lexDir, "tmpl_parameters.csv")): Unit = {
    val writer = new PrintWriter(new BufferedWriter(new FileWriter(file)))
    writer.println("id,probability,tokens,length,syncat,amr")

    filteredLexicon.delexed.items foreach { item =>
      val toks = item.tokens.map(_.text.replace("\"", "&quot;")).mkString(" ")
      val amr = item.meaning.toString.replaceAll("""\s+""", " ").replace("\"", "&quot;")
      writer.println(s""""${item.index}",${parameters.parameters.templateParameters(item.index)},"$toks",${item.tokens.size},"${item.synCat}","$amr"""")
    }

    writer.close()
  }

  def writeLexParameters(file: File = new File(lexDir, "lex_parameters.csv")): Unit = {
    val writer = new PrintWriter(new BufferedWriter(new FileWriter(file)))
    writer.println("id,probability,tokens,nodes")

    filteredLexicon.lexemes.lexemeItems foreach { item =>
      val toks = item.lexeme.tokens.map(_.replace("\"", "&quot;")).mkString(" ")
      val nodes = item.lexeme.labels.map(_.replace("\"", "&quot;")).mkString(" ")
      writer.println(s""""${item.id}",${parameters.parameters.lexemeParameters(item.id)},"$toks","$nodes"""")
    }

    writer.close()
  }

  writeTmplParameters()
  writeLexParameters()


  filteredLexicon.save(
    templateFile = new File(lexDir, "templates.txt"),
    lexemeFile = new File(lexDir, "lexemes.txt")
  )

}

def splitPass1(
  exampleDerivationPairs: Seq[ExampleDerivationPair],
  splittingAlgorithm: SentenceSplittingAlgorithm[MR, Example[MR]],
  chartFilter: Example[MR] => SplitChartFilter[MR],
  parallelism: Int
): Seq[ExampleDerivationPair] = {

  logger.info("Splitting pass 1: Computing quality of syntactic derivations")
  val splitQualities = ArrayBuffer.empty[(String, String, Double)]

  Job(exampleDerivationPairs, "split-1")
    .split(
      splitter = splittingAlgorithm,
      parallelism = parallelism,
      inOrder = false
    )
    .progressBar
    .flatten
    .filter(chartFilter)
    .trackSplitQualities(splitQualities)
    .run()

  val splitterMask = bestSplitterMask(splitQualities, margin = 0.1, count = 1)

  exampleDerivationPairs.filter {
    case (example, (derivationName, derivation)) =>
      splitterMask.contains(example.id, derivationName)
  }
}

def bestSplitterMask(splitQualities: Seq[(String, String, Double)], margin: Double, count: Int = -1): Set[(String, String)] = {
  val byExample = splitQualities.groupBy(_._1).toVector

  val counted = if(count != -1) {
    byExample map {
      case (id, entries) =>
        (id, entries.sortBy(-_._3).take(count))
    }
  } else {
    byExample
  }

  val best = counted map { case (id, entries) =>
    val bestQuality = entries.map(_._3).max

    def f(entry: (String, String, Double)) = entry._3 >= bestQuality - margin

    (id, entries.filter(f))
  }

  best.foreach { case (id, group) =>
    val idxs = group.map(_._2).mkString(", ")
    logger.trace(s"Best splitters for $id: $idxs")
  }

  val maskEntries = for {
    (id, group) <- best
    (_, idx, _) <- group
  } yield (id, idx)

  maskEntries.toSet
}

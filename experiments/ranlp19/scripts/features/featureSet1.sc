import $file.path
import gramr.experiments._
import gramr.graph.Graph
import gramr.learn.FeatureVector

def derivationFeatures[MR: Graph, FV: FeatureVector] = DerivationFeatureCollection[MR, FV](
  SupertaggerConfidence(attributeName = "supertags-template-id"),
  TokenSyncatConfidence(),
  LexicalItemIdentity(),
  GeneratorFeature(""".*""".r,
    context = List(GeneratorFeature.GeneratorName)),
  GeneratorFeature(""".*""".r,
    context = List(
      GeneratorFeature.GeneratorName,
      GeneratorFeature.SynCat(includeFeatures = false),
      GeneratorFeature.Tokens
    )
  ),
  GeneratorFeature(""".*""".r,
    context = List(
      GeneratorFeature.GeneratorName,
      GeneratorFeature.TokenCount(List(4, 8))
    )
  ),
  LexicalWordMeaningCooccurrence(),
  LexemeLemma(),
  TemplateLemma(),
  CombinatorFeature(Seq("lex.*".r, "tr.*".r, "bxc".r, "gbx".r, "fc".r, "gfc".r)),
  CombinatorFeature(Seq("lex.*".r, "tr.*".r, "bxc".r, "gbx".r, "fc".r, "gfc".r), includeSemanticRoot = true),
  SyncatRootFeature(NodeTransformer.Content),
  LexicalSkeletonFeature()
)

def stateFeatures[MR: Graph, FV: FeatureVector] = StateFeatureCollection[MR, FV](
  path.pathFeaturesL2,
  DuplicateEdgeFeature(NodeTransformer.Constant("*"), EdgeTransformer.Label),
  DuplicateEdgeFeature(NodeTransformer.Content, EdgeTransformer.Label),
  DuplicateNeighbourFeature(NodeTransformer.Constant("*"), NodeTransformer.Constant("*")),
  DuplicateNeighbourFeature(NodeTransformer.Content, NodeTransformer.Content)
)


def featureSet[MR: Graph, FV: FeatureVector]  : FeatureSet[MR, FV] = FeatureSet(
  derivationFeatures = derivationFeatures,
  stateFeatures = stateFeatures
)


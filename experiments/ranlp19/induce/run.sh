#!/usr/bin/env bash
# Performs the induction run.

set -e

SCRIPT_DIR=$(dirname "$0")
SCRIPTS_PATH="${SCRIPT_DIR}/../scripts"

ALIGNMENT=jamr+isi

java -Xmx12g -jar ${GRAMR} ${SCRIPTS_PATH}/induce.sc --corpus ${DATA}/processed/training/amr-release-1.0-training-proxy.txt --logDir ${DATA}/lexicon --alignments ${ALIGNMENT} --maxItemCount 10000

# Extract sentence files needed for supertagger feature extraction
java -Xmx12g -jar ${GRAMR} ${SCRIPTS_PATH}/supertaggerExtractTestSentences.sc --test ${DATA}/processed/dev/amr-release-1.0-dev-proxy.txt --out ${DATA}/lexicon/dev_sentences.txt --lexDir ${DATA}/lexicon
java -Xmx12g -jar ${GRAMR} ${SCRIPTS_PATH}/supertaggerExtractTestSentences.sc --test ${DATA}/processed/test/amr-release-1.0-test-proxy.txt --out ${DATA}/lexicon/test_sentences.txt --lexDir ${DATA}/lexicon

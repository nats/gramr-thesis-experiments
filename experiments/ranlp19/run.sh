#!/usr/bin/env bash
# Runs one or several experiments.
#
# Usage:
#
#    run.sh [STEP1 [STEP2 …]]

SCRIPT_DIR=$(dirname "$0")
ROOT_DIR="${SCRIPT_DIR}/../.."

export GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
export TAGGER=${ROOT_DIR}/deps/s2tagger
export DATA=${ROOT_DIR}/data

source ${ROOT_DIR}/deps/venv/bin/activate

if [[ $# -eq 0 ]]
then
    commands=(induce tag train-parser test-parser)
else
    commands=$@
fi

for cmd in ${commands[@]}
do
    ${SCRIPT_DIR}/bin/${cmd}
done

#!/usr/bin/env bash
# Generate slurm scripts for individual experimental conditions

set -e

FEATURESETS=(\
  identities\
  identities+path\
  identities+path+duplicate\
  identities+path+duplicate+tokens\
  identities+path+duplicate+tokens+syn\
  identities+path+duplicate+tokens+syn+supertagger\
  identities+path+duplicate+tokens+syn+supertagger+ccgtagger\
  identities+path+duplicate+tokens+syn+supertagger+ccgtagger+skeleton\
  )

for FEATURESET in ${FEATURESETS[@]}; do
  FEATURESET=$FEATURESET envsubst '$FEATURESET' < template_slurm_job.sh > run_slurm_$FEATURESET.sh
  chmod +x run_slurm_$FEATURESET.sh
done

#!/usr/bin/env bash
set -euo pipefail

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../.."

config="${SCRIPT_DIR}/tagger-config.10.yaml"
venv="${ROOT_DIR}/deps/s2tagger-venv"
data="${ROOT_DIR}/data"
output="${SCRIPT_DIR}/output/tagger.10"

mkdir -p "${output}"

source "${venv}/bin/activate"

# Train
echo Training
python -m s2tagger train -c ${config} -m ${output} --val 0.1
# Predict val + test
echo Predicting val
python -m s2tagger predict -c ${config} -m ${output} --data-set val -o ${output}/val_tags_pred.txt
python -m s2tagger predict -c ${config} -m ${output} --data-set test -o ${output}/test_tags_pred.txt
# Jackknife
echo Jackknifing / predicting train
python -m s2tagger jackknife -c ${config} -o ${output}/train_tags_pred.txt --splits 4

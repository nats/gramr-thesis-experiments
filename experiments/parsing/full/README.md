# full

This is the “final” condition with hyperparameters optimised for Smatch performance.

- Schedule: 1 heuristic, 10 cost
- Beam size: 25
- Max sentence length (training): 30

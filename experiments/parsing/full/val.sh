#!/usr/bin/env bash
set -e

GRAMMAR=all
ALIGNMENTS=jamr+tamr+amr_ud+isi-vote1

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../../.."

GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
DATA=${ROOT_DIR}/data
SCRIPTS_PATH=${SCRIPT_DIR}/../../scripts
OUTPUT_DIR=${SCRIPT_DIR}/output
LEX_DIR=${SCRIPT_DIR}/../output/induce
TAG_DIR=${SCRIPT_DIR}/../output/tagger.20

java -Xmx48g -jar ${GRAMR} ${SCRIPTS_PATH}/test.sc \
    --grammar ${GRAMMAR} \
    --alignments ${ALIGNMENTS} \
    --test ${DATA}/processed/dev/amr-release-1.0-dev-proxy.txt \
    --output ${OUTPUT_DIR}/val \
    --lexicon ${LEX_DIR} \
    --tags ${TAG_DIR}/val_tags_pred.txt \
    --model ${OUTPUT_DIR}/6-model.ser \
    --learner adadelta \
    --beam 25 \
    --testSentenceRejectLength 100

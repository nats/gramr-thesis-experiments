#!/bin/bash
#SBATCH --job-name=full-val
#SBATCH --time=0-12:00:00

#SBATCH --partition=std
#SBATCH --no-requeue
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --export=NONE

set -e

source /sw/batch/init.sh

module load java/oracle-jdk8u101
module load git/2.6.4

export COURSIER_CACHE=$PWD/.cache
./val.sh

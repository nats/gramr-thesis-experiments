#!/usr/bin/env bash

for d in output/*; do
    for f in $d/*-val-stats.csv; do
        echo $f
        ../../analysis/parse-stats.py $f
        echo
    done
done

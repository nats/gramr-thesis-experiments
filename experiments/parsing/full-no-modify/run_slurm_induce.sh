#!/bin/bash
#SBATCH --job-name=induce
#SBATCH --time=2-00:00:00

#SBATCH --partition=std
#SBATCH --no-requeue
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --export=NONE

set -e

source /sw/batch/init.sh

module load java/oracle-jdk8u101
module load git/2.6.4

export COURSIER_CACHE=$PWD/.cache
./01-induce.sh
./02-extract-test-sentences.sh
./03-tag.20.sh

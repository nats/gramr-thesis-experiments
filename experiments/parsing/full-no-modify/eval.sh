#!/usr/bin/env bash

function eval {
    prefix=$1
    testfile=$2
    goldfile=$3

    python -m amrcorpustools.reorder \
        --input "${testfile}" \
        --by "${goldfile}" \
        > "${prefix}-amr-reordered.txt"

    python -m smatch -f "${prefix}-amr-reordered.txt" "${goldfile}" --pr --significant 4
}

output_dir="output"
corpus_dir="../../../data/amr_anno_1.0/data/split"

echo "Eval val"
valfile="${output_dir}/val/test-amr.txt"
valgoldfile="${corpus_dir}/dev/amr-release-1.0-dev-proxy.txt"
eval val "$valfile" "$valgoldfile"

echo "Eval test"
testfile="${output_dir}/test/test-amr.txt"
testgoldfile="${corpus_dir}/test/amr-release-1.0-test-proxy.txt"
eval test "$testfile" "$testgoldfile"

#!/usr/bin/env bash

java -jar ../../../deps/gramr/gramr.jar ../../scripts/corpusToTikz.sc --corpus val-amr-reordered.txt --output graphs/val-parsed
java -jar ../../../deps/gramr/gramr.jar ../../scripts/corpusToTikz.sc --corpus ../../../data/amr_anno_1.0/data/split/dev/amr-release-1.0-dev-proxy.txt --output graphs/val-gold

./combine-graphs.py

pushd graphs
for f in *.combined.tex; do
    echo $f
    lualatex $f
done
popd


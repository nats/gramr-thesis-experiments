#!/usr/bin/env python3

from pathlib import Path

parsed = Path('graphs/val-parsed').glob('*.tex')

for parsed_path in parsed:
    example_name = parsed_path.stem
    example_name_escaped = example_name.replace('_', '\\_')
    tex = r"""\documentclass{article}
\usepackage[landscape,margin=5mm]{geometry}
\usepackage{standalone}
\usepackage{subcaption}
\usepackage{tikz}
\usetikzlibrary{graphdrawing}
\usetikzlibrary{shapes.geometric}
\usegdlibrary{force}

\begin{document}

\begin{figure*}
\centering
\subcaptionbox{parsed}{
    \maxsizebox{13cm}{\textheight-1.5cm}{
    \input{val-parsed/""" + example_name + r""".tex}
    }
}
\subcaptionbox{gold}{
    \maxsizebox{13cm}{\textheight-1.5cm}{
    \input{val-gold/""" + example_name + r""".tex}
    }
}
\caption{""" + example_name_escaped + r"""}
\end{figure*}

\end{document}"""
    output_path = Path('graphs') / f'{example_name}.combined.tex'
    with open(output_path, 'w') as f:
        f.write(tex)

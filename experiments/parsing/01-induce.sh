#!/usr/bin/env bash
# Needs <2d on a slurm node
set -euo pipefail

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../.."

GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
DATA=${ROOT_DIR}/data
SCRIPTS_PATH=${SCRIPT_DIR}/../scripts
OUTPUT_DIR="${SCRIPT_DIR}/output/induce"
LEXICON_DIR="${DATA}/lexicon/parsing"

java -Xmx56g -jar ${GRAMR} ${SCRIPTS_PATH}/largeScaleInduction.sc \
    --grammar all \
    --corpus ${DATA}/processed/training/amr-release-1.0-training-proxy.txt \
    --output ${OUTPUT_DIR} \
    --alignments jamr+tamr+amr_ud+isi-vote1 \
    --maxItemCount 10000 \
    --maxUnalignedNodes 10 \
    --ccgDerivations 50

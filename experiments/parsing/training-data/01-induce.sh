#!/usr/bin/env bash
set -euo pipefail

PERCENTAGE=$1

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../../.."

GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
DATA=${ROOT_DIR}/data
SCRIPTS_PATH="${ROOT_DIR}/experiments/scripts"
OUTPUT_DIR="${SCRIPT_DIR}/output/${PERCENTAGE}/induce"
IDFILE="train_ids_${PERCENTAGE}.txt"

java -Xmx56g -jar ${GRAMR} ${SCRIPTS_PATH}/largeScaleInduction.sc \
    --grammar all \
    --ids ${IDFILE} \
    --corpus ${DATA}/processed/training/amr-release-1.0-training-proxy.txt \
    --output ${OUTPUT_DIR} \
    --alignments jamr+tamr+amr_ud+isi-vote1 \
    --maxItemCount 10000 \
    --maxUnalignedNodes 10 \
    --ccgDerivations 10 \
    --emIterations 20

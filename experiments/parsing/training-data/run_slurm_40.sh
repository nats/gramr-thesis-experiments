#!/bin/bash
#SBATCH --job-name=40
#SBATCH --time=5-00:00:00

#SBATCH --partition=std
#SBATCH --no-requeue
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --export=NONE

set -e

source /sw/batch/init.sh

module load java/oracle-jdk8u101
module load git/2.6.4

export COURSIER_CACHE=$PWD/.cache


./01-induce.sh 40
./02-extract-test-sentences.sh 40
./03-tag.sh 40
./04-train.sh 40

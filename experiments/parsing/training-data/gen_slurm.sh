#!/usr/bin/env bash
# Generate slurm scripts for individual experimental conditions

set -e

PERCENTAGES=(1 10 20 30 40 50 60 70 80 90 100)

for PERCENTAGE in ${PERCENTAGES[@]}; do
  PERCENTAGE=$PERCENTAGE envsubst '$PERCENTAGE' < template_slurm_job.sh > run_slurm_$PERCENTAGE.sh
  PERCENTAGE=$PERCENTAGE envsubst '$PERCENTAGE' < tagger-config.template.yaml > tagger-config.$PERCENTAGE.yaml
  chmod +x run_slurm_$PERCENTAGE.sh
done

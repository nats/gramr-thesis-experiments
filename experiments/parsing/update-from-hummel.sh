#!/usr/bin/env bash
set -euo pipefail

rsync -az --info=progress2 hummel1:/work/intx039/thesis-experiments/experiments/parsing/output .


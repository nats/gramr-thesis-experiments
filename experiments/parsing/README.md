# Parsing Experiments

First, run the scripts in this directory to create a lexicon and supertags.
Then, run any of the experiments in the sub-directories.

## Experiments for thesis

- baseline: “Starting point” for hyperparameter optimisation.
- beam: Evaluate the effect of using various beam sizes.
- bootstrap: Train with and without bootstrapping and compare.
- features: Evaluate the with various feature sets.
- full: Configuration we use to evaluate overall system performance.
- training-data: These experiments each run a full induction pipeline, using
  reduced amounts of training data.
- (grammars)


Setup of experiments:

- Use moderate beam size, e.g. 20, and max sentence length, e.g. 25


## Other experiments

- tags: Try training with different amounts of supertagger tags.
- update: Try early vs. cost update with / without oracle and bootstrapping
- sentence-length

## Plan

- Figure out the influence of limited sentence lengths during training – both
  time and performance wise
- Find maximum feasible beam size (< 7 days training time) => this is the “full”
  system
- Run beam experiment: find beam size suitable for further experiments (train in
  1-2 days) but hopefully not too far from final system
- Run bootstrap experiments
- Design feature sets and run feature experiment
- Run training data experiment

#!/usr/bin/env bash
# Generate slurm scripts for individual experimental conditions

set -e

BEAM_SIZES=(5 10 15 20 25 30)

for BEAM_SIZE in ${BEAM_SIZES[@]}; do
  BEAM_SIZE=$BEAM_SIZE envsubst '$BEAM_SIZE' < template_slurm_job.sh > run_slurm_$BEAM_SIZE.sh
  chmod +x run_slurm_$BEAM_SIZE.sh
done

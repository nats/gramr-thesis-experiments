#!/usr/bin/env bash
# Generate slurm scripts for individual experimental conditions

set -e

SCHEDULES=(all bootstrap none)

for SCHEDULE in ${SCHEDULES[@]}; do
  SCHEDULE=$SCHEDULE envsubst '$SCHEDULE' < template_slurm_job.sh > run_slurm_$SCHEDULE.sh
  chmod +x run_slurm_$SCHEDULE.sh
done

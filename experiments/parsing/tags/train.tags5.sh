#!/usr/bin/env bash
set -e

BOOTSTRAP=$1
ORACLE=$2
UPDATE=$3
GRAMMAR=all
ALIGNMENTS=jamr+tamr+amr_ud+isi-vote1

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../../.."

GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
DATA=${ROOT_DIR}/data
SCRIPTS_PATH=${SCRIPT_DIR}/../../scripts
OUTPUT_DIR=${SCRIPT_DIR}/output/${BOOTSTRAP}-${ORACLE}-${UPDATE}-tags5
LEX_DIR=${SCRIPT_DIR}/../output/induce
TAG_DIR=${SCRIPT_DIR}/../output/tagger.5

java -Xmx48g -jar ${GRAMR} ${SCRIPTS_PATH}/parsing.sc \
    --grammar ${GRAMMAR} \
    --alignments ${ALIGNMENTS} \
    --train ${DATA}/processed/training/amr-release-1.0-training-proxy.txt \
    --validate ${DATA}/processed/dev/amr-release-1.0-dev-proxy.txt \
    --output ${OUTPUT_DIR} \
    --lexicon ${LEX_DIR} \
    --tags ${TAG_DIR} \
    --learner adadelta \
    --bootstrap ${BOOTSTRAP} \
    --update ${UPDATE} \
    --oracle ${ORACLE} \
    --beam 10 \
    --oracleBeam 10 \
    --maxSentenceLength 25 \
    --iterations 10

#!/usr/bin/env bash
# Generate slurm scripts for individual experimental conditions

set -e

SENTENCE_LENGTHS=(20 25 30 40 50)

for SENTENCE_LENGTH in ${SENTENCE_LENGTHS[@]}; do
  SENTENCE_LENGTH=$SENTENCE_LENGTH envsubst '$SENTENCE_LENGTH' < template_slurm_job.sh > run_slurm_$SENTENCE_LENGTH.sh
  chmod +x run_slurm_$SENTENCE_LENGTH.sh
done

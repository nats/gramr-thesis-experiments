#!/usr/bin/env bash

for f in output/*-val-stats.csv; do
    echo $f
    ../../analysis/parse-stats.py $f
    echo
done

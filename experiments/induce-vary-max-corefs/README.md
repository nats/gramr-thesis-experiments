# Induce with varying grammars

Introduces several alternative grammars. See what happens during grammar
induction.

- `full`: full CCG-ASKi
- `nodir-ac`: allow semantic operators for all application and composition combinators to work in all directions
- `app-only`: all combinators are interpreted as semantic application
- `app-only-nodir`: `app-only` but semantic application can occur in both directions

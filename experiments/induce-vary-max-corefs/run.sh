#!/usr/bin/env bash
set -e

MAXCOREFS=$1

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../.."

GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
DATA=${ROOT_DIR}/data
SCRIPTS_PATH=${SCRIPT_DIR}/../scripts
PATH_SUFFIX="${MAXCOREFS}"
OUTPUT_DIR="${SCRIPT_DIR}/output/${PATH_SUFFIX}"

java -Xmx48g -jar ${GRAMR} ${SCRIPTS_PATH}/largeScaleInduction.sc \
    --corpus ${DATA}/processed/training/amr-release-1.0-training-proxy.txt \
    --output ${OUTPUT_DIR} \
    --em false \
    --grammar all \
    --alignments jamr+tamr+amr_ud+isi-vote1 \
    --maxCorefs ${MAXCOREFS}

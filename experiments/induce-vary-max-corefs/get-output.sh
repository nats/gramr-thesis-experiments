#!/usr/bin/env bash
set -euo pipefail

rsync -az --delete --info=progress2 hummel1:/work/intx039/thesis-experiments/experiments/induce-vary-max-corefs/output .


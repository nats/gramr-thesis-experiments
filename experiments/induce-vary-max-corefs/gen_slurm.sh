#!/usr/bin/env bash
# Generate slurm scripts for individual experimental conditions

set -e

maxcoref_settings=(0 1 2 3 4 5)

for maxcorefs in ${maxcoref_settings[@]}; do
  MAXCOREFS=$maxcorefs envsubst '$MAXCOREFS' < template_slurm_job.sh > run_slurm_$maxcorefs.sh
done

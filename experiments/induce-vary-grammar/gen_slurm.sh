#!/usr/bin/env bash
# Generate slurm scripts for individual experimental conditions

set -e

GRAMMARS=(all base no-ignore no-modify no-tr unrestricted-a unrestricted-all)

for GRAMMAR in ${GRAMMARS[@]}; do
  GRAMMAR=$GRAMMAR envsubst '$GRAMMAR' < template_slurm_job.sh > run_slurm_$GRAMMAR.sh
done

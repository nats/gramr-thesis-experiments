#!/usr/bin/env bash
# Performs the supertagging step of the pipeline.
# Trains the supertagger and then predicts tags on the train, dev and test sets.
set -e

GRAMMAR=$1

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../.."

GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
DATA=${ROOT_DIR}/data
SCRIPTS_PATH=${SCRIPT_DIR}/../scripts
PATH_SUFFIX="${GRAMMAR}"
OUTPUT_DIR="${SCRIPT_DIR}/output/${PATH_SUFFIX}"
LEXICON_DIR="${DATA}/lexicon/vary-grammar/${PATH_SUFFIX}"

java -Xmx48g -jar ${GRAMR} ${SCRIPTS_PATH}/largeScaleInduction.sc \
    --grammar ${GRAMMAR} \
    --corpus ${DATA}/processed/training/amr-release-1.0-training-proxy.txt \
    --output ${OUTPUT_DIR} \
    --em false

# Copy result to lexicon directory, making it accessible for other experiments
mkdir -p "${LEXICON_DIR}"
cp -r "${OUTPUT_DIR}/"* "${LEXICON_DIR}/"

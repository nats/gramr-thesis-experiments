#!/usr/bin/env bash

# Produce a single split chart.

set -e

example=$1
derivation=$2
grammar=$3

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../.."

GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
DATA=${ROOT_DIR}/data
SCRIPTS_PATH=${SCRIPT_DIR}/../scripts
OUTPUT_DIR="${SCRIPT_DIR}/output_induce_one"

java -Xmx2g -jar ${GRAMR} ${SCRIPTS_PATH}/induceOne.sc \
    --corpus ${DATA}/processed/training/amr-release-1.0-training-proxy.txt \
    --output "${OUTPUT_DIR}/${example}_${derivation}_${grammar}" \
    --example ${example} \
    --derivation ${derivation} \
    --grammar ${grammar}

cp "${OUTPUT_DIR}/${example}_${derivation}_${grammar}/split1/der_${example}_${derivation}.html" "${OUTPUT_DIR}/${example}_${derivation}_${grammar}.html"

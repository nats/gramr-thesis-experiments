import java.io.File

import $file.commonSettings
import commonSettings._
import $file.induction
import induction._
import $file.grammars
import gramr.ccg.{CombinatorSystem, SimpleCombinatorSystem}
import gramr.construction.sgraph.SGraphRule
import gramr.experiments.ScriptSupport._
import gramr.experiments._
import gramr.split.SplitChartFilter
import gramr.util.Logging

import scala.util.Random

implicit val combinatorSystem: CombinatorSystem = new SimpleCombinatorSystem

@main
def main(
  corpus: String,
  output: String,
  grammar: String = "aski",
  alignments: String = "none",
  arityCheck: Boolean = true,
  samples: Option[Int] = None,
  maxItemCount: Option[Int] = None,
  maxUnalignedNodes: Option[Int] = None,
  maxCorefs: Int = 2,
  ccgDerivations: Int = 10,  // We use 10 by default to avoid OOM crashes
  seed: Int = 1,
  parallelism: Option[Int] = None
): Unit = {

  val outputDir = new File(output)

  logger.info(s"Selected alignments: $alignments")

  val logPath = outputDir
  Logging.setupLogging(logPath.toString)

  val rules = grammars.selectGrammar(grammar, maxCorefs = maxCorefs)

  logger.info(s"Selected grammar: $grammar")
  logger.info(s"CCG derivations used: $ccgDerivations")

  implicit val random: Random = new Random(seed)

  val actualParallelism = parallelism.getOrElse(Runtime.getRuntime.availableProcessors())
  logger.info(s"Parallelism: $actualParallelism")

  val examples = loadInductionData(corpus, alignments, samples, ccgDerivations = Some(ccgDerivations))

  saveSplitCharts(examples, rules, maxUnalignedNodes, maxItemCount, arityCheck, actualParallelism, logPath)
}


/** Run grammar induction, saving the split charts for each derivation as HTML.
  *
  * @param examples training examples to induce lexical items from
  * @param rules the set of syntactic-semantic rules
  * @param maxUnalignedNodes stop the algorithm if more than this number of nodes have no aligned words
  *                          (to avoid search space explosion)
  * @param maxItemCount stop the algorithm if more than this number of lexical items are produced from a single sentence
  *                     (to safely abort in case of search space explosion)
  * @param parallelism the number of threads to run in parallel
  * @param outputPath the directory to write output to
  */
def saveSplitCharts(
  examples: Seq[Example[MR]],
  rules: Vector[SGraphRule[MR]],
  maxUnalignedNodes: Option[Int],
  maxItemCount: Option[Int],
  arityCheck: Boolean,
  parallelism: Int,
  outputPath: File
): Unit = {
  val splittingAlgorithm = mkSplittingAlgorithm(
    rules=rules,
    maxUnalignedNodes=maxUnalignedNodes,
    maxItemCount=maxItemCount,
    arityCheck=arityCheck)

  def chartFilter(e: Example[MR]): SplitChartFilter[MR] = SplitChartFilter.noFilter

  logger.info("Inducing lexicalized lexicon")

  val exampleDerivationPairs = examples.flatMap { example => example.indexedDerivations.map { der => (example, der) } }

  splitPass1(exampleDerivationPairs, splittingAlgorithm, chartFilter, parallelism, outputPath, dumpCharts=true)

}
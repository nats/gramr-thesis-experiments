/** Induce from a single example and derivation, and save the resulting split
chart. */

import $file.commonSettings
import $file.grammars
import $file.induction
import commonSettings._
import induction._
import java.io.File

import gramr.ccg.{CombinatorSystem, SimpleCombinatorSystem}
import gramr.experiments.Example
import gramr.experiments.ScriptSupport._
import gramr.split.SplitChartFilter
import gramr.util.Logging

import scala.util.Random


implicit val combinatorSystem: CombinatorSystem = new SimpleCombinatorSystem


@main
def induceOne(
  corpus: String,
  output: String,
  example: String,
  derivation: String,
  grammar: String = "all",
  alignments: String = "jamr+tamr+amr_ud+isi-vote1",
  arityCheck: Boolean = true,
  maxCorefs: Int = 1,
  seed: Int = 1,
  parallelism: Option[Int] = None
): Unit = {

  val outputDir = new File(output)

  logger.info(s"Selected alignments: $alignments")

  val logPath = outputDir
  val lexPath = logPath
  Logging.setupLogging(logPath.toString)

  val rules = grammars.selectGrammar(grammar, maxCorefs = maxCorefs)

  logger.info(s"Selected grammar: $grammar")

  implicit val random: Random = new Random(seed)

  val examples = loadInductionData(corpus, alignments, only=Some(example))

  if(examples.isEmpty) {
      throw new Error(s"Example ${example} not found")
  }

  val splittingAlgorithm = mkSplittingAlgorithm(
    rules=rules,
    arityCheck=arityCheck)

  def chartFilter(e: Example[MR]): SplitChartFilter[MR] = SplitChartFilter.noFilter

  val exampleDerivationPairs = examples.flatMap { example => example.indexedDerivations.map { der => (example, der) } }
  
  val singlePair = exampleDerivationPairs.filter {
      case (example, (derIndex, der)) =>
        derIndex == derivation
  }

  val derivationIndices = exampleDerivationPairs.map(_._2._1)

  if(singlePair.isEmpty) {
      throw new Error(s"Derivation $derivation not found, available: $derivationIndices")
  }

  logger.info(s"Splitting example ${singlePair(0)._1.id} derivation ${singlePair(0)._2._1}")

  splitPass1(
    singlePair, splittingAlgorithm, chartFilter, parallelism = 1, lexPath,
    dumpCharts = true)
}


import $file.commonSettings
import $file.grammar.ccgbank
import $file.grammar.all
import $file.grammar.base
import $file.grammar.no_ignore
import $file.grammar.no_modify
import $file.grammar.no_tr
import $file.grammar.unrestricted_a
import $file.grammar.unrestricted_all
import gramr.construction.sgraph.SGraphRule
import gramr.graph.Graph

def selectGrammar[MR: Graph](grammar: String, maxCorefs: Int): Vector[SGraphRule[MR]] = grammar match {
  case "ccgbank" => ccgbank.rules(maxCorefs = maxCorefs)
  case "all" => all.rules(maxCorefs = maxCorefs)
  case "no-ignore" => no_ignore.rules(maxCorefs = maxCorefs)
  case "no-modify" => no_modify.rules(maxCorefs = maxCorefs)
  case "no-tr" => no_tr.rules(maxCorefs = maxCorefs)
  case "base" => base.rules(maxCorefs = maxCorefs)
  case "unrestricted-a" => unrestricted_a.rules(maxCorefs = maxCorefs)
  case "unrestricted-all" => unrestricted_all.rules(maxCorefs = maxCorefs)
}

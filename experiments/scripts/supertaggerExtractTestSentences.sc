import $file.commonSettings
import java.io.File

import commonSettings._
import gramr.ccg.{CombinatorSystem, SimpleCombinatorSystem}
import gramr.tagging.Layer

import scala.util.Random

implicit val combinatorSystem: CombinatorSystem = new SimpleCombinatorSystem

@main
def main(
  test: String = "corpus/test/amr-release-1.0-test-proxy.txt",
  out: String = "test_sentences.txt",
  lexDir: String = "."
): Unit = {

  implicit val random: Random = new Random(1)

  val testExamples = loadTestData(test)

  val goldSyncatsTest: Iterable[Vector[String]] = testExamples.map(_.ccgTags.map(_.map(_._1).headOption.getOrElse("UNK").toString))

  gramr.tagging.TaggedDataSet.fromExamples(testExamples)
    .addLayer(goldSyncatsTest.map(tags => Layer("syncat", tags)))
    .save(new File(out))
}

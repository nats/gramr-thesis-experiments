import java.io.{File, FileWriter}
import $file.commonSettings
import $file.grammars
import $file.grammar.ccgbank
import $file.induction
import $file.saveSplitCharts
import ccgbank.CcgbankCombinatorSystem
import commonSettings._
import gramr.amr.AmrCorpusItem
import gramr.experiments.ScriptSupport._
import gramr.graph.presentation.ToTikz
import gramr.util.Logging
import saveSplitCharts.saveSplitCharts

import scala.util.Random


@main
def grammarCoverage(
  corpus: String,
  output: String,
  grammar: String = "full",
  alignments: String = "gold",
  syntax: String = "gold",
  samples: Option[Int] = None,
  maxItemCount: Option[Int] = Some(10000),
  maxUnalignedNodes: Option[Int] = None,
  maxCorefs: Int = 1,
  arityCheck: Boolean = true,
  emIterations: Int = 100,
  seed: Int = 1,
  parallelism: Option[Int] = None,
  only: Option[String] = None
): Unit = {

  implicit val combinatorSystem = new CcgbankCombinatorSystem

  val outputDir = new File(output)

  val logPath = new File(outputDir, s"$grammar.$syntax.$alignments.maxCorefs=$maxCorefs.arityCheck=$arityCheck")
  Logging.setupLogging(logPath.toString)

  logger.info("Starting grammar coverage experiment")
  logger.info(s"grammar=$grammar syntax=$syntax alignments=$alignments")

  val rules = grammars.selectGrammar(grammar, maxCorefs = maxCorefs)

  implicit val random: Random = new Random(seed)

  val actualParallelism = parallelism.getOrElse(Runtime.getRuntime.availableProcessors())

  val derivationFilter: AmrCorpusItem => AmrCorpusItem = syntax match {
    case "gold" =>
      item => item.filterAttributes(attrName =>
        !attrName.startsWith("ccg-derivation-")
        || attrName == "ccg-derivation-gold")
    case "easyccg" =>
      item => item.filterAttributes(attrName =>
        !attrName.startsWith("ccg-derivation-")
        || attrName != "ccg-derivation-gold")
  }

  val examples = loadInductionData(corpus, alignments, samples, preprocessors=Seq(derivationFilter), only=only)

  logger.info(s"Saving .tex files")
  val toTikz = new ToTikz[MR]
  examples.foreach { example =>
    val texFile = new File(logPath, s"${example.id}.tex")
    val writer = new FileWriter(texFile)
    val tikz = toTikz(example.reference)
    writer.write(tikz)
    writer.close()
  }

  saveSplitCharts(
    examples=examples,
    rules=rules,
    maxUnalignedNodes=maxUnalignedNodes,
    maxItemCount=maxItemCount,
    arityCheck=arityCheck,
    parallelism=actualParallelism,
    outputPath=logPath
  )
}

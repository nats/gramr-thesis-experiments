import java.io._
import $file.features.featureSet1
import $file.features.identities
import $file.features.identities_path
import $file.features.identities_path_duplicate
import $file.features.identities_path_duplicate_tokens
import $file.features.identities_path_duplicate_tokens_syn
import $file.features.identities_path_duplicate_tokens_syn_supertagger
import $file.features.identities_path_duplicate_tokens_syn_supertagger_ccgtagger
import $file.features.identities_path_duplicate_tokens_syn_supertagger_ccgtagger_skeleton
import gramr.amr.alignments.Combiners
import gramr.amr.{AmrCorpusItem, AmrCorpusLoader, AttributeFileParser}
import gramr.ccg.{CombinatorSystem, Derivation, DerivationStep, SyntacticCategory}
import gramr.experiments.ScriptSupport._
import gramr.experiments.Example
import gramr.graph.lexemes.{EmptyFiller, LemmaFiller, PropbankFiller, QuotedFiller, WildcardFiller}
import gramr.graph.{AdjacencyMeaningGraph, Graph}
import gramr.learn._
import gramr.learn.features.FeatureSet
import gramr.learn.util.{FeatureHasher, FeatureIndex}
import gramr.parse.{ActionPredicate, ActionPredicates, BinaryChoice, CombinatoryChoice, Decision, FallbackParser, LexicalChoice, Parser, and}
import gramr.parse.chart._
import gramr.text.{HasMR, Span, Token}

import scala.collection.immutable.HashMap
import scala.collection.mutable.ArrayBuffer
import scala.util.Random


type MR = AdjacencyMeaningGraph
type FV = MapFeatureVector


// ------------------------------------------------------------------
// Grammar
// ------------------------------------------------------------------

val wildcardFillers: scala.collection.immutable.Iterable[WildcardFiller] =
  scala.collection.immutable.Iterable(LemmaFiller(), QuotedFiller(), PropbankFiller())

val wildcardFillersParsing: scala.collection.immutable.Iterable[WildcardFiller] =
  wildcardFillers ++ Iterable(EmptyFiller())


// ------------------------------------------------------------------
// Features
// ------------------------------------------------------------------

def selectFeatureSet(name: String = "all")(implicit fv: FeatureVector[MapFeatureVector]): FeatureSet[MR, MapFeatureVector, Example[MR]] =
  name match {
    case "all" => featureSet1.featureSet
    case "identities" => identities.featureSet
    case "identities+path" => identities_path.featureSet
    case "identities+path+duplicate" => identities_path_duplicate.featureSet
    case "identities+path+duplicate+tokens" => identities_path_duplicate_tokens.featureSet
    case "identities+path+duplicate+tokens+syn" => identities_path_duplicate_tokens_syn.featureSet
    case "identities+path+duplicate+tokens+syn+supertagger" => identities_path_duplicate_tokens_syn_supertagger.featureSet
    case "identities+path+duplicate+tokens+syn+supertagger+ccgtagger" => identities_path_duplicate_tokens_syn_supertagger_ccgtagger.featureSet
    case "identities+path+duplicate+tokens+syn+supertagger+ccgtagger+skeleton" => identities_path_duplicate_tokens_syn_supertagger_ccgtagger_skeleton.featureSet
  }


// ------------------------------------------------------------------
// Loading data
// ------------------------------------------------------------------

type Loader = AmrCorpusLoader[AdjacencyMeaningGraph, Example[AdjacencyMeaningGraph]]

def baseLoader(implicit cs: CombinatorSystem) = AmrCorpusLoader[AdjacencyMeaningGraph, Example[AdjacencyMeaningGraph]]


/** Make an alignment attribute ready for usage and combining with other alignments. */
def useAlignment(alignment: String, targetAttribute: String = "alignments"): Loader => Loader = {
  alignment match {
    case "none" => (x: Loader) => x
    case "gold" => _.useIsiAlignment("alignments-gold", targetAttribute)
    case "jamr" => _.useJamrAlignment("jamr-alignments", targetAttribute)  // for some reason, I deviated from my own naming scheme here…
    case "isi" => _.useIsiAlignment("alignments-isi", targetAttribute)
    case "tamr" => _.useJamrAlignment("alignments-tamr", targetAttribute)
    case "amr_ud" => _.useAmrUdAlignment("alignment-amr_ud", targetAttribute)
  }
}


val combiners: Map[String, Seq[Set[Int]] => Set[Int]] = Map(
  "union" -> Combiners.union,
  "vote1" -> Combiners.vote(1),
  "vote2" -> Combiners.vote(2),
  "vote3" -> Combiners.vote(3),
  "vote4" -> Combiners.vote(4),
  "vote5" -> Combiners.vote(5),
)


def useAlignmentCombination(alignments: Seq[String], combiner: String)(loader: Loader): Loader = {
  val alignmentAttributes = alignments.map("alignments-" + _)
  val usingAlignments = (alignments zip alignmentAttributes).foldLeft(loader) {
    case (l, (al, attr)) =>
      useAlignment(al, attr)(l)
  }
  val combinerFunction = combiners(combiner)
  usingAlignments.combineAlignments(combinerFunction, alignmentAttributes)
}


def combineAlignments(alignments: String): Loader => Loader = {
  val components = alignments.split("""-""", 2).toSeq
  components match {
    case Seq(alignments) =>
      useAlignmentCombination(alignments.split("""\+""").toSeq, "union")
    case Seq(alignments, combiner) =>
      useAlignmentCombination(alignments.split("""\+""").toSeq, combiner)
  }
}


def loadInductionData(
  corpus: String,
  alignments: String,
  nExamples: Option[Int] = None,
  ccgDerivations: Option[Int] = None,
  only: Option[Set[String]] = None,
  preprocessors: Seq[AmrCorpusItem => AmrCorpusItem] = Seq()
)(implicit random: Random, cs: CombinatorSystem): Seq[Example[MR]] = {
  val alignmentLoader = combineAlignments(alignments)(baseLoader)
  val derivationLimitedLoader = ccgDerivations match {
    case None => alignmentLoader
    case Some(count) => alignmentLoader.addPreprocessor(item => item.limitCcgDerivations(count))
  }
  val preprocessedLoader = preprocessors.foldLeft(derivationLimitedLoader)((l, p) => l.addPreprocessor(p))
  val loader = preprocessedLoader
  val dataSet = AmrCorpusLoader.loadDataSet(loader, corpus, nExamples)

  val filteredDataSet = only match {
    case Some(ids) => dataSet.filter(item => ids.contains(item.id))
    case None => dataSet
  }

  filteredDataSet
}

def loadTrainData(
  train: String,
  alignmentLoader: String,
  tagDir: String,
  trainSize: Option[Int],
  maxSentenceLength: Int,
  only: Option[Set[String]] = None,
  supertagFile: String = "train_tags_pred.txt"
)(implicit random: Random, cs: CombinatorSystem): Seq[Example[AdjacencyMeaningGraph]] = {
  val loader = combineAlignments(alignmentLoader)(baseLoader)
  val trainSupertagFile = new File(tagDir, supertagFile)
  val trainSupertagDict = AttributeFileParser.parseFile(trainSupertagFile)

  val xLoader = loader
    .extendAttributes(trainSupertagDict)

  val dataSet = AmrCorpusLoader.loadDataSet(xLoader, train, trainSize, Some(maxSentenceLength))

  val filteredDataSet = only match {
    case Some(ids) => dataSet.filter(item => ids.contains(item.id))
    case None => dataSet
  }
  filteredDataSet
}

def loadTestData(
  test: String,
  testSize: Option[Int] = None,
  supertagFile: Option[File] = None
)(implicit random: Random, cs: CombinatorSystem): Seq[Example[AdjacencyMeaningGraph]] = {
  val loader = baseLoader

  val xLoader = supertagFile match {
    case Some(file) =>
      val supertagDict = AttributeFileParser.parseFile(file)
      loader.extendAttributes(supertagDict)
    case _ => loader
  }

  AmrCorpusLoader.loadDataSet(xLoader, test, testSize, shuffle=false)
}


def loadIdList(fileName: String): Set[String] = {
  var ids = Set.empty[String]
  val reader = new BufferedReader(new FileReader(new File(fileName)))
  var line = reader.readLine()
  while(line != null) {
    ids = ids + line
    line = reader.readLine()
  }
  reader.close()
  logger.info(s"Loaded ${ids.size} example IDs from $fileName")
  ids
}


def leafCategories(example: Example[AdjacencyMeaningGraph], span: Span): Iterable[SyntacticCategory] = {
  if(span.size == 1)
    if(span.start < example.ccgTags.size)
      example.ccgTags(span.start).map(_._1)
    else Iterable("N", "N/N").map(SyntacticCategory.apply)
  else Iterable.empty
}

// ------------------------------------------------------------------
// Parsing
// ------------------------------------------------------------------

implicit def graphInstance: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance
implicit def exampleInstance: gramr.text.Example[Example[AdjacencyMeaningGraph]] = Example.ops.exampleInstance
implicit def hasMrInstance: HasMR[Example[AdjacencyMeaningGraph], AdjacencyMeaningGraph] = Example.ops.hasMRInstance

def rootCategories(derivation: Derivation[Unit]): Iterable[SyntacticCategory] = {
  def rootStepCategories(step: DerivationStep[Unit]): Iterable[SyntacticCategory] = {
    val subStepCats = step.subSteps match {
      case subStep :: Nil => rootStepCategories(subStep)
      case _ => Iterable.empty
    }
    Iterable(step.synCat.eraseFeatures) ++ subStepCats
  }
  val cats = rootStepCategories(derivation.root)
  cats
}

val evaluationFunction = (mr: AdjacencyMeaningGraph, e: Example[AdjacencyMeaningGraph]) =>
  gramr.graph.evaluate.kernelSmatch(1, 4)(mr, e)


def costFunction(state: State[AdjacencyMeaningGraph, MapFeatureVector], example: Example[AdjacencyMeaningGraph]): Double = {
  val smatch = gramr.graph.evaluate.kernelSmatch[AdjacencyMeaningGraph, Example[AdjacencyMeaningGraph]](1, 0)(state.mr, example)
  1.0 - smatch.f1
}


def mkUpdateStrategy(update: String)(implicit fv: FeatureVector[MapFeatureVector]): UpdateStrategy[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] = {
  val earlyUpdateStrategy: UpdateStrategy[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] =
    new EarlyUpdateStrategy(freeChartName = "free", oracleChartName = "oracle")
  val earlyUpdateStrategy2: UpdateStrategy[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] =
    new EarlyUpdateStrategy2(badChartName = "free", goodChartName = "oracle")
  val costUpdateStrategy: UpdateStrategy[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] =
    new CostSensitiveUpdateStrategy(costFunction)

  update match {
    case "early" =>
      logger.info("Using early update strategy")
      earlyUpdateStrategy
    case "early2" =>
      logger.info("Using early2 update strategy")
      earlyUpdateStrategy2
    case "cost" =>
      logger.info("Using cost update strategy")
      costUpdateStrategy
    case "early+cost" =>
      logger.info("Using early+cost update strategy")
      UpdateStrategy.fallBack(
        UpdateStrategy.choose { (example: Example[AdjacencyMeaningGraph], charts: Map[String, Chart[AdjacencyMeaningGraph, MapFeatureVector]]) =>
          if(charts("free").outputStates.nonEmpty && charts("oracle").outputStates.nonEmpty) {
            val bestFree = charts("free").outputStates.map(state => evaluationFunction(state.mr, example).f1).max
            val bestOracle = charts("oracle").outputStates.map(state => evaluationFunction(state.mr, example).f1).max
            if(bestFree > bestOracle) costUpdateStrategy else earlyUpdateStrategy
          }
          else costUpdateStrategy
        },
        costUpdateStrategy
      )
  }
}


def mkLearner(learnerType: String, model: LinearModel)(implicit fv: FeatureVector[MapFeatureVector]): LinearLearner[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] = {
  learnerType match {
    case "adadelta" =>
      logger.info("Using adadelta learner")
      new AdadeltaLearner[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](model)
    case "perceptron" =>
      logger.info("Using perceptron learner")
      new PerceptronLearner[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](model)
  }
}

def emptyLearner(learnerType: String): LinearLearner[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] = {
  implicit val featureIndex: FeatureIndex = new FeatureHasher(100000)
  implicit val fv: FeatureVector[MapFeatureVector] = MapFeatureVector.featureVectorLike
  val initialParameters: LinearModel = new LinearModel(HashMap.empty[Int, Double], _ => 0.0)
  mkLearner(learnerType, initialParameters)
}

def loadLearner(learnerType: String, modelFile: String): LinearLearner[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] = {
  logger.info(s"Loading model $modelFile")
  val modelIn = new ObjectInputStream(new FileInputStream(new File(modelFile)))
  val model: LinearModel = modelIn.readObject().asInstanceOf[LinearModel]
  modelIn.close()
  implicit val featureIndex: FeatureIndex = model.featureIndex
  implicit val fv: FeatureVector[MapFeatureVector] = MapFeatureVector.featureVectorLike
  mkLearner(learnerType, model)
}

def mkOracleParser(
  oracleType: String,
  beam: Int,
  lexicalChoice: LexicalChoice[Example[MR], MR],
  binaryChoice: BinaryChoice[MR],
  actionFilter: ActionPredicate[MR, FV, Example[MR]],
  learner: LinearLearner[MR, FV, Example[MR]],
  featureSet: FeatureSet[MR, MapFeatureVector, Example[MR]]
)(implicit
  cs: CombinatorSystem,
  featureIndex: FeatureIndex,
  fv: FeatureVector[FV],
  bv: BaseVector[FV]
): Parser[MR, FV, Example[MR]] = {
  oracleType match {
    case "none" =>
      logger.info("Training without oracle")
      new ChartParser[MR, FV, Unit, Example[MR]] {
        override def parse(sentence: Vector[Token], context: Example[MR]): Chart =
          Chart.empty(sentence)

        override def label(decision: Decision[MR], span: Span, context: Example[MR]): Unit = ???

        override def actionFilter = ???

        override implicit def featureVector: FeatureVector[FV] = ???

        override implicit def chartLabel: ChartLabel[Unit] = ???

        override implicit def hasFeatures: HasFeatures[FV, Unit] = ???

        override implicit def labelOrdering: Ordering[Unit] = ???
      }
    case "heuristic" =>
      logger.info(s"Using heuristic oracle with beam size $beam")
      def heuristic(c: Example[AdjacencyMeaningGraph])(mr: AdjacencyMeaningGraph, span:Span): Double = {
        val p = ActionPredicates.GraphElementsCorrect.precision[AdjacencyMeaningGraph](mr, c.reference)
        val a = ActionPredicates.AlignmentsFulfilled.accuracy(mr, span, c)
        2.0 * (p * a / (p + a))
      }

      val heuristicParser = gramr.parse.postprocess.resolveCorefs(
        gramr.parse.cky.heuristic[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](
          lexicalChoice = lexicalChoice,
          binaryChoice = binaryChoice,
          combinatorSystem = cs,
          decisionFeatures = featureSet.derivationFeatures.extract,
          stateFeatures = featureSet.stateFeatures.extract,
          score = heuristic,
          actionFilter = actionFilter,
          leafCategories = leafCategories,
          dynamicBeamSize = _ => beam,
          branchingFactor = None,
          limitUnaryActions = false
        )
      )

      val heuristicTrimmed = new gramr.parse.MemorisingParser(
        gramr.parse.postprocess.keepBestEvaluatedParse(heuristicParser, (mr: AdjacencyMeaningGraph, e: Example[AdjacencyMeaningGraph]) => evaluationFunction(mr, e).f1)
      )
      heuristicTrimmed

    case "forced" =>
      logger.info(s"Using forced oracle with beam size $beam")

      val permittedErrorsValues = List(0, 3, 6, 9)
      val parsers = permittedErrorsValues map { permittedErrors =>
        val forcedDecodingFilter: ActionPredicate[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] =
          ActionPredicates.graphElementsCorrect(permittedErrors)

        gramr.parse.postprocess.resolveCorefs(
          gramr.parse.cky.learned[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](
            lexicalChoice = lexicalChoice,
            binaryChoice = binaryChoice,
            combinatorSystem = cs,
            decisionFeatures = featureSet.derivationFeatures.extract,
            stateFeatures = featureSet.stateFeatures.extract,
            modelScore = fv => learner.trainModel.score(fv),
            actionFilter = and(actionFilter, forcedDecodingFilter),
            leafCategories = leafCategories,
            dynamicBeamSize = _ => beam,
            branchingFactor = None,
            limitUnaryActions = false
          )
        )
      }

      new FallbackParser[MR, FV, Example[MR]](parsers)
  }
}

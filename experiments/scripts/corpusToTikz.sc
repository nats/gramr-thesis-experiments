import java.io.{File, FileWriter}
import $file.commonSettings
import $file.grammars
import $file.grammar.ccgbank
import $file.induction
import $file.saveSplitCharts
import ccgbank.CcgbankCombinatorSystem
import commonSettings._
import gramr.amr.AmrCorpusItem
import gramr.experiments.ScriptSupport._
import gramr.graph.presentation.ToTikz
import gramr.util.Logging
import saveSplitCharts.saveSplitCharts

import scala.util.Random


@main
def grammarCoverage(
  corpus: String,
  output: String,
  samples: Option[Int] = None,
  seed: Int = 1,
): Unit = {

  implicit val combinatorSystem = new CcgbankCombinatorSystem

  val outputDir = new File(output)
  outputDir.mkdirs()

  implicit val random: Random = new Random(seed)

  val examples = loadTestData(corpus, testSize=samples)

  logger.info(s"Saving .tex files")
  val toTikz = new ToTikz[MR]
  examples.foreach { example =>
    val texFile = new File(outputDir, s"${example.id}.tex")
    val writer = new FileWriter(texFile)
    val tikz = toTikz(example.reference)
    writer.write(tikz)
    writer.close()
  }

}

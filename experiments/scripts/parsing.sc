import java.io.{BufferedWriter, File, FileWriter, PrintWriter}
import $file.commonSettings
import $file.grammars
import commonSettings._
import gramr.ccg.{CombinatorSystem, SimpleCombinatorSystem}
import gramr.construction.sgraph.SGraphRule
import gramr.experiments.Example
import gramr.experiments.ScriptSupport.{logger, _}
import gramr.graph.AdjacencyMeaningGraph
import gramr.graph.Predicates.{checkArgEdges, checkConstantDegrees, checkInstanceConcepts, checkNumberedEdgesUnique, checkPolarityEdges}
import gramr.jobs
import gramr.jobs.{Listener, NamedParses}
import gramr.jobs.ops._
import gramr.learn.util.FeatureIndex
import gramr.learn.{BaseVector, FeatureVector, MapFeatureVector, UpdateStrategy}
import gramr.learn.features.FeatureSet
import gramr.parse.Implicits._
import gramr.parse.{ActionPredicate, ActionPredicates, Parser, PatternChoice, and}
import gramr.util.{Logging, ParseStatsCsvLogger}

import scala.util.Random


@main
def main(
  train: String,
  validate: String,
  lexicon: String,
  tags: String,
  output: String,
  grammar: String = "all",
  features: String = "all",
  alignments: String = "jamr+tamr+amr_ud+isi-vote1",
  learner: String = "adadelta",
  update: String = "early",
  oracle: String = "forced",
  bootstrap: String = "none",
  trainSize: Option[Int] = None,
  valSize: Option[Int] = None,
  arityCheck: Boolean = true,
  maxSentenceLength: Int = 40,
  beam: Int = 15,
  oracleBeam: Int = 20,
  iterations: Int = 5,
  batchSize: Int = 64,
  parallelism: Option[Int] = None,
  seed: Int = 1,
  ids: Option[String] = None,
  testSentenceRejectLength: Int = 25,
  saveDerivations: Boolean = false,
  saveCharts: Boolean = false
): Unit = {

  implicit val random: Random = new Random(seed)
  implicit val cs: CombinatorSystem = new SimpleCombinatorSystem

  logger.info("Starting parsing experiment")
  logger.info(s"grammar=$grammar alignments=$alignments features=$features")

  val outputDir = new File(output)

  val logPath = outputDir
  Logging.setupLogging(logPath.toString)

  // We do not need to set maxCorefs as we don't do induction
  val rules: Vector[SGraphRule[MR]] = grammars.selectGrammar[MR](grammar, maxCorefs = 0)


  // Load data

  val idList = ids.map(loadIdList)

  val trainExamples = loadTrainData(train, alignments, tags, trainSize, maxSentenceLength, only = idList)
  val valExamples = loadTestData(validate, valSize, supertagFile = Some(new File(tags, "val_tags_pred.txt")))


  // Load lexicon

  logger.info(s"Loading lexicon from $lexicon")
  val lexicalChoice = loadLexicon(new File(lexicon), wildcardFillersParsing)

  val actualParallelism = parallelism.getOrElse(  // default: allow at least 2GB per parsed example)
    Integer.min(
      Runtime.getRuntime.availableProcessors(),
      (Runtime.getRuntime.maxMemory() / (1024L * 1024L * 1024L * 2.0)).toInt))
  logger.info(s"Parsing with parallelism level $actualParallelism")


  // Build learner

  val learnerInstance = emptyLearner(learner)
  implicit val featureIndex: FeatureIndex = learnerInstance.trainModel.featureIndex
  implicit val fv: FeatureVector[MapFeatureVector] = MapFeatureVector.featureVectorLike
  implicit val bv: BaseVector[MapFeatureVector] = fv


  // Build parsers
  var actionFilter: ActionPredicate[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] = and(
    // limitNamedEntities(NamedEntities.proxyReductions),
    ActionPredicates.rootCategoryFilter(example => example.derivations.flatMap(rootCategories).toSet),
    // ActionPredicates.rootCategoryFilter(_ => Set("S", "S[dcl]", "S[wq]", "S[q]", "NP", "N").map(SyntacticCategory.apply)),
    ActionPredicates.mrPredicate(mr => checkPolarityEdges(mr) && checkConstantDegrees(mr) && checkArgEdges(mr) && checkNumberedEdgesUnique(mr) && checkInstanceConcepts(mr))
  )
  if(arityCheck) {
    actionFilter = and(actionFilter, ActionPredicates.matchSyntacticArity)
  }

  val combinatoryChoice = new PatternChoice[MR](cs, rules)

  val featureSet: FeatureSet[MR, MapFeatureVector, Example[MR]] = selectFeatureSet(features)


  // Build bootstrap oracle parser

  val bootstrapParser = bootstrap match {
    case "none" => None
    case _ =>
      Some(
        mkOracleParser(bootstrap, oracleBeam, lexicalChoice, combinatoryChoice, actionFilter, learnerInstance, featureSet)
      )
  }


  // Build oracle parser

  val oracleParser = mkOracleParser(oracle, oracleBeam, lexicalChoice, combinatoryChoice, actionFilter, learnerInstance, featureSet)


  // Build learned parser

  val updateStrategy = mkUpdateStrategy(update)

  logger.info(s"Using beam size $beam for decoding")
  val trainDecoder = gramr.parse.postprocess.resolveCorefs(
    gramr.parse.cky.learned[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](
      lexicalChoice = lexicalChoice,
      binaryChoice = combinatoryChoice,
      combinatorSystem = cs,
      decisionFeatures = featureSet.derivationFeatures.extract,
      stateFeatures = featureSet.stateFeatures.extract,
      modelScore = fv => learnerInstance.trainModel.score(fv),
      actionFilter = actionFilter,
      leafCategories = leafCategories,
      dynamicBeamSize = _ => beam,
      branchingFactor = None,
      limitUnaryActions = false
    ))

  val valDecoder = gramr.parse.postprocess.resolveCorefs(
    gramr.parse.cky.learned[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](
      lexicalChoice = lexicalChoice,
      binaryChoice = combinatoryChoice,
      combinatorSystem = cs,
      decisionFeatures = featureSet.derivationFeatures.extract,
      stateFeatures = featureSet.stateFeatures.extract,
      modelScore = fv => learnerInstance.testModel.score(fv),
      actionFilter = actionFilter,
      leafCategories = leafCategories,
      dynamicBeamSize = _ => beam,
      branchingFactor = None,
      limitUnaryActions = false
    )).rejecting(_.sentence.length > testSentenceRejectLength)


  // First define what an iteration looks like
  def trainValIteration(
    iterationId: String,
    trainDecoder: Parser[MR, FV, Example[MR]],
    oracleParser: Parser[MR, FV, Example[MR]],
    updateStrategy: UpdateStrategy[MR, FV, Example[MR]]
  ): Unit = {
    val trainStatsLogger = new ParseStatsCsvLogger(new File(logPath, s"$iterationId-train-stats.csv"))
    val trainAmrFile = new File(logPath, s"$iterationId-train-amr.txt")
    val trainAmrWriter = new PrintWriter(new BufferedWriter(new FileWriter(trainAmrFile)))

    def updateCondition(namedCharts: NamedParses[MR, FV], example: Example[MR]) = {
      if(update.contains("cost")) {
        true  // cost can update with only one chart => never block it
      }
      else {
        val oracleStateO = namedCharts("oracle").outputStates.headOption
        val freeStateO = namedCharts("free").outputStates.headOption
        (oracleStateO, freeStateO) match {
          case (Some(oracleState), Some(freeState)) =>
            // Update if oracle parse is better than free parse
            evaluationFunction(oracleState.mr, example).f1 > evaluationFunction(freeState.mr, example).f1
          case (Some(_), None) => true // Update because no free parse was found
          case _ => false // Don’t update if no oracle parse was found
        }
      }
    }

    logger.info(s"Training iteration $iterationId")

    val shuffledExamples = random.shuffle(trainExamples)
    var trainParseJob = jobs.Job(shuffledExamples, s"$iterationId-train")
      .dualParse(trainDecoder, oracleParser, actualParallelism, inOrder = false, firstName = "free", secondName = "oracle")
      .progressBar
      .addListener(Listener.evaluate(
        evaluationFunction,
        parseConsumers = Seq(Listener.logItemStatsToCsv("train", trainStatsLogger)),
        iterationConsumers = Seq(
          Listener.printIterationStats("train"),
          Listener.logIterationStatsToCsv("train", trainStatsLogger)
        )
      ))
      .logAmr(trainAmrWriter, evaluationFunction)
    if(saveDerivations) trainParseJob = trainParseJob.logTopDerivationAsHtml(logPath)
    if(saveCharts) trainParseJob = trainParseJob.logChartAsHtml(logPath)
    val trainUpdateJob = trainParseJob
      .generateUpdates(updateStrategy, condition = updateCondition)
      .applyTo(learnerInstance)
    trainUpdateJob.runBatched(batchSize)

    trainStatsLogger.close()
    trainAmrWriter.close()


    val modelFileName = s"$iterationId-model.ser"
    logger.info(s"Saving model: $modelFileName")

    import java.io.{FileOutputStream, ObjectOutputStream}

    val modelOut: ObjectOutputStream = new ObjectOutputStream(new FileOutputStream(new File(logPath, modelFileName)))
    modelOut.writeObject(learnerInstance.trainModel)
    modelOut.close()


    val valStatsLogger = new ParseStatsCsvLogger(new File(logPath, s"$iterationId-val-stats.csv"))
    val valAmrFile = new File(logPath, s"$iterationId-val-amr.txt")
    val valAmrWriter = new PrintWriter(new BufferedWriter(new FileWriter(valAmrFile)))

    logger.info(s"Validating iteration $iterationId")
    var valParseJob = jobs.Job(valExamples, s"$iterationId-val")
      .parse(valDecoder, actualParallelism, inOrder = false)
      .progressBar
      .logAmr(valAmrWriter, evaluationFunction)
    if(saveDerivations) valParseJob = valParseJob.logTopDerivationAsHtml(logPath)
    if(saveCharts) valParseJob = valParseJob.logChartAsHtml(logPath)
    val valEvalJob = valParseJob
      .evaluate(evaluationFunction)
      .printIterationStats
      .logStatsToCsv(valStatsLogger)
    valEvalJob.run()

    valStatsLogger.close()
    valAmrWriter.close()
  }

  // Optional bootstrap iteration
  bootstrapParser.foreach { parser =>
    trainValIteration("bootstrap", trainDecoder, parser, updateStrategy)
  }

  // Training iterations
  for(i <- 1 to iterations) {
    trainValIteration(s"$i", trainDecoder, oracleParser, updateStrategy)
  }
}

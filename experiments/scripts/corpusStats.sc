/** Induce from a single example and derivation, and save the resulting split
chart. */

import $ivy.`io.circe::circe-core:0.12.3`
import $ivy.`io.circe::circe-generic:0.12.3`
import $file.commonSettings
import $file.grammars
import $file.grammar.ccgbank
import commonSettings._

import java.io.{File, FileWriter, PrintWriter}
import gramr.ccg.{CombinatorSystem, CombinatoryStep, Derivation}
import gramr.experiments.Example
import gramr.experiments.ScriptSupport._
import gramr.util.Logging

import scala.util.Random
import io.circe._
import io.circe.generic.auto._
import io.circe.syntax._
import ccgbank.CcgbankCombinatorSystem


implicit val combinatorSystem: CombinatorSystem = new CcgbankCombinatorSystem


/** Print statistics about a corpus, such as number of sentences, CCG combinator stats. */
@main
def corpusStats(
  corpus: String,
  output: String,
  grammar: String = "all",
  alignments: String = "jamr+tamr+amr_ud+isi-vote1",
  maxCorefs: Int = 1,
  seed: Int = 1,
): Unit = {

  val outputDir = new File(output)

  logger.info(s"Selected alignments: $alignments")

  val logPath = outputDir
  Logging.setupLogging(logPath.toString)

  val rules = grammars.selectGrammar(grammar, maxCorefs = maxCorefs)

  logger.info(s"Selected grammar: $grammar")

  implicit val random: Random = new Random(seed)

  val examples = loadInductionData(corpus, alignments)

  val combinatorCountsAll: CombinatorCounts = examples.par.map { example =>
    example.item.ccgDerivations.map(_._2).map(countCombinators).fold(CombinatorCounts.empty)(addCounts)
  }.fold(CombinatorCounts.empty)(addCounts)

  val data = CorpusStats(
    exampleCount = examples.size,
    examples = examples.map(e => (e.id, ExampleStats.of(e))).toMap,
    combinators = CombinatorStats(all = combinatorCountsAll)
  )

  val outputFileName = java.nio.file.Paths.get(corpus).getFileName + ".json"
  val jsonFile = new File(outputDir, outputFileName)
  val jsonWriter: PrintWriter = new PrintWriter(new FileWriter(jsonFile))
  jsonWriter.println(data.asJson.toString)
  jsonWriter.close()

}


case class CorpusStats(
  exampleCount: Int,
  examples: Map[String, ExampleStats],
  combinators: CombinatorStats
)


case class ExampleStats(
  sentenceLength: Int
)

case class CombinatorStats(
  all: CombinatorCounts
)

type CombinatorCounts = Map[String, Int]

object CombinatorCounts {
  def empty: CombinatorCounts = Map.empty
}

object ExampleStats {
  def of(example: Example[MR]): ExampleStats = {
    ExampleStats(
      example.sentence.length
    )
  }
}

def countCombinators(derivation: Derivation[Unit]): CombinatorCounts = {
  derivation.steps.map {
    case CombinatoryStep(combinator, subSteps, synCat, span, label) =>
      Map(combinator.toString -> 1)
    case _ => Map.empty[String, Int]
  }.fold(Map.empty[String, Int]) { case (l, r) => addCounts(l, r) }
}

def addCounts(l: CombinatorCounts, r: CombinatorCounts): CombinatorCounts = {
  r.foldLeft(l) { case (acc, (name, count)) => acc + (name -> (acc.getOrElse(name, 0) + count)) }
}


/** Compute the token-wise coverage of every possible alignment setting.
  */

import java.io.{File, FileWriter, PrintWriter}

import $file.commonSettings
import $file.grammars
import $file.induction
import commonSettings._
import gramr.ccg.{CombinatorSystem, SimpleCombinatorSystem}
import gramr.experiments.Example
import gramr.experiments.ScriptSupport._
import gramr.graph.{AlignmentsAttribute, Indexed, Node}
import gramr.util.Logging

import scala.util.Random


implicit val combinatorSystem: CombinatorSystem = new SimpleCombinatorSystem


@main
def alignerCoverage(
  corpus: String,
  output: String,
  alignments: String = "all",
  seed: Int = 1
): Unit = {

  val outputDir = new File(output)

  val logPath = outputDir
  Logging.setupLogging(logPath.toString)

  implicit val random: Random = new Random(seed)

  val alignmentSpecs = if(alignments == "all") allAlignmentSpecs else alignments.split(",").toSeq

  logger.info("Looping through specs: " + alignmentSpecs.mkString(", "))

  val statMap = alignmentSpecs map { spec =>
    val stats = alignmentConfigurationStats(corpus, spec)
    (spec, stats)
  }

  val outFile = new File(output, "alignerCoverage.csv")
  val writer = new PrintWriter(new FileWriter(outFile))
  writer.println("alignments,covered,total")
  statMap.foreach {
    case (spec, stats) =>
      writer.println(s"$spec,${stats.covered},${stats.total}")
  }
  writer.close()
}


def allAlignmentSpecs: Seq[String] = {
  // enumerate all combinations of aligners
  val aligners = Set("jamr", "tamr", "amr_ud", "isi")
  val alignerCombinations = aligners.subsets.filter(_.nonEmpty)
  alignerCombinations.flatMap { aligners =>
    if(aligners.size == 1) {
      aligners
    }
    else {
      (1 to aligners.size).map { i =>
        aligners.mkString("+") + s"-vote$i"
      }
    }
  }.toSeq
}


def alignmentConfigurationStats(corpus: String, alignments: String)(implicit random: Random): Stats = {
  val examples = loadInductionData(corpus, alignments)

  val stats = examples.foldLeft(Stats.empty) {
    case (stats, example) =>
      stats + exampleStats(example)
  }

  logger.info(s"$alignments: ${stats.covered} / ${stats.total} nodes aligned (ratio: ${stats.covered.toDouble / stats.total.toDouble})")

  stats
}


def exampleStats(example: Example[MR]): Stats = {
  def isAligned(node: Indexed[Node]): Boolean = {
    node.element.attributes.contains("alignments") &&
      node.element.attributes.get("alignments").asInstanceOf[AlignmentsAttribute].edges.nonEmpty
  }

  val aligned = example.reference.nodes.count(isAligned)
  val total = example.reference.nodes.size
  Stats(covered = aligned, total = total)
}


case class Stats(
  covered: Int,
  total: Int
) {

  def +(other: Stats): Stats = copy(
    covered = covered + other.covered,
    total = total + other.total
  )

}


object Stats {
  def empty: Stats = Stats(0, 0)
}

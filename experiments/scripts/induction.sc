import java.io.{BufferedReader, BufferedWriter, File, FileReader, FileWriter, PrintWriter}

import $ivy.`org.apache.commons:commons-csv:1.5`
import org.apache.commons.csv.CSVFormat
import $file.commonSettings
import commonSettings._
import gramr.ccg.StoredLexicon.KeyExtractor
import gramr.ccg.{DelexedLexicon, Derivation, StoredLexicon}
import gramr.construction.sgraph.SGraphRule
import gramr.experiments.ScriptSupport._
import gramr.experiments._
import gramr.graph.presentation.ops._
import gramr.jobs.Job
import gramr.jobs.ops._
import gramr.split.em.{DelexicalizedEstimator, DelexicalizedModel}
import gramr.split.filters.MatchSyntacticArity
import gramr.split.labelling.{CompleteNodeAlignments, LabelAlignments}
import gramr.split.{PatternBasedSplitGenerator, SentenceSplittingAlgorithm, SplitChartFilter, SplitFilter, SplitGenerator}
import gramr.tagging.{Layer, TaggedDataSet}
import gramr.util.SparseVector

import scala.collection.mutable.ArrayBuffer
import scala.collection.concurrent.TrieMap

type ExampleDerivationPair = (Example[MR], (String, Derivation[Unit]))


/** Runs induction and delexicalisation.
  */
def induceDelex(
  filteredExampleDerivationPairs: Seq[ExampleDerivationPair],
  splittingAlgorithm: SentenceSplittingAlgorithm[MR, Example[MR]],
  chartFilter: Example[MR] => SplitChartFilter[MR],
  parallelism: Int
): DelexedLexicon[MR] = {
  logger.info("Inducing lexicalized lexicon")

  val lexicalizedLexicon = splitPass2(filteredExampleDerivationPairs,
      splittingAlgorithm, chartFilter, gramr.ccg.StoredLexicon.defaultKeyExtractor, parallelism)

  logger.info(s"Lexicalized lexical entry count: ${lexicalizedLexicon.size}")

  logger.info("Delexicalizing")

  val delexedLexicon = gramr.graph.lexemes.delexicalizeLexicon(
    lexicalizedLexicon,
    maxLexNodes = 1,
    wildcardFillers = wildcardFillers)

  logger.info(s"Delexicalized template count: ${delexedLexicon.delexed.itemCount}")
  logger.info(s"Delexicalized lexeme count  : ${delexedLexicon.lexemes.size}")

  delexedLexicon
}


def emFilter(
  filteredExampleDerivationPairs: Seq[ExampleDerivationPair],
  delexedLexicon: DelexedLexicon[MR],
  splittingAlgorithm: SentenceSplittingAlgorithm[MR, Example[MR]],
  emIterations: Int,
  outputPath: File,
  parallelism: Int
): DelexedLexicon[MR] = {
  logger.info(s"Estimating EM parameters")

  val parameters = estimateEmParameters(
    exampleDerivationPairs=filteredExampleDerivationPairs,
    lexicon=delexedLexicon,
    splittingAlgorithm=splittingAlgorithm,
    iterations=emIterations
  )

  logger.info(s"Saving EM parameters to $outputPath")
  writeDelexedParameters(parameters, delexedLexicon, outputPath)

  val (filteredLexicon, taggedDataSet) = filteringPass(
    filteredExampleDerivationPairs,
    lexicon=delexedLexicon,
    splittingAlgorithm=splittingAlgorithm,
    parameters=parameters,
    parallelism=parallelism
  )
  logger.info(s"Filtered template count: ${filteredLexicon.delexed.itemCount}")
  logger.info(s"Filtered lexeme count  : ${filteredLexicon.lexemes.size}")

  logger.info(s"Saving filtered lexicon to $outputPath")
  filteredLexicon.save(
    templateFile = new File(outputPath, "templates.txt"),
    lexemeFile = new File(outputPath, "lexemes.txt")
  )

  val supertagsOutputFile = new File(outputPath, "train_tags_gold.txt")
  logger.info(s"Saving super tagger training data to $supertagsOutputFile")
  // Save training data for the super tagger
  saveSupertaggerTrainSet(
    taggedDataSet,
    filteredExampleDerivationPairs,
    supertagsOutputFile
  )


  filteredLexicon
}


def loadDelexedParameters(
  path: File,
  lexicon: DelexedLexicon[MR],
  maxLexNodes: Int
): DelexicalizedModel[MR] = {
  def loadParameters(file: File): SparseVector = {
    import scala.collection.JavaConverters._

    val reader = new BufferedReader(new FileReader(file))
    val records = CSVFormat.DEFAULT.withHeader().parse(reader)
    val params = records.asScala.map { record =>
      (record.get("id").toInt, record.get("probability").toDouble)
    }.toMap
    reader.close()
    SparseVector(params)
  }

  val tmplParameters = loadParameters(new File(path, "tmpl_parameters.csv"))
  val lexParameters = loadParameters(new File(path, "lex_parameters.csv"))

  val parameters = DelexicalizedEstimator.Parameters(tmplParameters, lexParameters)

  new DelexicalizedModel[MR](parameters, lexicon.delexed, lexicon.lexemes, maxLexNodes)
}


def writeDelexedParameters(parameters: DelexicalizedModel[MR], filteredLexicon: DelexedLexicon[MR], outputPath: File): Unit = {
  def writeTmplParameters(parameters: DelexicalizedModel[MR], filteredLexicon: DelexedLexicon[MR], file: File): Unit = {
    val writer = new PrintWriter(new BufferedWriter(new FileWriter(file)))
    writer.println("id,probability,tokens,length,syncat,amr")

    filteredLexicon.delexed.items foreach { item =>
      val toks = item.tokens.map(_.text.replace("\"", "&quot;")).mkString(" ")
      val amr = item.meaning.toString.replaceAll("""\s+""", " ").replace("\"", "&quot;")
      writer.println(s""""${item.index}",${parameters.parameters.templateParameters(item.index)},"$toks",${item.tokens.size},"${item.synCat}","$amr"""")
    }

    writer.close()
  }

  def writeLexParameters(parameters: DelexicalizedModel[MR], filteredLexicon: DelexedLexicon[MR], file: File): Unit = {
    val writer = new PrintWriter(new BufferedWriter(new FileWriter(file)))
    writer.println("id,probability,tokens,nodes")

    filteredLexicon.lexemes.wildcardFillers foreach { item =>
      writer.println(s""""${item.id}",${parameters.parameters.lexemeParameters(item.id)},"*","${item.filler.toString}"""")
    }
    filteredLexicon.lexemes.lexemeItems foreach { item =>
      val toks = item.lexeme.tokens.map(_.replace("\"", "&quot;")).mkString(" ")
      val nodes = item.lexeme.labels.map(_.replace("\"", "&quot;")).mkString(" ")
      writer.println(s""""${item.id}",${parameters.parameters.lexemeParameters(item.id)},"$toks","$nodes"""")
    }

    writer.close()
  }

  writeTmplParameters(parameters, filteredLexicon, new File(outputPath, "tmpl_parameters.csv"))
  writeLexParameters(parameters, filteredLexicon, new File(outputPath, "lex_parameters.csv"))
}



/**
 * Define the splitting algorithm.
 *
 * A splitting algorithm defines how partial MRs are extracted from sentential MRs.
 * Here, we use an implementation that recursively splits meaning representations according to a syntactic derivation
 * and word-node alignments.
 *
 * @param rules the set of syntactic-semantic rules
 * @param maxUnalignedNodes stop the algorithm if more than this number of nodes have no aligned words
 *                          (to avoid search space explosion)
 * @param maxItemCount stop the algorithm if more than this number of lexical items are produced from a single sentence
 *                     (to safely abort in case of search space explosion)
 * @param arityCheck allow only items where the semantic arity is less than or equal to the syntactic arity
 * @return a SplittingAlgorithm which can be used to run splitting passes
 */
def mkSplittingAlgorithm(
  rules: Vector[SGraphRule[MR]],
  maxUnalignedNodes: Option[Int] = None,
  maxItemCount: Option[Int] = None,
  arityCheck: Boolean = true
): SentenceSplittingAlgorithm[MR, Example[MR]] = {
  logger.info("Split Filter Options:")
  logger.info("- Connectedness filter enabled")
  var splitFilter: SplitFilter[MR] = gramr.split.filters.Connected()

  if(arityCheck) {
    logger.info("- Arity checking enabled (semantic arity <= syntactic arity)")
    splitFilter = splitFilter and MatchSyntacticArity.lessOrEqual
  }
  else {
    logger.info("- Arity checking disabled")
  }

  def splitGenerator(example: Example[MR]): SplitGenerator[MR] = {
    val labellingFunction = LabelAlignments() andThen CompleteNodeAlignments(maxUnalignedNodes)
    new PatternBasedSplitGenerator(labellingFunction, rules)
  }

  new gramr.split.RecursiveSplitting(splitGenerator, splitFilter, maxItemCount)
}

case class ExampleSplitStats(
  exampleId: String,
  derivationName: String,
  tokens: Int,
  tokenLevelCoverage: Double
)

def loadSplitStats(file: File): Seq[ExampleSplitStats] = {
  import scala.collection.JavaConverters._

  val reader = new BufferedReader(new FileReader(file))
  val records = CSVFormat.DEFAULT.withHeader().parse(reader)
  val stats = records.asScala.map { record =>
    ExampleSplitStats(
      record.get("exampleId"),
      record.get("derivationName"),
      record.get("sentenceLength").toInt,
      record.get("tokenLevelCoverage").toDouble
    ) }
  reader.close()
  stats.toSeq
}

/** Write table of token-level coverage  for every derivation
  *
  * @param stats rows to write
  * @param file file to write to
  */
def writeSplitStats(stats: Seq[ExampleSplitStats], file: File): Unit = {
  val writer = new PrintWriter(new BufferedWriter(new FileWriter(file)))
  writer.println("exampleId,derivationName,sentenceLength,tokenLevelCoverage")

  stats foreach {
    case ExampleSplitStats(exampleId, derivationName, sentenceLength, tokenLevelCoverage) =>
      writer.println(s""""$exampleId",$derivationName,$sentenceLength,$tokenLevelCoverage""")
  }

  writer.close()
}

def splitPass1(
  exampleDerivationPairs: Seq[ExampleDerivationPair],
  splittingAlgorithm: SentenceSplittingAlgorithm[MR, Example[MR]],
  chartFilter: Example[MR] => SplitChartFilter[MR],
  parallelism: Int,
  outputPath: File,
  dumpCharts: Boolean = false
): Map[String, Seq[ExampleSplitStats]] = {

  logger.info("Splitting pass 1: Computing quality of syntactic derivations")
  val tokenLevelCoverages = ArrayBuffer.empty[(String, String, Double)]

  var job =
  Job(exampleDerivationPairs, "split-1")
    .split(
      splitter = splittingAlgorithm,
      parallelism = parallelism,
      inOrder = false
    )
    .progressBar
    .filter(chartFilter)
    .trackTokenLevelCoverage(tokenLevelCoverages)

  if(dumpCharts) {
    val splitChartDir = new File(outputPath, "split1")
    splitChartDir.mkdir()
    job = job.dumpSplitCharts(splitChartDir, "")
  }

  job.run()

  // Compile the results into a sequence of split stat objects
  val tokenLevelCoverageMap: Map[(String, String), Double] = tokenLevelCoverages.groupBy(entry => (entry._1, entry._2)).mapValues(_.head._3)
  val stats = exampleDerivationPairs.map {
    case (example, (derivationName, _)) =>
      val tokenLevelCoverage = tokenLevelCoverageMap((example.id, derivationName))
      ExampleSplitStats(example.id, derivationName, example.sentence.length, tokenLevelCoverage)
  }
  writeSplitStats(stats, new File(outputPath, "split1_stats.csv"))

  val statsMap = stats.groupBy(_.exampleId)
  // Compute average token-level coverage for the best derivations
  val averageCoverage = statsMap.values.map(_.map(_.tokenLevelCoverage).max).sum / statsMap.size
  logger.info(s"Average best token-level coverage: $averageCoverage")

  statsMap
}

def splitPass2(
  filteredExampleDerivationPairs: Seq[ExampleDerivationPair],
  splittingAlgorithm: SentenceSplittingAlgorithm[MR, Example[MR]],
  chartFilter: Example[MR] => SplitChartFilter[MR],
  keyExtractor: KeyExtractor,
  parallelism: Int
): StoredLexicon[MR] = {
    logger.info("Splitting pass 2: Splitting with only the best derivations")

  val lexiconBuilder = new gramr.ccg.StoredLexicon.Builder[MR](keyExtractor = keyExtractor)

  Job(filteredExampleDerivationPairs, "split-2")
    .split(
      splitter = splittingAlgorithm,
      parallelism = parallelism,
      inOrder = false
    )
    .progressBar
    .filter(chartFilter)
    .addToLexicon(lexiconBuilder, eraseSyntacticAttributes = true)
    .run()

  val lexicalizedLexicon = lexiconBuilder.build

  lexicalizedLexicon
}

def filterDerivationsByTokenLevelCoverage(
  stats: Map[String, Seq[ExampleSplitStats]],
  examples: Seq[Example[MR]]
): Iterable[ExampleDerivationPair] = {
  // Map exampleIDs to the ID of the best derivation
  val bestDerivations: Map[String, String] = stats.mapValues(_.maxBy(_.tokenLevelCoverage).derivationName)
  for {
    example <- examples
    derivationId <- bestDerivations.get(example.id)
  } yield {
    val derivation = example.ccgDerivation(derivationId).get

    (example, (derivationId, derivation))
  }
}

def estimateEmParameters(
  exampleDerivationPairs: Seq[ExampleDerivationPair],
  lexicon: DelexedLexicon[MR],
  splittingAlgorithm: SentenceSplittingAlgorithm[MR, Example[MR]],
  iterations: Int
): DelexicalizedModel[MR] = {
  val estimator = new DelexicalizedEstimator[MR, Example[MR]](
    exampleDerivationPairs,
    lexicon = lexicon.delexed,
    lexemes = lexicon.lexemes,
    maxLexNodeCount = 1,
    sentenceSplitter = splittingAlgorithm,
    iterations = iterations
  )
  estimator.estimate
}

def filteringPass(
  exampleDerivationPairs: Seq[ExampleDerivationPair],
  lexicon: DelexedLexicon[MR],
  splittingAlgorithm: SentenceSplittingAlgorithm[MR, Example[MR]],
  parameters: DelexicalizedModel[MR],
  parallelism: Int
): (DelexedLexicon[MR], TaggedDataSet) = {
  logger.info("Final pass: creating filtered lexicon and saving supertagger training data")

  val bestLexemeIds = TrieMap.empty[Int, Unit]
  val bestTemplateIds = TrieMap.empty[Int, Unit]

  val taggedDataSetBuilder = new TaggedDataSet.Builder

  Job(exampleDerivationPairs, "filter")
    .split(
      splitter = splittingAlgorithm,
      parallelism = parallelism,
      inOrder = true
    )
    .progressBar
    .createTaggedDataSet(taggedDataSetBuilder, parameters, lexicon)
    .listenToItems {
      case (example, (derivationName, Right(splitChart))) =>
        logger.trace(s"Example ${example.id}: Finding best lexemes and templates")
        val best = parameters.bestSplit(splitChart)
        for((entry, lexeme, template) <- best) {
          val tokens = entry.span.in(splitChart.sentence)
          val lexemeId = DelexicalizedModel.lexemeId(tokens, lexicon.lexemes)(lexeme)
          val templateIds = parameters.templateIds(entry, template)
          if(lexemeId.nonEmpty && templateIds.nonEmpty) {
            bestLexemeIds.putIfAbsent(lexemeId.get, ())
            templateIds.foreach { templateId =>
              bestTemplateIds.putIfAbsent(templateId, ())
            }
          }
          else {
            logger.warn(s"Example ${example.id} derivation $derivationName: Best split contains entry that is out of lexicon: $lexeme ${entry.partialMR.synCats}\n$template")
          }
        }
        logger.trace(s"Example ${example.id}: added best lexemes and templates")
      case (example, (derivationName, Left(splitError))) =>
        logger.debug(s"Skipping example ${example.id} derivation $derivationName: $splitError")
    }
    .run()

  val filteredLexicon = new DelexedLexicon(
    lexicon.delexed.filter(item => bestTemplateIds contains item.index),
    lexicon.lexemes.filter { item => bestLexemeIds contains item.id }
  )

  val taggedDataSet = taggedDataSetBuilder.build

  (filteredLexicon, taggedDataSet)
}

def saveSupertaggerTrainSet(
  taggedDataSet: TaggedDataSet,
  exampleDerivationPairs: Seq[ExampleDerivationPair],
  file: File
): Unit = {
  val examples = exampleDerivationPairs.map(_._1)
  val goldSyncats: Iterable[Vector[String]] = examples.map(_.ccgTags.map(_.map(_._1).headOption.getOrElse("UNK").toString))

  taggedDataSet
    .addLayer(goldSyncats.map(tags => Layer("syncat", tags)))
    .save(file)
}
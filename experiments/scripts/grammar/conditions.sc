import gramr.ccg.{SyntacticCategory, ForwardSlash, BackwardSlash}


val trNp1: SyntacticCategory = SyntacticCategory("""S/(S\NP)""")

def isTrNp1(syncat: SyntacticCategory): Boolean = {
  syncat.matches(trNp1)
}

val trNp2: SyntacticCategory = SyntacticCategory("""(S\NP)\((S\NP)/NP)""")

def isTrNp2(syncat: SyntacticCategory): Boolean = {
  syncat.matches(trNp2)
}

def isForwardModifier(syncat: SyntacticCategory): Boolean = {
  syncat match {
    case ForwardSlash(x, y) if x.matches(y) => true
    case _ => false
  }
}

def isBackwardModifier(syncat: SyntacticCategory): Boolean = {
  syncat match {
    case BackwardSlash(x, y) if x.matches(y) => true
    case _ => false
  }
}

def determiner: SyntacticCategory = SyntacticCategory("NP/N")

def isDeterminer(syncat: SyntacticCategory): Boolean = syncat.matches(determiner)

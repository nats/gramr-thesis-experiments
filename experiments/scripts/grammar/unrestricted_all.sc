import gramr.construction.SyntacticContext
import gramr.construction.sgraph.SGraphRule
import gramr.graph.Graph
import gramr.graph.edit.sgraph.Operators

def rules[MR: Graph](maxCorefs: Int = 1): Vector[SGraphRule[MR]] =
  Vector(
    new SGraphRule[MR](
      "forward apply",
      (context: SyntacticContext) => true,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "backward apply",
      (context: SyntacticContext) => true,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = true
    ),
    new SGraphRule[MR](
      "forward modify",
      (context: SyntacticContext) => true,
      Operators.Modification[MR](depth = 1, maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "backward modify",
      (context: SyntacticContext) => true,
      Operators.Modification[MR](depth = 1, maxCorefs = maxCorefs),
      backward = true
    ),
    new SGraphRule[MR](
      "forward compose",
      (context: SyntacticContext) => true,
      Operators.Composition[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "backward compose",
      (context: SyntacticContext) => true,
      Operators.Composition[MR](maxCorefs = maxCorefs),
      backward = true
    ),
    new SGraphRule[MR](
      "forward substitute",
      (context: SyntacticContext) => true,
      Operators.Substitution[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "backward substitute",
      (context: SyntacticContext) => true,
      Operators.Substitution[MR](maxCorefs = maxCorefs),
      backward = true
    ),
    new SGraphRule[MR](
      "forward ignore",
      (context: SyntacticContext) => true,
      Operators.Identity[MR](),
      backward = false
    ),
    new SGraphRule[MR](
      "backward ignore",
      (context: SyntacticContext) => true,
      Operators.Identity[MR](),
      backward = true
    ),
  )

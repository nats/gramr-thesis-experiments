import $file.conditions
import conditions._

import gramr.ccg.SyntacticCategory
import gramr.ccg.EasyccgCombinatorSystem._
import gramr.construction.SyntacticContext
import gramr.construction.sgraph.SGraphRule
import gramr.graph.Graph
import gramr.graph.edit.sgraph.Operators

def rules[MR: Graph](maxCorefs: Int = 1): Vector[SGraphRule[MR]] =
  Vector(
    new SGraphRule[MR](
      "fa",
      (context: SyntacticContext) => context.combinator == FA,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "fa-ignore",
      (context: SyntacticContext) => context.combinator == FA && context.childCats(1).matches(SyntacticCategory("NP")),
      Operators.Identity[MR](),
      backward = true
    ),
    new SGraphRule[MR](
      "ba",
      (context: SyntacticContext) => context.combinator == BA && !isTrNp2(context.childCats(1)),
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = true
    ),
    new SGraphRule[MR](
      "ba-ignore",
      (context: SyntacticContext) => context.combinator == BA && context.childCats(0).matches(SyntacticCategory("NP")),
      Operators.Identity[MR](),
      backward = false
    ),
    new SGraphRule[MR](
      "ba-tr",
      (context: SyntacticContext) => context.combinator == BA && isTrNp2(context.childCats(1)),
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "fc",
      (context: SyntacticContext) => context.combinator == FC && !isTrNp1(context.childCats(0)),
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "fc-tr",
      (context: SyntacticContext) => context.combinator == FC && isTrNp1(context.childCats(0)),
      Operators.Composition[MR](maxCorefs = 0),
      backward = true
    ),
    new SGraphRule[MR](
      "fc2",
      (context: SyntacticContext) => context.combinator == FC2,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "bxc",
      (context: SyntacticContext) => context.combinator == BXC && !isTrNp2(context.childCats(1)),
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = true
    ),
    new SGraphRule[MR](
      "bxc-tr",
      (context: SyntacticContext) => context.combinator == BXC && isTrNp2(context.childCats(1)),
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "bxc2",
      (context: SyntacticContext) => context.combinator == BXC2,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = true
    ),
    new SGraphRule[MR](
      "conj",
      (context: SyntacticContext) => context.combinator == Conj,
      Operators.Substitution[MR](maxCorefs = 0),
      backward = false
    ),
    new SGraphRule[MR](
      "conj-apply",
      (context: SyntacticContext) => context.combinator == Conj,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "lp",
      (context: SyntacticContext) => context.combinator == LPunct,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "rp",
      (context: SyntacticContext) => context.combinator == RPunct,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = true
    ),
    new SGraphRule[MR](
      "tc-rel",
      (context: SyntacticContext) => context.combinator == TcRel,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    )
  )

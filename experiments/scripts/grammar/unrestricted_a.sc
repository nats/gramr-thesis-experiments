import gramr.construction.SyntacticContext
import gramr.construction.sgraph.SGraphRule
import gramr.graph.Graph
import gramr.graph.edit.sgraph.Operators

def rules[MR: Graph](maxCorefs: Int = 1): Vector[SGraphRule[MR]] =
  Vector(
    new SGraphRule[MR](
      "forward apply",
      (context: SyntacticContext) => true,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "backward apply",
      (context: SyntacticContext) => true,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = true
    )
  )

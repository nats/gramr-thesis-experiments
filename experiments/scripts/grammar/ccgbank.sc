import $ivy.`com.lihaoyi::scalatags:0.6.3`
import $file.all
import all.{rules => baseRules}

import scalatags.Text.all._

import gramr.ccg
import gramr.ccg.EasyccgCombinatorSystem._
import gramr.ccg.{Atom, CombinatorSystem, EasyccgCombinatorSystem, SyntacticCategory}
import gramr.construction.SyntacticContext
import gramr.construction.sgraph.SGraphRule
import gramr.graph.edit.sgraph.Operators
import gramr.graph.Graph


implicit def stringToSyncat(s: String): SyntacticCategory = SyntacticCategory.apply(s)


class CcgbankCombinatorSystem extends CombinatorSystem {

  import EasyccgCombinatorSystem._

  import scala.language.implicitConversions

  val combinators : Iterable[ccg.Combinator] = List(
    FA, BA, FC, BXC, FC2, BXC2, Conj,
    RPunct, LPunct,
    // Rule used to allow nouns to become noun-phrases without needed a determiner.
    UnaryRule("""N""", """NP""", "lex"),
    // Relativization, as in "the boy playing tennis"
    UnaryRule("""S[pss]\NP""", """NP\NP""", "lex"),
    UnaryRule("""S[ng]\NP""", """NP\NP""", "lex"),
    UnaryRule("""S[adj]\NP""", """NP\NP""", "lex"),
    UnaryRule("""S[to]\NP""", """NP\NP""", "lex"),
    UnaryRule("""S[to]\NP""", """N\N""", "lex"),
    UnaryRule("""S[dcl]/NP""", """NP\NP""", "lex"),
    UnaryRule("""S[dcl]/NP""", """N\N""", "lex"),
    // Rules that let verb-phrases modify sentences, as in "Born in Hawaii, Obama is the 44th president."
    UnaryRule("""S[pss]\NP""", """S/S""", "lex"),
    UnaryRule("""S[ng]\NP""", """S/S""", "lex"),
    UnaryRule("""S[to]\NP""", """S/S""", "lex"),
    // Type raising
    UnaryRule("""NP""", """S[X]/(S[X]\NP)""", "tr"),
    UnaryRule("""NP""", """(S[X]\NP)\((S[X]\NP)/NP)""", "tr"),
    UnaryRule("""PP""", """(S[X]\NP)\((S[X]\NP)/PP)""", "tr"),

    // Binary type-changing rules
    TcRel, // Converts sentence-modifying relative clauses to NP modifiers
    TcNmod, // Composes noun modifiers while specifying they are still incomplete to the left

    // rules from CCGBank
    LComma, Merge, SFlip, SComma, ConjAdj, ConjNP,
    UnaryRule("""S\NP""", """(S\NP)/(S\NP)""", "lex"),
    UnaryRule("""S\NP""", """(S\NP)\(S\NP)""", "lex")
  )

  def synApply(combinator : ccg.Combinator, arguments : SyntacticCategory*) : Option[SyntacticCategory] =
    combinator.synApply(arguments : _*)

}

case object LComma extends ccg.Combinator with Binary with Forward {

  def synApply(arguments : SyntacticCategory*) : Option[SyntacticCategory] = {
    arguments.toList match {
      case List(Atom(","), y) => Some(addFeature(y))
      case List(Atom(";"), y) => Some(addFeature(y))
      case List(Atom(":"), y) => Some(addFeature(y))
      case List(Atom("."), y) => Some(y)
      case List(Atom("conj"), y) => Some(addFeature(y))
      case _ => None
    }
  }

  override def toString = "lcomma"

  def display = toString

  def addFeature(c : SyntacticCategory) : SyntacticCategory = c match {
    case a@Atom(_) => new Atom(a.category, Some("conj"))
    case _ => c
  }

}

case object Merge extends ccg.Combinator with Binary with Forward {

  def synApply(arguments : SyntacticCategory*) : Option[SyntacticCategory] = {
    arguments.toList match {
      case List(x, y) if x matches y => Some(x)
      case _ => None
    }
  }

  override def toString = "merge"

  def display = toString

}

case object SFlip extends ccg.Combinator with Binary with Forward {

  def synApply(arguments : SyntacticCategory*) : Option[SyntacticCategory] = {
    arguments.toList match {
      case List(x, Atom(",")) if x matches """S\S""" => Some("""S/S""")
      case _ => None
    }
  }

  override def toString = "sflip"

  def display = toString

}

case object SComma extends ccg.Combinator with Binary with Forward {

  import scalatags.Text.all._

  def synApply(arguments : SyntacticCategory*) : Option[SyntacticCategory] = {
    arguments.toList match {
      case List(Atom("NP"), Atom(",")) => Some("""S/S""")
      case _ => None
    }
  }

  override def toString = "scomma"

  def display = toString

}

case object ConjAdj extends ccg.Combinator with Binary with Forward {

  import scalatags.Text.all._

  def synApply(arguments: SyntacticCategory*) : Option[SyntacticCategory] = {
    arguments.toList match {
      case List(Atom("conj"), Atom("NP")) => Some("""S[adj]\NP[conj]""")
      case _ => None
    }
  }

  override def toString = "conjadj"

  def display = toString

}

case object ConjNP extends ccg.Combinator with Binary with Forward {

  import scalatags.Text.all._

  def synApply(arguments: SyntacticCategory*) : Option[SyntacticCategory] = {
    arguments.toList match {
      case List(Atom("conj"), Atom("S")) => Some("""NP[conj]""")
      case _ => None
    }
  }

  override def toString = "conjnp"

  def display = toString

}

def rules[MR: Graph](maxCorefs: Int): Vector[SGraphRule[MR]] = baseRules(maxCorefs) ++ Vector(

  // Additional rules for CCGbank
  new SGraphRule[MR](
    "lcomma-conj",
    (context: SyntacticContext) => context.combinator == LComma,
    Operators.Substitution[MR](maxCorefs = maxCorefs),
    backward = false
  ),
  new SGraphRule[MR](
    "lcomma-fa",
    (context: SyntacticContext) => context.combinator == LComma,
    Operators.Application[MR](maxCorefs = maxCorefs),
    backward = false
  ),
  new SGraphRule[MR](
    "lcomma-empty",
    (context: SyntacticContext) => context.combinator == LComma &&
      ((context.childCats(0) matches """,""") || (context.childCats(0) matches """:""")),
    Operators.Identity[MR](),
    backward = false
  ),
  new SGraphRule[MR](
    "merge-ba",
    (context: SyntacticContext) => context.combinator == Merge,
    Operators.Application[MR](maxCorefs = maxCorefs),
    backward = true
  ),
  new SGraphRule[MR](
    "merge-conj",
    (context: SyntacticContext) => context.combinator == Merge,
    Operators.Substitution[MR](maxCorefs = maxCorefs),
    backward = false
  ),
  new SGraphRule[MR](
    "sflip",
    (context: SyntacticContext) => context.combinator == SFlip,
    Operators.Identity[MR](),
    backward = true
  ),
  new SGraphRule[MR](
    "scomma",
    (context: SyntacticContext) => context.combinator == SComma,
    Operators.Identity[MR](),
    backward = true
  ),
  new SGraphRule[MR](
    "scomma-ba",
    (context: SyntacticContext) => context.combinator == SComma,
    Operators.Application[MR](maxCorefs = maxCorefs),
    backward = true
  ),
  new SGraphRule[MR](
    "conjnp-conj",
    (context: SyntacticContext) => context.combinator == ConjNP,
    Operators.Substitution[MR](maxCorefs = maxCorefs),
    backward = false
  )
)
import $file.conditions
import conditions._

import gramr.ccg.SyntacticCategory
import gramr.ccg.EasyccgCombinatorSystem._
import gramr.construction.SyntacticContext
import gramr.construction.sgraph.SGraphRule
import gramr.graph.Graph
import gramr.graph.edit.sgraph.Operators

def rules[MR: Graph](maxCorefs: Int = 1): Vector[SGraphRule[MR]] =
  Vector(
    new SGraphRule[MR](
      "fa",
      (context: SyntacticContext) => context.combinator == FA,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    // Allow modification of depth-1 nodes, such as “opium farmer”:
    // (p / person :ARG0-of (f / farm :ARG1 (o / opium)))
    new SGraphRule[MR](
      "fa-modify",
      (context: SyntacticContext) => context.combinator == FA && isForwardModifier(context.childCats(0)),
      Operators.Modification[MR](depth = 1, maxCorefs = maxCorefs),
      backward = false
    ),
    // Allow depth-1 modification for determiners such as 's genitive
    new SGraphRule[MR](
      "fa-modify-det",
      (context: SyntacticContext) => context.combinator == FA && isDeterminer(context.childCats(0)),
      Operators.Modification[MR](depth = 1, maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "fa-ignore",
      (context: SyntacticContext) => context.combinator == FA && context.childCats(1).matches(SyntacticCategory("NP")),
      Operators.Identity[MR](),
      backward = true
    ),
    new SGraphRule[MR](
      "ba",
      (context: SyntacticContext) => context.combinator == BA && !isTrNp2(context.childCats(1)),
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = true
    ),
    new SGraphRule[MR](
      "ba-modify",
      (context: SyntacticContext) => context.combinator == BA && isBackwardModifier(context.childCats(1)),
      Operators.Modification[MR](depth = 1, maxCorefs = maxCorefs),
      backward = true
    ),
    new SGraphRule[MR](
      "ba-ignore",
      (context: SyntacticContext) => context.combinator == BA && context.childCats(0).matches(SyntacticCategory("NP")),
      Operators.Identity[MR](),
      backward = false
    ),
    new SGraphRule[MR](
      "fc",
      (context: SyntacticContext) => context.combinator == FC && !isTrNp1(context.childCats(0)),
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "fc2",
      (context: SyntacticContext) => context.combinator == FC2,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "bxc",
      (context: SyntacticContext) => context.combinator == BXC && !isTrNp2(context.childCats(1)),
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = true
    ),
    new SGraphRule[MR](
      "bxc2",
      (context: SyntacticContext) => context.combinator == BXC2,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = true
    ),
    new SGraphRule[MR](
      "conj",
      (context: SyntacticContext) => context.combinator == Conj,
      Operators.Substitution[MR](maxCorefs = 0),
      backward = false
    ),
    new SGraphRule[MR](
      "conj-apply",
      (context: SyntacticContext) => context.combinator == Conj,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "lp",
      (context: SyntacticContext) => context.combinator == LPunct,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    ),
    new SGraphRule[MR](
      "rp",
      (context: SyntacticContext) => context.combinator == RPunct,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = true
    ),
    new SGraphRule[MR](
      "tc-rel",
      (context: SyntacticContext) => context.combinator == TcRel,
      Operators.Application[MR](maxCorefs = maxCorefs),
      backward = false
    )
  )

import java.io.File

import $file.commonSettings
import $file.grammars
import $file.induction
import commonSettings._
import gramr.ccg.{CombinatorSystem, DelexedLexicon, SimpleCombinatorSystem}
import gramr.construction.sgraph.SGraphRule
import gramr.experiments.ScriptSupport._
import gramr.util.Logging
import induction._

import scala.util.Random


@main
def main(
  corpus: String,
  lexicon: String,
  grammar: String = "all",
  alignments: String = "jamr+tamr+amr_ud+isi-vote1",
  maxItemCount: Int = 10000,
  maxUnalignedNodes: Int = 10,
  arityCheck: Boolean = true,
  samples: Option[Int] = None,
  seed: Int = 1,
  parallelism: Option[Int] = None,
  saveFilteredLexicon: Boolean = false
): Unit = {

  implicit val random: Random = new Random(seed)
  implicit val cs: CombinatorSystem = new SimpleCombinatorSystem

  logger.info("Starting parsing experiment")
  logger.info(s"grammar=$grammar alignments=$alignments")

  val actualParallelism = parallelism.getOrElse(Runtime.getRuntime.availableProcessors())
  logger.info(s"Parallelism: $actualParallelism")

  val outputDir = new File(lexicon)

  val logPath = outputDir
  Logging.setupLogging(logPath.toString)

  // We do not need to set maxCorefs as we don't do induction
  val rules: Vector[SGraphRule[MR]] = grammars.selectGrammar[MR](grammar, maxCorefs = 0)

  val splittingAlgorithm = mkSplittingAlgorithm(
    rules=rules,
    maxUnalignedNodes=Some(maxUnalignedNodes),
    maxItemCount=Some(maxItemCount),
    arityCheck=arityCheck)


  // Load data

  val examples = loadInductionData(corpus, alignments, samples)

  // Load split stats (needed to figure out which derivations to use)
  val splitStatsFile = new File(lexicon, "split1_stats.csv")
  logger.info(s"Loading split stats from $splitStatsFile")
  val splitStats = loadSplitStats(splitStatsFile)
  logger.info("Filtering for the best derivations")
  val exampleDerivationPairs = filterDerivationsByTokenLevelCoverage(splitStats.groupBy(_.exampleId), examples).toSeq
  logger.info(s"${exampleDerivationPairs.length} examples have derivations")


  // Load lexicon

  logger.info(s"Loading lexicon from $lexicon")
  val templateFile = new File(lexicon, "templates.unfiltered.txt")
  val lexemeFile = new File(lexicon, "lexemes.unfiltered.txt")

  val delexedLexicon = DelexedLexicon.load(
    templateFile=templateFile,
    lexemeFile=lexemeFile,
    wildcardFillers=wildcardFillers
  )

  logger.info(s"Loading EM parameters from $lexicon")
  val parameters = loadDelexedParameters(new File(lexicon), delexedLexicon, maxLexNodes = 1)

  val (filteredLexicon, taggedTrainData) = filteringPass(
    exampleDerivationPairs,
    delexedLexicon,
    splittingAlgorithm,
    parameters,
    actualParallelism
  )

  if(saveFilteredLexicon) {
    logger.info(s"Saving filtered lexicon to $lexicon")
    filteredLexicon.save(
      templateFile = new File(lexicon, "templates.txt"),
      lexemeFile = new File(lexicon, "lexemes.txt")
    )
  }

  logger.info(s"Saving gold tags")
  // Save training data for the super tagger
  saveSupertaggerTrainSet(
    taggedTrainData,
    exampleDerivationPairs=exampleDerivationPairs,
    file=new File(outputDir, "train_tags_gold.txt")
  )
}

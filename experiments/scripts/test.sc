import java.io.{BufferedWriter, File, FileWriter, PrintWriter}
import $file.commonSettings
import $file.grammars
import commonSettings._
import gramr.ccg.{CombinatorSystem, SimpleCombinatorSystem}
import gramr.construction.sgraph.SGraphRule
import gramr.experiments.Example
import gramr.experiments.ScriptSupport.{logger, _}
import gramr.graph.AdjacencyMeaningGraph
import gramr.graph.Predicates.{checkArgEdges, checkConstantDegrees, checkInstanceConcepts, checkNumberedEdgesUnique, checkPolarityEdges}
import gramr.jobs
import gramr.jobs.{Listener, NamedParses}
import gramr.jobs.ops._
import gramr.learn.util.FeatureIndex
import gramr.learn.features.FeatureSet
import gramr.learn.{BaseVector, FeatureVector, MapFeatureVector, UpdateStrategy}
import gramr.parse.Implicits._
import gramr.parse.{ActionPredicate, ActionPredicates, Parser, PatternChoice, and}
import gramr.util.{Logging, ParseStatsCsvLogger}

import scala.util.Random


@main
def main(
  test: String,
  lexicon: String,
  tags: String,
  output: String,
  grammar: String = "all",
  features: String = "all",
  alignments: String = "jamr+tamr+amr_ud+isi-vote1",
  learner: String = "adadelta",
  model: String = "model.ser",
  testSize: Option[Int] = None,
  arityCheck: Boolean = true,
  beam: Int = 15,
  parallelism: Option[Int] = None,
  seed: Int = 1,
  testSentenceRejectLength: Int = 100,
  saveCharts: Boolean = true
): Unit = {

  implicit val random: Random = new Random(seed)
  implicit val cs: CombinatorSystem = new SimpleCombinatorSystem

  logger.info("Testing")
  logger.info(s"grammar=$grammar alignments=$alignments features=$features")

  val outputDir = new File(output)

  val logPath = outputDir
  Logging.setupLogging(logPath.toString)

  // We do not need to set maxCorefs as we don't do induction
  val rules: Vector[SGraphRule[MR]] = grammars.selectGrammar[MR](grammar, maxCorefs = 0)


  // Load data

  val valExamples = loadTestData(test, testSize = testSize, supertagFile = Some(new File(tags)))


  // Load lexicon

  logger.info(s"Loading lexicon from $lexicon")
  val lexicalChoice = loadLexicon(new File(lexicon), wildcardFillersParsing)

  val actualParallelism = parallelism.getOrElse(  // default: allow at least 2GB per parsed example)
    Integer.min(
      Runtime.getRuntime.availableProcessors(),
      (Runtime.getRuntime.maxMemory() / (1024L * 1024L * 1024L * 2.0)).toInt))
  logger.info(s"Parsing with parallelism level $actualParallelism")


  // Build learner

  val learnerInstance = loadLearner(learner, model)
  implicit val featureIndex: FeatureIndex = learnerInstance.trainModel.featureIndex
  implicit val fv: FeatureVector[MapFeatureVector] = MapFeatureVector.featureVectorLike
  implicit val bv: BaseVector[MapFeatureVector] = fv


  // Build parsers
  var actionFilter: ActionPredicate[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] = and(
    // limitNamedEntities(NamedEntities.proxyReductions),
    ActionPredicates.rootCategoryFilter(example => example.derivations.flatMap(rootCategories).toSet),
    // ActionPredicates.rootCategoryFilter(_ => Set("S", "S[dcl]", "S[wq]", "S[q]", "NP", "N").map(SyntacticCategory.apply)),
    ActionPredicates.mrPredicate(mr => checkPolarityEdges(mr) && checkConstantDegrees(mr) && checkArgEdges(mr) && checkNumberedEdgesUnique(mr) && checkInstanceConcepts(mr))
  )
  if(arityCheck) {
    actionFilter = and(actionFilter, ActionPredicates.matchSyntacticArity)
  }

  val combinatoryChoice = new PatternChoice[MR](cs, rules)

  val featureSet: FeatureSet[MR, MapFeatureVector, Example[MR]] = selectFeatureSet(features)


  // Build parser

  logger.info(s"Using beam size $beam for decoding")

  val valStatsLogger = new ParseStatsCsvLogger(new File(logPath, s"test-stats.csv"))
  val valAmrFile = new File(logPath, s"test-amr.txt")
  val valAmrWriter = new PrintWriter(new BufferedWriter(new FileWriter(valAmrFile)))

  val valDecoder = gramr.parse.postprocess.resolveCorefs(
    gramr.parse.cky.learned[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](
      lexicalChoice = lexicalChoice,
      binaryChoice = combinatoryChoice,
      combinatorSystem = cs,
      decisionFeatures = featureSet.derivationFeatures.extract,
      stateFeatures = featureSet.stateFeatures.extract,
      modelScore = fv => learnerInstance.testModel.score(fv),
      actionFilter = actionFilter,
      leafCategories = leafCategories,
      dynamicBeamSize = _ => beam,
      branchingFactor = None,
      limitUnaryActions = false
    )).rejecting(_.sentence.length > testSentenceRejectLength)

  var valParseJob = jobs.Job(valExamples, "test")
    .parse(valDecoder, actualParallelism, inOrder = false)
    .progressBar
    .logAmr(valAmrWriter, evaluationFunction)
    .logTopDerivationAsHtml(logPath)
  if(saveCharts) valParseJob = valParseJob.logChartAsHtml(logPath)
  val valEvalJob = valParseJob
    .evaluate(evaluationFunction)
    .printIterationStats
    .logStatsToCsv(valStatsLogger)
  valEvalJob.run()

  valStatsLogger.close()
  valAmrWriter.close()
}

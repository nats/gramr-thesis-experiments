import $file.path
import gramr.experiments.Example
import gramr.graph.Graph
import gramr.learn.FeatureVector
import gramr.learn.features._

def derivationFeatures[MR: Graph, FV: FeatureVector] = DerivationFeatureCollection[MR, FV, Example[MR]](
  // How confident is the supertagger about the selected lexical template?
  SupertaggerConfidence(example => example.supertags(attributeName = "supertags-template-id")),
  // How confident is the CCG supertagger about the assigned syntactic category?
  TokenSyncatConfidence(example => example.ccgTags),
  LexicalItemIdentity(),
  // Assigns a parameter to every individual lexical generator
  GeneratorFeature(""".*""".r,
    context = List(GeneratorFeature.GeneratorName)),
  // Indicates when a generator is used with a specific syncat and set of tokens
  GeneratorFeature(""".*""".r,
    context = List(
      GeneratorFeature.GeneratorName,
      GeneratorFeature.SynCat(includeFeatures = false),
      GeneratorFeature.Tokens
    )
  ),
  // Indicates when a generator is used with a specific syncat and binned token count
  GeneratorFeature(""".*""".r,
    context = List(
      GeneratorFeature.GeneratorName,
      GeneratorFeature.TokenCount(List(4, 8))
    )
  ),
  // Counts pairs of token and node content in lexical decisions
  LexicalWordMeaningCooccurrence(),
  // Counts occurrence of lexeme with token
  LexemeLemma(),
  // Counts occurrence of template with token
  TemplateLemma(),
  // Counts syntactic combinators, grouping lex and tr combinators
  CombinatorFeature(Seq("lex.*".r, "tr.*".r, "bxc".r, "gbx".r, "fc".r, "gfc".r)),
  // Counts syntactic combinators together with the root content of the meaning representation, grouping lex and tr combinators
  CombinatorFeature(Seq("lex.*".r, "tr.*".r, "bxc".r, "gbx".r, "fc".r, "gfc".r), includeSemanticRoot = true),
  // Counts the syntactic category of any node together with the semantic root
  SyncatRootFeature(NodeTransformer.Content),

)

def stateFeatures[MR: Graph, FV: FeatureVector] = StateFeatureCollection[MR, FV, Example[MR]](
  // Features over paths of length 2
  path.pathFeaturesL2,
  // Counts if more than one edge with the same label is adjacent to a node
  DuplicateEdgeFeature(NodeTransformer.Constant("*"), EdgeTransformer.Label),
  // Counts if more than one edge with the same label and target node label is adjacent to a node
  DuplicateEdgeFeature(NodeTransformer.Content, EdgeTransformer.Label),
  // Counts if a node has more than one equivalent neighbour
  // TODO seems buggy? this should trigger for every node
  DuplicateNeighbourFeature(NodeTransformer.Constant("*"), NodeTransformer.Constant("*")),
  // Counts if a certain node has more than one equivalent neighbour
  DuplicateNeighbourFeature(NodeTransformer.Content, NodeTransformer.Content)
)

def featureSet[MR: Graph, FV: FeatureVector]  : FeatureSet[MR, FV, Example[MR]] = FeatureSet(
  derivationFeatures = derivationFeatures,
  stateFeatures = stateFeatures
)


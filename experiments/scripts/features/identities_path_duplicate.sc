import $file.path
import gramr.experiments.Example
import gramr.graph.Graph
import gramr.learn.FeatureVector
import gramr.learn.features._

def derivationFeatures[MR: Graph, FV: FeatureVector] = DerivationFeatureCollection[MR, FV, Example[MR]](
  LexicalItemIdentity(),
  // Assigns a parameter to every individual lexical generator
  GeneratorFeature(""".*""".r,
    context = List(GeneratorFeature.GeneratorName)),
)

def stateFeatures[MR: Graph, FV: FeatureVector] = StateFeatureCollection[MR, FV, Example[MR]](
  // Features over paths of length 2
  path.pathFeaturesL2,
  // Counts if more than one edge with the same label is adjacent to a node
  DuplicateEdgeFeature(NodeTransformer.Constant("*"), EdgeTransformer.Label),
  // Counts if more than one edge with the same label and target node label is adjacent to a node
  DuplicateEdgeFeature(NodeTransformer.Content, EdgeTransformer.Label),
  // Counts if a node has more than one equivalent neighbour
  // TODO seems buggy? this should trigger for every node
  DuplicateNeighbourFeature(NodeTransformer.Constant("*"), NodeTransformer.Constant("*")),
  // Counts if a certain node has more than one equivalent neighbour
  DuplicateNeighbourFeature(NodeTransformer.Content, NodeTransformer.Content)
)

def featureSet[MR: Graph, FV: FeatureVector]  : FeatureSet[MR, FV, Example[MR]] = FeatureSet(
  derivationFeatures = derivationFeatures,
  stateFeatures = stateFeatures
)


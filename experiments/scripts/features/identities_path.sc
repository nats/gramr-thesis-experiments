import $file.path
import gramr.experiments.Example
import gramr.graph.Graph
import gramr.learn.FeatureVector
import gramr.learn.features._

def derivationFeatures[MR: Graph, FV: FeatureVector] = DerivationFeatureCollection[MR, FV, Example[MR]](
  LexicalItemIdentity(),
  // Assigns a parameter to every individual lexical generator
  GeneratorFeature(""".*""".r,
    context = List(GeneratorFeature.GeneratorName)),
)

def stateFeatures[MR: Graph, FV: FeatureVector] = StateFeatureCollection[MR, FV, Example[MR]](
  // Features over paths of length 2
  path.pathFeaturesL2,
)

def featureSet[MR: Graph, FV: FeatureVector]  : FeatureSet[MR, FV, Example[MR]] = FeatureSet(
  derivationFeatures = derivationFeatures,
  stateFeatures = stateFeatures
)


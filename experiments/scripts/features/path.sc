import gramr.experiments._
import gramr.graph.Graph
import gramr.learn.FeatureVector
import gramr.learn.features._

// Common path features of length 2
def pathFeaturesL2[MR: Graph, FV: FeatureVector]: PathFeature2[MR, FV, Example[MR]] = {
  val tripleFeatureFilter = EdgeFilter.ExcludeHalfEdges() and EdgeFilter.ExcludeCorefEdges()

  PathFeature2[MR, FV, Example[MR]](
    PathFeatureConfig(
      tripleFeatureFilter,
      NodeTransformer.Content,
      List((EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Content))
    ),
    PathFeatureConfig(
      tripleFeatureFilter,
      NodeTransformer.Content,
      List((EdgeTransformer.Constant("*"), NodeTransformer.Content))
    ),
    PathFeatureConfig(
      tripleFeatureFilter,
      NodeTransformer.Class,
      List((EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Content))
    ),
    PathFeatureConfig(
      tripleFeatureFilter,
      NodeTransformer.Content,
      List((EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Class))
    ),
    PathFeatureConfig(
      EdgeFilter.ExcludeHalfEdges(source=false) and EdgeFilter.ExcludeCorefEdges(source=false),
      NodeTransformer.Constant("*"),
      List((EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Content))
    ),
    PathFeatureConfig(
      EdgeFilter.ExcludeHalfEdges(target=false) and EdgeFilter.ExcludeCorefEdges(target=false),
      NodeTransformer.Content,
      List((EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Constant("*")))
    ),
    PathFeatureConfig(
      EdgeFilter.ExcludeHalfEdges(target=false) and EdgeFilter.ExcludeCorefEdges(target=false),
      NodeTransformer.Content,
      List(
        (EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Content),
        (EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Content)
      )
    ),
    PathFeatureConfig(
      EdgeFilter.ExcludeHalfEdges(target=false) and EdgeFilter.ExcludeCorefEdges(target=false),
      NodeTransformer.Constant("*"),
      List(
        (EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Constant("*")),
        (EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Constant("*"))
      )
    ),
    PathFeatureConfig(
      EdgeFilter.ExcludeHalfEdges(target=false) and EdgeFilter.ExcludeCorefEdges(target=false),
      NodeTransformer.Content,
      List(
        (EdgeTransformer.Constant("*"), NodeTransformer.Content),
        (EdgeTransformer.Constant("*"), NodeTransformer.Content)
      )
    )
  )
}
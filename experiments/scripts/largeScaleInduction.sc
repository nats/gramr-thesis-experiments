import $file.commonSettings
import $file.grammars
import $file.induction
import commonSettings._
import induction._
import java.io.File

import gramr.ccg.{CombinatorSystem, SimpleCombinatorSystem}
import gramr.experiments.Example
import gramr.experiments.ScriptSupport._
import gramr.split.SplitChartFilter
import gramr.util.Logging

import scala.util.Random


implicit val combinatorSystem: CombinatorSystem = new SimpleCombinatorSystem


@main
def inductionExperiments(
  corpus: String,
  output: String,
  grammar: String = "all",
  alignments: String = "jamr+tamr+amr_ud+isi-vote1",
  arityCheck: Boolean = true,
  samples: Option[Int] = None,
  ids: Option[String] = None,  // Can point to a file of line-separate IDs
  maxItemCount: Int = 10000,
  maxUnalignedNodes: Int = 10,
  maxCorefs: Int = 1,
  em: Boolean = true,
  emIterations: Int = 100,
  ccgDerivations: Int = 10,  // We use 10 by default to avoid OOM crashes
  seed: Int = 1,
  parallelism: Option[Int] = None,
  dumpCharts: Boolean = false
): Unit = {

  val outputDir = new File(output)
  val lexPath = outputDir
  Logging.setupLogging(outputDir.toString)


  logger.info(s"Selected alignments: $alignments")

  val rules = grammars.selectGrammar(grammar, maxCorefs = maxCorefs)

  logger.info(s"Selected grammar: $grammar")
  logger.info(s"CCG derivations used: $ccgDerivations")

  implicit val random: Random = new Random(seed)

  val actualParallelism = parallelism.getOrElse(Runtime.getRuntime.availableProcessors())
  logger.info(s"Parallelism: $actualParallelism")

  val idList = ids.map(loadIdList)

  val examples = loadInductionData(corpus, alignments, samples, only = idList, ccgDerivations = Some(ccgDerivations))

  val splittingAlgorithm = mkSplittingAlgorithm(
    rules=rules,
    maxUnalignedNodes=Some(maxUnalignedNodes),
    maxItemCount=Some(maxItemCount),
    arityCheck=arityCheck)

  def chartFilter(e: Example[MR]): SplitChartFilter[MR] = gramr.split.chartFilters.LowestCrossSection[MR](e.derivations)

  val exampleDerivationPairs = examples.flatMap { example => example.indexedDerivations.map { der => (example, der) } }

  val splitStats = splitPass1(
    exampleDerivationPairs, splittingAlgorithm, chartFilter, actualParallelism, lexPath,
    dumpCharts = dumpCharts)

  val filteredExampleDerivationPairs = filterDerivationsByTokenLevelCoverage(splitStats, examples).toSeq

  val lexicon = induceDelex(
    filteredExampleDerivationPairs,
    splittingAlgorithm,
    chartFilter,
    parallelism = actualParallelism)

  // lexicon.save(
  //   templateFile = new File(lexPath, "templates.unfiltered.txt"),
  //   lexemeFile = new File(lexPath, "lexemes.unfiltered.txt")
  // )

  if(em) {
    emFilter(
      filteredExampleDerivationPairs = filteredExampleDerivationPairs,
      delexedLexicon = lexicon,
      splittingAlgorithm = splittingAlgorithm,
      emIterations = emIterations,
      outputPath = lexPath,
      parallelism = actualParallelism)
  }
}


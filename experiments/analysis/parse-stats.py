#!/usr/bin/env python

from pathlib import Path
import pandas as pd
import typer


def main(stats: Path, parser: str = 'parse'):
    df = pd.read_csv(stats)
    
    df = df[df['parserId'] == parser]
    print(f'Total : {len(df)} examples')
    print_stats(df)
    
    parsed_df = df[(df['correct'] + df['incorrect']) > 0]
    print(f'Parsed: {len(parsed_df)} examples')
    print_stats(parsed_df)
    

def print_stats(df: pd.DataFrame):
    total = df['total'].sum()
    retrieved = (df['correct'] + df['incorrect']).sum()
    correct = df['correct'].sum()
    p = correct / retrieved
    r = correct / total
    f1 = (2.0 * p * r) / (p + r)
    print(f'-- p: {p:.4} r: {r:.4} f: {f1:.4}')


if __name__ == '__main__':
    typer.run(main)
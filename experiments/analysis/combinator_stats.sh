#!/usr/bin/env bash

# Uses the output of induce/dump-charts to count the occurrences of every combinator in the EasyCCG output.

path=../induce/dump-charts

combinators=(fa ba fc bx gfc gbx conj rp lp tc-rel tc-nmod lex_n_np lex_pss lex_ng lex_adj lex_to_np lex_to_n lex_dcl lex_pss_s lex_ng_s lex_to_s tr_np1 tr_np2 tr_pp)

echo combinator,occurrence count,sentence count
for combinator in ${combinators[@]}; do
    occ_count=$(grep -o ">$combinator<" "${path}/output/split1"/*.html | wc -l)
    snt_count=$(grep -c ">$combinator<" "${path}/output/split1"/*.html | grep -v ":0" | wc -l)
    echo $combinator,$occ_count,$snt_count
done

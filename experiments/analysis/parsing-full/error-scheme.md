# Error Scheme

- Errors become apparent on the graph level
- We can use syntactic derivations to search for causes of these errors


## Constituent-level
- mc: constituent is not represented semantically
- ec: content is present which does not represent any constituent
- wc: constituent is represented but incorrectly
- sc: wrong scoping of conjunction
- ppa: wrong attachment of PP

## Node/Edge-level
- foc: incorrect focus
- att: incorrect attachment
- rol: incorrect role
- lab: incorrect node label
    - nv: noun-verb confusion
    - sen: incorrect sense

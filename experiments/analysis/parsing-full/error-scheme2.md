# Error Scheme

## Syntactic-Semantic level
- lex: wrong lexical content
- scope: wrong scoping of conjunction / quantifier
- attach: wrong phrase attachment
- control: missed control structure
- grammar: unsolvable with grammar
- anno: annotation mistake

## Graph level
- focus: wrong focus of graph
- label: incorrect node label
- spurious link
- wrong link
- direction: edge has wrong direction
- role: edge is labelled with wrong role
- missing content
- spurious content
- missed decompositionality
- spurious compositionality
- missed coreference

from dataclasses import dataclass
from itertools import islice, takewhile
from pathlib import Path
import re
from typing import Dict, List, Iterator, Optional, Set

import pandas as pd


@dataclass
class TaggedToken:
    tags: List[str]
    probabilities: List[float]

def parse_token_prediction(pred_str: str, probs: bool) -> TaggedToken:
    pairs = pred_str.split(',')
    if probs:
        tags, probs_str = zip(*(s.split('|') for s in pairs))
        probs = [float(p) for p in probs_str]
    else:
        tags = pairs
        probs = [1.0 for _ in tags]
    return TaggedToken(tags, probs)

@dataclass
class TaggedSentence:
    id: str
    layers: Dict[str, TaggedToken]
    sentence: Optional[List[str]] = None

def chunk_by_empty_lines(lines: Iterator[str]) -> Iterator[List[str]]:
    lines = (l.strip() for l in lines)
    while True:
        chunk = list(takewhile(lambda line: line != '', lines))
        if chunk:
            yield chunk
        else:
            break

attr_re = re.compile(r'::([\w-]+) ')

def load_tagger_output(path: Path) -> Iterator[TaggedSentence]:
    with path.open() as f:
        chunks = chunk_by_empty_lines(f)
        for chunk in chunks:
            id_line = chunk[0]
            id_str = id_line[7:]

            layers = {}
            for pred_line in chunk[1:]:
                attr_name = attr_re.search(pred_line).groups(1)[0]
                pred_str = attr_re.split(pred_line)[2]
                token_predictions = [parse_token_prediction(s, probs=True) for s in pred_str.split(' ')]
                layers[attr_name] = token_predictions

            sentence_prediction = TaggedSentence(id_str, layers)
            yield sentence_prediction


def load_gold_tags(path: Path) -> Iterator[TaggedSentence]:
    with path.open() as f:
        chunks = chunk_by_empty_lines(f)
        for chunk in chunks:
            id_str = chunk[0]
            snt_line = chunk[1]
            sentence = snt_line.split(' ')

            layers = {}
            for pred_line in chunk[2:]:
                fields = pred_line.split(': ', maxsplit=1)
                attr_name = fields[0]
                pred_str = fields[1] if len(fields) > 1 else ""
                if not pred_str:
                    print(f'Warning: {id_str} has empty annotation for attribute {attr_name}')
                token_predictions = [parse_token_prediction(s, probs=False) for s in pred_str.split(' ')]
                layers[attr_name] = token_predictions

            sentence_prediction = TaggedSentence(id_str, layers, sentence)
            yield sentence_prediction


"""
Code for analysis of induction experiments.
"""

from dataclasses import dataclass, asdict
import re
from typing import Optional, List, Dict
from pathlib import Path

import numpy as np
import pandas as pd

def stat_csv_path(experiment: str, condition: str) -> str:
    """
    Returns the path to the split stats CSV file for a given experimental
    condition.
    """
    return f'../{experiment}/output/{condition}/split1_stats.csv'

def log_path(experiment: str, condition: str) -> str:
    """
    Returns the path to the log file for a given experimental
    condition.
    """
    return f'../{experiment}/output/{condition}/log.txt'

def load_stats_table(experiment: str, condition: str) -> Optional[pd.DataFrame]:
    """
    Loads the split stats for a given experimental condition as a DataFrame.
    """
    try:
        csv_file = stat_csv_path(experiment, condition)
        csv_df = pd.read_csv(csv_file)
        csv_df = csv_df.assign(index=csv_df['exampleId'] + '-' + csv_df['derivationName'].map(str))
        csv_df = csv_df.set_index('index').sort_index()
        # An older version outputs 'sentenceLength' as 'tokens', this code
        # accounts for it
        if 'sentenceLength' not in csv_df.columns:
            print(f'{csv_file}: "sentenceLength" column not found, using "tokens"')
            csv_df.rename(columns={'tokens': 'sentenceLength'}, inplace=True)
        return csv_df
    except FileNotFoundError as e:
        print(e)
        return None

def best_derivation_only(table: pd.DataFrame) -> pd.DataFrame:
    """
    Extract only the derivation with the highest token level coverage for a sentence.
    """
    table = table.sort_values(['tokenLevelCoverage', 'derivationName'], ascending=[False, True]) \
        .drop_duplicates(['exampleId']) \
        .set_index('exampleId')
    table['dropped'] = dropped_tokens(table)
    table['coveredTokens'] = table['sentenceLength'] - table['dropped']
    return table

def summarize_stats(best: Dict[str, pd.DataFrame], log_stats: pd.DataFrame) -> pd.DataFrame:
    def gen_results():
        for condition in best.keys():
            table = best[condition]
            yield {
                'Condition': condition,
                'Token Cov.': token_coverage(table),
                'Perfect': perfect_sentences(table),
                'Lexicon Size': log_stats.at[condition, 'lexentry_count'],
                'Runtime': log_stats.at[condition, 'split1_seconds']
            }

    summary = pd.DataFrame(gen_results()).set_index('Condition')
    return summary

@dataclass
class LogStats:
    condition: str
    split1_seconds: Optional[int] = None
    lexentry_count: Optional[int] = None
        
    def update(self, field_name: str, x):
        if x is not None:
            setattr(self, field_name, x)

    @staticmethod
    def load(file_name: str, condition: str) -> 'LogStats':
        def extract_int(regex, line):
            match = regex.search(line)
            if match:
                return int(match.group(1))
            return None

        split1_seconds_re = re.compile(r'split-1 took (\d+) seconds')
        def read_split1_seconds(line):
            return extract_int(split1_seconds_re, line)

        lexentry_count_re = re.compile(r'Lexicalized lexical entry count: (\d+)')
        def read_lexentry_count(line):
            return extract_int(lexentry_count_re, line)

        def process_log_line(line, stats):
            stats.update('split1_seconds', read_split1_seconds(line))
            stats.update('lexentry_count', read_lexentry_count(line))

        stats = LogStats(condition)
        with open(file_name) as log_file:
            for line in log_file:
                process_log_line(line, stats)
        return stats

def load_log_stats(experiment: str, conditions: List[str]) -> pd.DataFrame:
    log_stats_objects = {condition: LogStats.load(log_path(experiment, condition), condition) for condition in conditions}
    # convert to data frame
    log_stats_dicts = {condition: asdict(stats) for condition, stats in log_stats_objects.items()}
    log_stats = pd.DataFrame.from_dict(log_stats_dicts, orient='index')
    return log_stats

# Average sentencewise token coverage
def avg_token_coverage(stats_table):
    return stats_table['tokenLevelCoverage'].mean()

def perfect_sentences(stats_table):
    return stats_table[stats_table['tokenLevelCoverage'] == 1.0]

# Total token coverage
def token_coverage(table):
    covered = int(table['coveredTokens'].sum())  # dropped tokens across all sentences
    total_tokens = table['sentenceLength'].sum()
    return covered / total_tokens

# Compute the number of dropped tokens for each example in a data frame according to the tokenLevelCoverage
def dropped_tokens(table):
    drop_rates = 1.0 - table['tokenLevelCoverage']
    dropped = drop_rates * table['sentenceLength']
    return dropped

def perfect_sentences(table):
    return (table['tokenLevelCoverage'] == 1.0).sum()

def contrasting_examples(best: Dict[str, pd.DataFrame], condition1: str, condition2: str) -> pd.DataFrame:
    """
    Returns the examples where condition1 has higher token coverage than condition2.
    """
    df1 = best[condition1]
    df2 = best[condition2]
    joined = df1.join(df2, lsuffix=f'.{condition1}', rsuffix=f'.{condition2}')
    coverages1 = joined[f'tokenLevelCoverage.{condition1}']
    coverages2 = joined[f'tokenLevelCoverage.{condition2}']
    contrasting = joined[coverages1 > coverages2]
    contrasting['contrast'] = coverages1 - coverages2
    contrasting.sort_values(f'tokenLevelCoverage.{condition1}', ascending=False, inplace=True)
    return contrasting

# Load metadata from a template file
def load_template_metadata(template_file: Path) -> List[Dict[str, str]]:
    value_re = re.compile(r'# ::(.+?) (.+)')
    with template_file.open() as f:
        def gen():
            values = {}
            for line in f:
                line = line.strip()
                if not line:
                    yield values
                    values = {}
                
                match = value_re.match(line)
                if match:
                    k = match.group(1)
                    v = match.group(2)
                    values[k] = v
            if values:
                yield values
        return list(gen())

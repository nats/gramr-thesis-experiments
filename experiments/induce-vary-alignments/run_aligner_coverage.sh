#!/usr/bin/env bash
set -eou pipefail

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../.."
GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
DATA=${ROOT_DIR}/data
SCRIPTS_PATH=${SCRIPT_DIR}/../scripts
OUTPUT_DIR="${SCRIPT_DIR}/output"

java -Xmx4g -jar ${GRAMR} ${SCRIPTS_PATH}/alignerCoverage.sc \
    --corpus ${DATA}/processed/training/amr-release-1.0-training-proxy.txt \
    --output ${OUTPUT_DIR}

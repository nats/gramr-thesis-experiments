#!/usr/bin/env bash
# Generate slurm scripts for individual experimental conditions

set -e

ALIGNMENTS=(none jamr tamr amr_ud isi \
            jamr+tamr-vote1 jamr+tamr-vote2 \
            jamr+amr_ud-vote1 jamr+amr_ud-vote2 \
            jamr+isi-vote1 jamr+isi-vote2 \
            tamr+amr_ud-vote1 tamr+amr_ud-vote2 \
            tamr+isi-vote1 tamr+isi-vote2 \
            amr_ud+isi-vote1 amr_ud+isi-vote2 \
            jamr+tamr+amr_ud-vote1 jamr+tamr+amr_ud-vote2 jamr+tamr+amr_ud-vote3 \
            jamr+tamr+isi-vote1 jamr+tamr+isi-vote2 jamr+tamr+isi-vote3 \
            jamr+amr_ud+isi-vote1 jamr+amr_ud+isi-vote2 jamr+amr_ud+isi-vote3 \
            tamr+amr_ud+isi-vote1 tamr+amr_ud+isi-vote2 tamr+amr_ud+isi-vote3 \
            jamr+tamr+amr_ud+isi-vote1 jamr+tamr+amr_ud+isi-vote2 jamr+tamr+amr_ud+isi-vote3 jamr+tamr+amr_ud+isi-vote4)

for ALIGNMENT in ${ALIGNMENTS[@]}; do
  ALIGNMENT=$ALIGNMENT envsubst '$ALIGNMENT' < template_slurm_job.sh > run_slurm_$ALIGNMENT.sh
done

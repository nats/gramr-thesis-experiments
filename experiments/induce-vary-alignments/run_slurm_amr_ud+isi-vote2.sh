#!/bin/bash
#SBATCH --job-name=amr_ud+isi-vote2
#SBATCH --time=0-06:00:00

#SBATCH --partition=std
#SBATCH --no-requeue
#SBATCH --nodes=1
#SBATCH --tasks-per-node=1
#SBATCH --export=NONE

set -e

source /sw/batch/init.sh

module load java/oracle-jdk8u101
module load python/3.6.8
module load git/2.6.4

export COURSIER_CACHE=$PWD/.cache
./run.sh amr_ud+isi-vote2

# Sentence stats

Run `sentence_stats.py` using the `/deps/venv`. Output:

```
Min sentence length   : 4
Mean sentence length  : 22.58
Median sentence length: 21.0
Sentences longer than 40 tokens: 3
```

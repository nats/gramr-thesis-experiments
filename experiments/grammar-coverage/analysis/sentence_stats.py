# Uses deps/venv for dependencies.

from argparse import ArgumentParser
import csv
import sys

import numpy as np

from amrcorpustools.example import parse_corpus

# attribute containing tokens
tok_attribute = 'tok'

def main():
    argparser = ArgumentParser(
        usage='Extracts the sentences from an AMR corpus, one sentence per line.')
    argparser.add_argument('--input', '-i',
                           default='../../../data/processed/consensus/consensus-dev.annotated.txt',
                           help='The AMR corpus to process')
    argparser.add_argument('--output', '-o',
                           default='sentence_stats.csv',
                           help='File name for per-sentence statistics in CSV format')

    args = argparser.parse_args()

    input_file = open(args.input) if args.input else sys.stdin
    input_examples = parse_corpus(input_file.readlines())

    # Calculate stats over sentence lengths: for that purpose, extract the token
    # sequences
    tok_seqs = [example.attributes[tok_attribute][0].split(' ') for example in input_examples]
    sentence_lengths = np.array([len(s) for s in tok_seqs])

    # Write CSV file with sentence stats
    with open(args.output, mode='w', newline='') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(['exampleId', 'tokens'])
        for example, toks in zip(input_examples, tok_seqs):
            writer.writerow([example.attributes['id'][0], len(toks)])
    print(f'Sentence stats written to {args.output}')
    
    print()

    # Print length stats
    print(f'Max sentence length   : {np.max(sentence_lengths)}')
    print(f'Min sentence length   : {np.min(sentence_lengths)}')
    print(f'Mean sentence length  : {np.mean(sentence_lengths)}')
    print(f'Median sentence length: {np.median(sentence_lengths)}')
    print(f'Sentences longer than 40 tokens: {len([s for s in tok_seqs if len(s) > 40])}')


if __name__ == '__main__':
    main()

#!/usr/bin/env bash
set -e

SCRIPT_DIR=$(readlink -f $(dirname "$0"))

syntax=( "gold" "easyccg" )
alignments=( "gold" "jamr+tamr+amr_ud+isi-vote1" )

for syn in "${syntax[@]}"; do
  for al in "${alignments[@]}"; do
    if [ "$syn" = "gold" ]; then
      grammar="ccgbank"
    else
      grammar="all"
    fi
    echo "--grammar=$grammar --syntax=$syn --alignments=$al"
    ${SCRIPT_DIR}/run.sh --grammar ${grammar} --syntax ${syn} --alignments ${al}
  done
done

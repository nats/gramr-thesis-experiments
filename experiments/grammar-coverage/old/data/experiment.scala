import gramr.experiments._
import gramr.experiments.reporting._
import gramr.mr.graph.{AdjacencyMeaningGraph, AlignmentGraph, MeaningGraphLike}
import gramr.learn.{LinearModel, TupleFeatureVector}
import java.io.{BufferedWriter, File, FileWriter, PrintWriter}

import gramr._
import gramr.ccg._
import EasyccgCombinatorSystem._
import gramr.construction._
import gramr.construction.sgraph.SGraphRule
import gramr.mr.graph.edit.sgraph.Operators
import gramr.parse._
import gramr.parse.freesyn.{AnchoredSyntaxFilter, FixedSyntaxFilter}

//noinspection RedundantDefaultArgument, TypeAnnotation
class MyExperiment extends Experiment[MyExperiment] {

  implicit def self = this
  type MR = AdjacencyMeaningGraph
  implicit def meaningGraphLike : MeaningGraphLike[MR] = AdjacencyMeaningGraph.meaningGraphLike
  type FV = TupleFeatureVector
  def featureVectorLike = TupleFeatureVector.featureVectorLike

  var splitCs: CombinatorSystem = new CcgbankCombinatorSystem
  override implicit def combinatorSystem = splitCs

  case class Config(
    corpus: Option[String] = None,
    alignments: Option[String] = None,
    useSyntax: Option[String] = None,
    twoPassSplitting: Boolean = false,
    useLemmas: Boolean = false,
    update: String = "early",
    trainSize: Option[Int] = None,
    testSize: Option[Int] = None,
    loadLexicon: Option[File] = None,
    loadModel: Option[File] = None,
    iterations: Int = 5,
    freeBeamSize: Int = 50,
    forcedBeamSize: Int = 50,
    bindingBeamSize: Int = 0,
    maxModifieeDepth: Option[Int] = None,
    branchingFactor: Double = 4.0,
    maxLoss: Double = 0.1,
    trainSentenceLength: Int = Integer.MAX_VALUE,
    testSentenceLength: Int = Integer.MAX_VALUE,
    synTest: Boolean = false,
    dumpCharts: Boolean = true,
    dumpTestCharts: Boolean = true,
    dumpChartProbability: Double = 1.0,
    syntax: String = "anchor",
    cmb: String = "ccgbank"
  )

  val optParser = new scopt.OptionParser[Config]("SGraph") {
    opt[String]("corpus").action( (x, c) => c.copy(corpus = Some(x)) )
    opt[String]("alignments").action( (x, c) => c.copy(alignments = Some(x)) )
    opt[String]("use-syntax").action( (x, c) => c.copy(useSyntax = Some(x)) )
    opt[Unit]("two-pass-splitting").action((_, c) => c.copy(twoPassSplitting = true) )
    opt[Unit]("use-lemmas").action((_, c) => c.copy(useLemmas = true) )
    opt[String]("update").action( (x, c) => c.copy(update = x) )
    opt[Int]("train-size").action( (x, c) => c.copy(trainSize = Some(x)) )
    opt[Int]("test-size").action( (x, c) => c.copy(testSize = Some(x)) )
    opt[File]("load-lexicon").action( (x, c) => c.copy(loadLexicon = Some(x)) )
    opt[File]("load-model").action( (x, c) => c.copy(loadModel = Some(x)) )
    opt[Int]("iterations").action( (x, c) => c.copy(iterations = x) )
    opt[Int]("beam").action( (x, c) => c.copy(freeBeamSize = x, forcedBeamSize = x) )
    opt[Int]("forced-beam").action( (x, c) => c.copy(forcedBeamSize = x) )
    opt[Int]("binding-beam").action( (x, c) => c.copy(bindingBeamSize = x) )
    opt[Int]("max-modifiee-depth").action( (x, c) => c.copy(maxModifieeDepth = Some(x)) )
    opt[Double]("branching-factor").action( (x, c) => c.copy(branchingFactor = x) )
    opt[Double]("max-loss").action( (x, c) => c.copy(maxLoss = x) )
    opt[Int]("train-sentence-length").action( (x, c) => c.copy(trainSentenceLength = x) )
    opt[Int]("test-sentence-length").action( (x, c) => c.copy(testSentenceLength = x) )
    opt[Unit]("syn-test").action( (_, c) => c.copy(synTest = true) )
    opt[Unit]("dump-charts").action( (_, c) => c.copy(dumpCharts = true) )
    opt[Unit]("dump-test-charts").action( (_, c) => c.copy(dumpTestCharts = true) )
    opt[Double]("dump-chart-probability").action( (x, c) => c.copy(dumpChartProbability = x))
    opt[String]("syntax").action( (x, c) => c.copy(syntax = x))
    opt[String]("cmb").action( (x, c) => c.copy(cmb = x) )
  }

  def run(args : Array[String]) = {

    val config = optParser.parse(args, Config()).get

    config.cmb match {
      case "ccgbank" => ()
      case "simple" => splitCs = new EasyccgCombinatorSystem
    }

    new SummaryReport(new File(reportPath, "summary.txt"), statCollector.observable)
    new TabularReport(new File(reportPath, "results.csv"), statCollector.observable)
    new SplittingSummaryReport(statCollector.observable, new File(reportPath, "splitting.txt"), csvOutput = Some(new File(reportPath, "splitting.csv")))
    new AmrReport(iter => new File(reportPath, s"amr_test.$iter.txt"), statCollector.observable, iter => Some(new File(reportPath, s"amr_gold.$iter.txt")))
    new CcgReport(iter => new File(reportPath, s"ccg.$iter.auto"), statCollector.observable)
    if(iterationLog) {
      new IterationReport(new File(reportPath, "iterations.html"), iterationLogInterval, statCollector.observable)
    }

    lazy val dumpTrainCharts = config.dumpCharts
    lazy val dumpTestCharts = config.dumpCharts || config.dumpTestCharts
    lazy val iterationLog = true
    lazy val iterationLogInterval = Math.min(200, config.trainSize.getOrElse(0))

    val dataRoot = scala.util.Properties.envOrNone("HOME").get + "/data/"
    val dataDir = dataRoot + "amr_1.0-derived/processed/"
    val synDataDir = dataRoot + "ccgsyn/anno/"
    val batchSize = 32
    val iterations = config.iterations

    val splittingSpanSize = 4

    val patterns = List(
      new SGraphRule[MR](
        "fa",
        (context: SyntacticContext) => context.combinator == FA,
        Operators.Application[MR](maxModifieeDepth = config.maxModifieeDepth),
        backward = false
      ),
      new SGraphRule[MR](
        "ba",
        (context: SyntacticContext) => context.combinator == BA,
        Operators.Application[MR](maxModifieeDepth = config.maxModifieeDepth),
        backward = true
      ),
      new SGraphRule[MR](
        "fc",
        (context: SyntacticContext) => context.combinator == FC,
        Operators.Application[MR](maxModifieeDepth = config.maxModifieeDepth),
        backward = false
      ),
      new SGraphRule[MR](
        "fc2",
        (context: SyntacticContext) => context.combinator == FC2,
        Operators.Application[MR](maxModifieeDepth = config.maxModifieeDepth),
        backward = false
      ),
      new SGraphRule[MR](
        "bxc",
        (context: SyntacticContext) => context.combinator == BXC,
        Operators.Application[MR](maxModifieeDepth = config.maxModifieeDepth),
        backward = true
      ),
      new SGraphRule[MR](
        "bxc2",
        (context: SyntacticContext) => context.combinator == BXC2,
        Operators.Application[MR](maxModifieeDepth = config.maxModifieeDepth),
        backward = true
      ),
      new SGraphRule[MR](
        "conj",
        (context: SyntacticContext) => context.combinator == Conj,
        Operators.Substitution[MR](),
        backward = false
      ),
      new SGraphRule[MR](
        "lp",
        (context: SyntacticContext) => context.combinator == LPunct,
        Operators.Identity[MR](),
        backward = false
      ),
      new SGraphRule[MR](
        "rp",
        (context: SyntacticContext) => context.combinator == RPunct,
        Operators.Identity[MR](),
        backward = true
      ),

      // Additional rules for CCGbank
      new SGraphRule[MR](
        "lcomma-conj",
        (context: SyntacticContext) => context.combinator == LComma,
        Operators.Substitution[MR](),
        backward = false
      ),
      new SGraphRule[MR](
        "lcomma-fa",
        (context: SyntacticContext) => context.combinator == LComma,
        Operators.Application[MR](maxModifieeDepth = config.maxModifieeDepth),
        backward = false
      ),
      new SGraphRule[MR](
        "lcomma-empty",
        (context: SyntacticContext) => context.combinator == LComma &&
          ((context.childCats(0) matches """,""") || (context.childCats(0) matches """:""")),
        Operators.Identity[MR](),
        backward = false
      ),
      new SGraphRule[MR](
        "merge-ba",
        (context: SyntacticContext) => context.combinator == Merge,
        Operators.Application[MR](maxModifieeDepth = config.maxModifieeDepth),
        backward = true
      ),
      new SGraphRule[MR](
        "merge-conj",
        (context: SyntacticContext) => context.combinator == Merge,
        Operators.Substitution[MR](),
        backward = false
      ),
      new SGraphRule[MR](
        "sflip",
        (context: SyntacticContext) => context.combinator == SFlip,
        Operators.Identity[MR](),
        backward = true
      ),
      new SGraphRule[MR](
        "scomma",
        (context: SyntacticContext) => context.combinator == SComma,
        Operators.Identity[MR](),
        backward = true
      ),
      new SGraphRule[MR](
        "scomma-ba",
        (context: SyntacticContext) => context.combinator == SComma,
        Operators.Application[MR](),
        backward = true
      ),
      new SGraphRule[MR](
        "conjnp-conj",
        (context: SyntacticContext) => context.combinator == ConjNP,
        Operators.Substitution[MR](),
        backward = false
      )
    )


    // Load data

    val alignmentPostProcessor = config.alignments match {
      case Some("gold") =>
        DataSet.CopyAttribute("alignments-gold", "alignments")
      case Some("jamr") =>
        DataSet.CopyAttribute("alignments-jamr", "alignments")
      case Some("isi") =>
        DataSet.CopyAttribute("alignments-isi", "alignments")
      case Some("combine") =>
        DataSet.CombineAlignments(
          "alignments-jamr",
          "alignments-isi", { (l, r) =>
            if (l.nonEmpty && r.nonEmpty) {
              l intersect r
            }
            else {
              if (r.isEmpty) l else r
            }
          }
        )
    }

    val syntaxPreProcessor = config.useSyntax match {
      case Some("gold") =>
        DataSet.DeleteAttributes("""ccg-derivation-\d+""".r)
      case Some("easyccg") =>
        DataSet.DeleteAttributes("""ccg-derivation-gold""".r)
    }

    val trainData =
      AmrCorpus(config.corpus.get).
        useJamrAlignment("alignments-jamr").
        useIsiAlignment("alignments-isi").
        useIsiAlignment("alignments-gold").
        preProcessed(syntaxPreProcessor).
        postProcessed(alignmentPostProcessor).
        sentenceLengthLimited(config.trainSentenceLength).
        randomised(12345).
        //idFiltered("PROXY_AFP_ENG_20020117_0270.10").
        countLimited(config.trainSize.getOrElse(100000))

    val w = new PrintWriter(new BufferedWriter(new FileWriter(new File(reportPath, "semdeps.txt"))))
    trainData.examples.foreach { example =>
      w.println(s"${example.id} ${syntacticDependencies(example.reference).mkString(" ")}")
    }
    w.close()

    // Grammar induction

    val excludeBusySpans = 100

    val splitLexicon: StoredLexiconResource = config.loadLexicon match {
      case None =>
        val preSplittingAlgorithm: RecursiveSplitting = RecursiveSplitting(
          maxItemCount = 1000,
          individualDerivations = true,
          maxUnalignedNodes = 20,
          splitFilter = gramr.split.filters.Connected() and
            gramr.split.filters.MatchSyntacticArity(gramr.split.filters.MatchSyntacticArity.LessOrEqual),
          splitGenerator = PatternBased(
            patterns = patterns
          )
        )

        val keyExtractor =
          if(config.useLemmas) gramr.ccg.StoredLexicon.lemmaKeyExtractor
          else gramr.ccg.StoredLexicon.defaultKeyExtractor

        if(config.twoPassSplitting) {
          val preSplitLexicon: SplitLexicon = SplitLexicon(
            data = trainData,
            splittingAlgorithm = preSplittingAlgorithm,
            dumpChartName = if (config.dumpCharts) Some("presplit") else None,
            chartFilter = (_: Example[MyExperiment]) =>
              gramr.split.chartFilters.SpanSize(_ <= splittingSpanSize) and
                gramr.split.chartFilters.ExcludeBusySpans(excludeBusySpans),
            eraseSyntacticAttributes = false,
            keyExtractor = keyExtractor
          )

          val splitterMask = InductionResult.bestSplitterMask(preSplitLexicon.inductionResult.splitQualities, margin = 0.1)

          val splittingAlgorithm = preSplittingAlgorithm.copy(splitterMask = splitterMask)

          val splitLexicon = SplitLexicon(
            data = trainData,
            splittingAlgorithm = splittingAlgorithm,
            dumpChartName = if (config.dumpCharts) Some("split") else None,
            chartFilter = (_: Example[MyExperiment]) =>
              gramr.split.chartFilters.SpanSize(_ <= splittingSpanSize) and
                gramr.split.chartFilters.ExcludeBusySpans(excludeBusySpans),
            eraseSyntacticAttributes = false,
            keyExtractor = keyExtractor
          )

          splitLexicon.dump(new File(reportPath + "/lexicon.html"))
          splitLexicon.writeSummary(new File(reportPath + "/lexicon_summary.txt"))
          splitLexicon.serialize(new File(reportPath + "/lexicon.ser"))

          splitLexicon
        }
        else {
          val splitLexicon = SplitLexicon(
            data = trainData,
            splittingAlgorithm = preSplittingAlgorithm,
            dumpChartName = if (config.dumpCharts) Some("split") else None,
            chartFilter = (_: Example[MyExperiment]) =>
              gramr.split.chartFilters.SpanSize(_ <= splittingSpanSize) and
                gramr.split.chartFilters.ExcludeBusySpans(excludeBusySpans),
            eraseSyntacticAttributes = false,
            keyExtractor = keyExtractor
          )

          splitLexicon.dump(new File(reportPath + "/lexicon.html"))
          splitLexicon.writeSummary(new File(reportPath + "/lexicon_summary.txt"))
          splitLexicon.serialize(new File(reportPath + "/lexicon.ser"))

          splitLexicon
        }

      case Some(path) =>
        val loaded = LoadLexicon(path)
        loaded
    }

    val generatedLexicon = {
      import GeneratedLexicon._
      val generators: List[LexicalGenerator] = List(
        //Empty("empty", Set("""N""", """NP""")),
        Identity("id"),
        SingleNode("thing", "thing", Set("""N""", """NP""")),
        Noun("noun"),
        IntransitiveVerb("v-intrans"),
        TransitiveVerb("v-trans"),
        // Quoted("quot"),
        // Name("name", 0.5),
        DashDate("dash-date"),
        AmericanSlashDate("slash-date"),
        TextDate("text-date"),
        Modifier("adj-mod", "mod", Set("""N/N""")),
        Modifier("adv-mod", "mod", Set("""(S\NP)/(S\NP)""", """(S\NP)\(S\NP)""")),
        Modifier("adv-manner", "manner", Set("""(S\NP)/(S\NP)""", """(S\NP)\(S\NP)"""))
      ) ++
        proxyNamedEntities("ne-")
      GeneratedLexicon(generators: _*)
    }

    val lexicon =
      UnionLexicon(
        splitLexicon without generatedLexicon,
        generatedLexicon
      )


    // Training

    val namedEntityFilters: ActionFilter[MR, FV, Example[MyExperiment]] =
      ActionFilterTrue() // LimitNamedEntities(NamedEntities.proxyReductions)

    val derivationFeatures = DerivationFeatureCollection(
      //SupertagProbability(syntaxStore, eraseFeatures = true),
      LexicalItemIdentity,
      GeneratorFeature(""".*""".r,
        context = List(GeneratorFeature.GeneratorName)),
      GeneratorFeature(""".*""".r,
        context = List(
          GeneratorFeature.GeneratorName,
          GeneratorFeature.SynCat(includeFeatures = false),
          GeneratorFeature.Tokens
        )
      ),
      GeneratorFeature(""".*""".r,
        context = List(
          GeneratorFeature.GeneratorName,
          GeneratorFeature.TokenCount(List(4, 8))
        )
      ),
      LexicalWordMeaningCooccurrence(),
      CombinatorFeature(Seq("lex.*".r, "tr.*".r, "bxc".r, "gbx".r, "fc".r, "gfc".r)),
      CombinatorFeature(Seq("lex.*".r, "tr.*".r, "bxc".r, "gbx".r, "fc".r, "gfc".r), includeSemanticRoot = true),
      SyncatRootFeature(NodeTransformer.Content),
      LexicalSkeletonFeature()
    )

    val tripleFeatureFilter = EdgeFilter.ExcludeHalfEdges() and EdgeFilter.ExcludeCorefEdges()

    val stateFeatures = StateFeatureCollection(
      PathFeatureSet(
        PathFeatureConfig(
          tripleFeatureFilter,
          NodeTransformer.Content,
          List((EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Content))
        ),
        PathFeatureConfig(
          tripleFeatureFilter,
          NodeTransformer.Content,
          List((EdgeTransformer.Constant("*"), NodeTransformer.Content))
        ),
        PathFeatureConfig(
          tripleFeatureFilter,
          NodeTransformer.Class,
          List((EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Content))
        ),
        PathFeatureConfig(
          tripleFeatureFilter,
          NodeTransformer.Content,
          List((EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Class))
        ),
        PathFeatureConfig(
          EdgeFilter.ExcludeHalfEdges(source=false) and EdgeFilter.ExcludeCorefEdges(source=false),
          NodeTransformer.Constant("*"),
          List((EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Content))
        ),
        PathFeatureConfig(
          EdgeFilter.ExcludeHalfEdges(target=false) and EdgeFilter.ExcludeCorefEdges(target=false),
          NodeTransformer.Content,
          List((EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Constant("*")))
        ),
        PathFeatureConfig(
          EdgeFilter.ExcludeHalfEdges(target=false) and EdgeFilter.ExcludeCorefEdges(target=false),
          NodeTransformer.Content,
          List(
            (EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Content),
            (EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Content)
          )
        ),
        PathFeatureConfig(
          EdgeFilter.ExcludeHalfEdges(target=false) and EdgeFilter.ExcludeCorefEdges(target=false),
          NodeTransformer.Constant("*"),
          List(
            (EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Constant("*")),
            (EdgeTransformer.LabelDenumeralisedExceptArg, NodeTransformer.Constant("*"))
          )
        ),
        PathFeatureConfig(
          EdgeFilter.ExcludeHalfEdges(target=false) and EdgeFilter.ExcludeCorefEdges(target=false),
          NodeTransformer.Content,
          List(
            (EdgeTransformer.Constant("*"), NodeTransformer.Content),
            (EdgeTransformer.Constant("*"), NodeTransformer.Content)
          )
        )
      ),
      DuplicateEdgeFeature(NodeTransformer.Constant("*"), EdgeTransformer.Label),
      DuplicateEdgeFeature(NodeTransformer.Content, EdgeTransformer.Label),
      DuplicateNeighbourFeature(NodeTransformer.Constant("*"), NodeTransformer.Constant("*")),
      DuplicateNeighbourFeature(NodeTransformer.Content, NodeTransformer.Content)
    )


    val featureSet : FeatureSet = FeatureSet(
      derivationFeatures = derivationFeatures,
      stateFeatures = stateFeatures
    )

    implicit def extractDerivations(e: Example[MyExperiment]): Seq[Derivation[Unit]] = e.derivations

    val trainActionFilterWithoutSyntax =
      MatchSyntacticArity() and
      namedEntityFilters and
      RootCategoryFilter(Set("S", "S[dcl]", "S[wq]", "S[q]", "NP"))

    val trainActionFilter = config.syntax match {
      case "free" => trainActionFilterWithoutSyntax
      case "anchor" => trainActionFilterWithoutSyntax and AnchoredSyntaxFilter(generatedOnly = false)
      case "anchor-generated" => trainActionFilterWithoutSyntax and AnchoredSyntaxFilter(generatedOnly = true)
    }

    val testActionFilter = trainActionFilter

    val evaluationFunction = SimpleRetrievalEvaluationFunction(
      measure = SmatchF1(1, 0),
      preprocessor = TrimToKernel
    )

    val learner: UpdateRuleLearner =
      UpdateRuleLearner(
        updater = ResourceReference.empty,
        evaluationFunction = evaluationFunction,
        batchSize = batchSize,
        initialParameters = LinearModel.empty,
        decay = 0.001//,
        //learningRate = RatioLearningRate(offset=10)
      )

    val parseCs = config.cmb match {
      case "ccgbank" => splitCs
      case "simple" => new SimpleCombinatorSystem
    }

    val freeDecoder = BottomUpDecoder(
      lexicon = lexicon,
      combinatoryChoice = PatternChoiceConfig(patterns),
      combinatorSystem = parseCs,
      model = learner.trainModel,
      featureSet = featureSet,
      actionFilter = trainActionFilter,
      dynamicBeamSize = ConstantBeamSize(config.freeBeamSize),
      branchingFactor = config.branchingFactor,
      bindingBeamSize = config.bindingBeamSize
    ).resolveCorefs

    val forcedDecoder = BottomUpDecoder(
      lexicon = lexicon,
      combinatoryChoice = PatternChoiceConfig(patterns),
      combinatorSystem = parseCs,
      model = learner.trainModel,
      featureSet = featureSet,
      actionFilter = trainActionFilterWithoutSyntax and FixedSyntaxFilter() and AlignmentsFulfilled(1.0) and GraphElementsCorrect(),
      dynamicBeamSize = ConstantBeamSize(config.forcedBeamSize),
      branchingFactor = config.branchingFactor,
      bindingBeamSize = config.bindingBeamSize
    ).resolveCorefs

    val innerUpdateRule = config.update match {
      case "early" =>
        EarlyUpdate()
      case "loss" =>
        LossSensitiveUpdate(maxLoss = config.maxLoss)
      case "early+loss" =>
        EarlyUpdate().fallBack(LossSensitiveUpdate(maxLoss = config.maxLoss))
      case "early+force" =>
        EarlyUpdate().fallBack(TrainForcedDecoder())
    }
    val updateRule = LoggingUpdateRule(innerUpdateRule, ResourceReference.empty)
    learner.updater.define(ForcedDecodingUpdater(updateRule, freeDecoder, forcedDecoder))

    val testDecoder: FreeDecoder = BottomUpDecoder(
      lexicon = lexicon,
      combinatoryChoice = PatternChoiceConfig(patterns),
      combinatorSystem = parseCs,
      model = learner.trainModel,
      featureSet = featureSet,
      actionFilter = testActionFilter,
      dynamicBeamSize = ConstantBeamSize(config.freeBeamSize),
      branchingFactor = config.branchingFactor,
      bindingBeamSize = config.bindingBeamSize
    ).resolveCorefs.rejecting(_.sentence.size > config.testSentenceLength)


    val testModel: ResourceReference[ModelResource] = config.loadModel match {
      case None =>
        val trainer =
          TrainTester(
            iterations = iterations,
            learner = learner,
            testDecoder = testDecoder,
            postProcess = NoProcessing,
            trainData = trainData,
            testData = trainData,
            dumpTrainCharts = dumpTrainCharts,
            dumpTestCharts = dumpTestCharts,
            dumpDerivationCharts = false,
            testEveryIteration = false,
            testFinal = false,
            dumpChartProbability = config.dumpChartProbability
          )

        updateRule.exampleLogger.define(trainer.exampleLogger)
        trainer.run()
        learner.testModel
      case Some(path) =>
        LoadModel(path)
    }
    testModel.serialize(new File(reportPath + "/model.ser"))


    // output test syntax

    if(config.synTest) {
      val synTestData =
        AmrCorpus(synDataDir + "test")

      val synTester =
        Tester(
          learner = learner,
          testDecoder = testDecoder,
          postProcess = NoProcessing,
          testData = synTestData,
          dumpTestCharts = dumpTestCharts,
          dumpDerivationCharts = false
        )
      synTester.test("syn")
    }
  }

  class CcgbankCombinatorSystem extends CombinatorSystem {

    import EasyccgCombinatorSystem._

    import scala.language.implicitConversions

    val combinators : Iterable[ccg.Combinator] = List(
      FA, BA, FC, BXC, FC2, BXC2, Conj,
      RPunct, LPunct,
      // Rule used to allow nouns to become noun-phrases without needed a determiner.
      UnaryRule("""N""", """NP""", "lex"),
      // Relativization, as in "the boy playing tennis"
      UnaryRule("""S[pss]\NP""", """NP\NP""", "lex"),
      UnaryRule("""S[ng]\NP""", """NP\NP""", "lex"),
      UnaryRule("""S[adj]\NP""", """NP\NP""", "lex"),
      UnaryRule("""S[to]\NP""", """NP\NP""", "lex"),
      UnaryRule("""S[to]\NP""", """N\N""", "lex"),
      UnaryRule("""S[dcl]/NP""", """NP\NP""", "lex"),
      // Rules that let verb-phrases modify sentences, as in "Born in Hawaii, Obama is the 44th president."
      UnaryRule("""S[pss]\NP""", """S/S""", "lex"),
      UnaryRule("""S[ng]\NP""", """S/S""", "lex"),
      UnaryRule("""S[to]\NP""", """S/S""", "lex"),
      // Type raising
      UnaryRule("""NP""", """S[X]/(S[X]\NP)""", "tr"),
      UnaryRule("""NP""", """(S[X]\NP)\((S[X]\NP)/NP)""", "tr"),
      UnaryRule("""PP""", """(S[X]\NP)\((S[X]\NP)/PP)""", "tr"),

      // A combinator that is sometimes output by Easyccg but that doesn’t seem to make any sense
      Junk,

      // rules from CCGBank
      LComma, Merge, SFlip, SComma, ConjAdj, ConjNP,
      UnaryRule("""S\NP""", """(S\NP)/(S\NP)""", "lex"),
      UnaryRule("""S\NP""", """(S\NP)\(S\NP)""", "lex")
    )

    def synApply(combinator : ccg.Combinator, arguments : SyntacticCategory*) : Option[SyntacticCategory] =
      combinator.synApply(arguments : _*)

  }

  case object LComma extends ccg.Combinator with Binary with Forward {

    import scalatags.Text.all._

    def synApply(arguments : SyntacticCategory*) : Option[SyntacticCategory] = {
      arguments.toList match {
        case List(Atom(","), y) => Some(addFeature(y))
        case List(Atom(";"), y) => Some(addFeature(y))
        case List(Atom(":"), y) => Some(addFeature(y))
        case List(Atom("."), y) => Some(y)
        case List(Atom("conj"), y) => Some(addFeature(y))
        case _ => None
      }
    }

    override def toString = "lcomma"

    def display = toString

    def addFeature(c : SyntacticCategory) : SyntacticCategory = c match {
      case a@Atom(_) => new Atom(a.category, Some("conj"))
      case _ => c
    }

  }

  case object Merge extends ccg.Combinator with Binary with Forward {

    import scalatags.Text.all._

    def synApply(arguments : SyntacticCategory*) : Option[SyntacticCategory] = {
      arguments.toList match {
        case List(x, y) if x matches y => Some(x)
        case _ => None
      }
    }

    override def toString = "merge"

    def display = toString

  }

  case object SFlip extends ccg.Combinator with Binary with Forward {

    import scalatags.Text.all._

    def synApply(arguments : SyntacticCategory*) : Option[SyntacticCategory] = {
      arguments.toList match {
        case List(x, Atom(",")) if x matches """S\S""" => Some("""S/S""")
        case _ => None
      }
    }

    override def toString = "sflip"

    def display = toString

  }

  case object SComma extends ccg.Combinator with Binary with Forward {

    import scalatags.Text.all._

    def synApply(arguments : SyntacticCategory*) : Option[SyntacticCategory] = {
      arguments.toList match {
        case List(Atom("NP"), Atom(",")) => Some("""S/S""")
        case _ => None
      }
    }

    override def toString = "scomma"

    def display = toString

  }

  case object ConjAdj extends ccg.Combinator with Binary with Forward {

    import scalatags.Text.all._

    def synApply(arguments: SyntacticCategory*) : Option[SyntacticCategory] = {
      arguments.toList match {
        case List(Atom("conj"), Atom("NP")) => Some("""S[adj]\NP[conj]""")
        case _ => None
      }
    }

    override def toString = "conjadj"

    def display = toString

  }

  case object ConjNP extends ccg.Combinator with Binary with Forward {

    import scalatags.Text.all._

    def synApply(arguments: SyntacticCategory*) : Option[SyntacticCategory] = {
      arguments.toList match {
        case List(Atom("conj"), Atom("S")) => Some("""NP[conj]""")
        case _ => None
      }
    }

    override def toString = "conjnp"

    def display = toString

  }

  def depNeighbours(mr: MyExperiment#MR, startNode: MyExperiment#MR#NodePointer): Iterable[MyExperiment#MR#NodePointer] = {
    var agenda = Vector.empty[MyExperiment#MR#NodePointer]
    var output = Vector.empty[MyExperiment#MR#NodePointer]
    var visited = Set[MyExperiment#MR#NodePointer](startNode)
    for(node <- mr.neighbours(startNode)) {
      agenda = agenda :+ node.pointer
      visited = visited + node.pointer
    }
    while(agenda.nonEmpty) {
      val node = mr.node(agenda.head)
      agenda = agenda.tail
      if(AlignmentGraph.getElementAlignments(node.element).isEmpty) {
        // If a node is unaligned, we allow it to be skipped and continue with any of its neighbours
        for(node <- mr.neighbours(startNode)) {
          if(!visited.contains(node.pointer)) {
            agenda = agenda :+ node.pointer
            visited = visited + node.pointer
          }
        }
      }
      else {
        // If a node is aligned, this is a dependency (and we stop BFS)
        output = output :+ node.pointer
      }
    }
    output
  }

  def syntacticDependencies(mr: MyExperiment#MR): Iterable[(Int, Int)] = {
    for {
      node1 <- mr.nodes
      node2p <- depNeighbours(mr, node1.pointer)
      node2 = mr.node(node2p)
      alignment1 <- AlignmentGraph.getElementAlignments(node1.element)
      alignment2 <- AlignmentGraph.getElementAlignments(node2.element)
    } yield (alignment1, alignment2)
  }



}

new MyExperiment()

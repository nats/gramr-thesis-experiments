#!/usr/bin/env python3

import sys, re

if(len(sys.argv) != 3):
    print("Usage: add_gold_alignments.py <amr corpus file> <gold alignments file>")
    exit(1)

amrFileName = sys.argv[1]
alignmentFileName = sys.argv[2]

alignmentFile = open(alignmentFileName)
alignmentLines = [line.strip() for line in alignmentFile.readlines() if len(line.strip()) > 0]
alignmentFile.close()

# Collect all the attributes and make them accessible by sentence ID.
alignments = {}

idPrefix = r'# ::id '
alPrefix = r'# ::alignments '

for i in range(0, len(alignmentLines), 2):
    (idLine, alignmentLine) = alignmentLines[i:i+2]
    if not idLine.startswith(idPrefix):
        print('Invalid id line {0}'.format(idLine), file=sys.stderr)
        exit(1)
    if not alignmentLine.startswith(alPrefix):
        print('Invalid alignment line {0}'.format(alignmentLine), file=sys.stderr)
        exit(1)
    ident = idLine[len(idPrefix):]
    alignment = alignmentLine[len(alPrefix):]
    alignments[ident] = alignment


def print_attr(ident):
    print('# ::alignments-gold {0}'.format(alignments[ident]))

amrFile = open(amrFileName)

lastId = None
line = amrFile.readline()
idRe = re.compile(r"::id ([^\s]+)")
wasInComment = True
while line:
    if line.startswith("#"):
        m = idRe.search(line)
        if m:
            lastId = m.group(1)
        wasInComment = True
    else:
        if wasInComment and lastId != None:
            # Insert the attributes at the end of the comments section
            print_attr(lastId)
        wasInComment = False
    print(line, end="")
    line = amrFile.readline()

amrFile.close()
    



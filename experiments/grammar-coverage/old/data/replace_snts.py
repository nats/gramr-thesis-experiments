#!/usr/bin/env python3

import sys, re

if(len(sys.argv) != 3):
    print("Usage: replace_snts.py <raw file> <amr corpus file>")
    exit(1)

rawFileName = sys.argv[1]
amrFileName = sys.argv[2]

rawFile = open(rawFileName)
rawLines = rawFile.readlines()
rawFile.close()

amrFile = open(amrFileName)
amrLines = amrFile.readlines()
amrFile.close()

def isSntLine(line):
    return len(line.strip()) > 0 and not line.startswith('#')
rawSnts = list(filter(isSntLine, rawLines))

for line in amrLines:
    if '::snt' in line:
        rawSnt = rawSnts.pop(0)
        # This replaces the triples tokenize app and so also creates fake 'tok' and 'lemmas' attributes.
        print("# ::snt {0}".format(rawSnt), end='')
        print("# ::tok {0}".format(rawSnt), end='')
        print("# ::lemmas {0}".format(rawSnt), end='')
    else:
        print(line, end='')

#!/usr/bin/env python3

import sys, re

if(len(sys.argv) != 3):
    print("Usage: amr_add_alignments.py <amr corpus file> <alignment file>")
    exit(1)

amrFileName = sys.argv[1]
alignmentFileName = sys.argv[2]

alignmentFile = open(alignmentFileName)
alignmentLines = alignmentFile.readlines()
alignmentFile.close()

# Add the attributes to the AMR file
amrFile = open(amrFileName)

currentAlignmentLine = 0
line = amrFile.readline()
wasInComment = False
while line:
    if line.startswith('# ::'):
        wasInComment = True
    else:
        if wasInComment:
            # Insert the attribute at the end of the comments section
            print('# ::alignments-isi {0}'.format(alignmentLines[currentAlignmentLine]), end='')
            currentAlignmentLine += 1
        wasInComment = False
    print(line, end='')
    line = amrFile.readline()

amrFile.close()

#!/usr/bin/env bash
# Runs the grammar coverage experiment.
# Usage:
#   run.sh [--grammar <GRAMMAR>] [--syntax <SYNTAX>] [--alignments <ALIGNMENTS>] [--maxCorefs <MAX_COREFS>]
#
# GRAMMAR can be one of the GA-CCG grammars
# SYNTAX can be gold,easyccg
# ALIGNMENTS is "aligner1+aligner2+aligner3+aligner4-voteN"
set -e

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../.."

GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
DATA=${ROOT_DIR}/data
SCRIPTS_PATH=${SCRIPT_DIR}/../scripts
OUTPUT_DIR=${SCRIPT_DIR}/output

java -Xmx8g -jar "${GRAMR}" "${SCRIPTS_PATH}/grammarCoverage.sc" \
    --corpus "${DATA}/processed/consensus/consensus-dev.annotated.txt" \
    --output "${OUTPUT_DIR}" \
    "$@"

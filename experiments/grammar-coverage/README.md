# Experiment on Grammar Coverage

This experiment uses the `consensus-dev` corpus to examine coverage of CCG/ASKi
grammars. This section of the corpus is part of the WSJ corpus of CCGBank.

Also, Nima Pourdamghani has kindly made gold standard alignments available [on
his website](https://isi.edu/~damghani/papers/gold_alignments.zip) for
`consensus-dev` and `consensus-test` as part of the ISI aligner paper [0]. We
use this data in a modified form, adapting it to CCGBank tokenization.

## Requirements

- AMR release 1.0 in `data/amr_anno_1.0`
- CCGBank 1.1 in `data/ccgbank_1_1`
- Run both `preprocess.sh` and `preprocess-ccgbank.sh` in the root directory of
  this repository.

## Running

To reproduce results:

```bash
$ ./run_all.sh
```

# References

[0] Pourdamghani, Nima, Yang Gao, Ulf Hermjakob, and Kevin Knight. “Aligning
English Strings with Abstract Meaning Representation Graphs.” In Proceedings of
the 2014 Conference on Empirical Methods in Natural Language Processing (EMNLP),
425–429. Doha, Qatar: Association for Computational Linguistics, 2014.
http://www.aclweb.org/anthology/D14-1048.

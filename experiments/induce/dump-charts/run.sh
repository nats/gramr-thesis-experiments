#!/usr/bin/env bash
set -euo pipefail

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/../../.."

GRAMR=${ROOT_DIR}/deps/gramr/gramr.jar
DATA=${ROOT_DIR}/data
SCRIPTS_PATH=${SCRIPT_DIR}/../../scripts
OUTPUT_DIR="${SCRIPT_DIR}/output"

java -Xmx48g -jar ${GRAMR} ${SCRIPTS_PATH}/saveSplitCharts.sc \
    --grammar aski \
    --alignments jamr+tamr+amr_ud+isi-vote1 \
    --corpus ${DATA}/processed/training/amr-release-1.0-training-proxy.txt \
    --output ${OUTPUT_DIR} \
    --maxUnalignedNodes 10 \
    --maxItemCount 10000 \
    --ccgDerivations 1

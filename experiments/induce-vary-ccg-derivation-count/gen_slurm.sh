#!/usr/bin/env bash
# Generate slurm scripts for individual experimental conditions

set -e

COUNTS=(1 2 5 10 20 50)

for COUNT in ${COUNTS[@]}; do
  DERIVATION_COUNT=$COUNT envsubst '$DERIVATION_COUNT' < template_slurm_job.sh > run_slurm_$COUNT.sh
done

#!/usr/bin/env bash
# Installs s2tagger.

# Prerequisites:
#   - git
#   - python3.8

set -e

DEPSDIR=$1

echo Installing s2tagger
python3.8 -m venv ${DEPSDIR}/s2tagger-venv
source ${DEPSDIR}/s2tagger-venv/bin/activate
pip install poetry

git clone https://gitlab.com/nats/s2tagger.git ${DEPSDIR}/s2tagger
cd ${DEPSDIR}/s2tagger
git checkout v1.2.2
poetry install

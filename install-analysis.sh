#!/usr/bin/env bash
# Installs a jupyter kernel for the analysis notebooks

# Prerequisites:
#   - python3.9

set -e

DEPSDIR=$1

python3.9 -m venv ${DEPSDIR}/analysis
source ${DEPSDIR}/analysis/bin/activate
pip install -U pip
pip install -r requirements.analysis.txt
pip install -e preprocess/scripts/amrcorpustools

ipython kernel install --name "analysis" --user

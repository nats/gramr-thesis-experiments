# gramr Experiments

This project reproduces the results in Sebastian Beschke's PhD Thesis. The
remainder of this README explains how to run the experiments.

Despite efforts to keep things as simple as possible, running the experiments
entails a number of steps, which are described in more detail below.

1. **Prerequisites.** Obtain the AMR corpus and set up your local computing
   environment to be able to follow the remaining steps.
2. **Preprocessing.** Uses external tools to enrich the AMR corpus with
   additional attributes. This step requires the use of Docker.
3. **Experiments.** The experiment pipeline is divided into the steps
   **induce**, **tag**, and **parse**. You can choose to run these steps with
   Docker (easy setup) or without (full control, no root required).
4. **Analysis.** Extracting useful statistics, graphs and other information from
   the experimental logs.

The following repositories contain the actual source code used in the
experiments:

- [gramr](https://gitlab.com/nats/gramr) Semantic Parser
- [s2tagger](https://gitlab.com/nats/s2tagger) Supertagger

Don't hesitate to contact me at `beschke@informatik.uni-hamburg.de` if you need
help with anything.

## Prerequisites

You will need the following:

- A computer you can run Docker on – e.g. a Linux box where you have root access.
- [Docker](https://docker.com) and `docker-compose` installed on your computer.
  On Ubuntu 18.04, you can use

     ```bash
     $ sudo apt-get install docker.io docker-compose
     ```
    
  to install it. Check the [Docker website](https://docker.com/get-started) for
  installation options on other systems.
- Python 3.8 is required:

  ```bash
  $ python3.8 -v
  3.8.10
  ```
- [sbt](https://www.scala-sbt.org/) and `git` must be available.
- [The AMR 1.0 (LDC2014T12) data set](https://catalog.ldc.upenn.edu/LDC2014T12).
- [CCGBank 1.1](https://catalog.ldc.upenn.edu/LDC2005T13).


## Preprocessing

This repository defines two preprocessing pipelines: a main pipeline defined in
`preprocess.sh` and a CCGBank-specific one defined in `preprocess-ccgbank.sh`.
The latter pipeline is only required for the experiments in
`experiments/grammar-coverage`.

### Unpacking the Data

Before you begin, you must unpack the [AMR 1.0
corpus](https://catalog.ldc.upenn.edu/LDC2014T12) into the `data` directory.
This directory is mounted by docker and used to access the corpus data. E.g.:

```bash
$ cd data/
$ cp ~/Downloads/amr_anno_1.0_LDC2014T12.tgz .
$ tar -xzf amr_anno_1.0_LDC2014T12
$ ls
amr_anno_1.0
```

[CCGBank 1.1](https://catalog.ldc.upenn.edu/LDC2005T13) is also required for the
experiments in `experiments/grammar-coverage`.

### Building the Docker Images

Preprocessing uses a diverse set of tools, which are pulled from a wide variety
of sources and which in turn require very specific dependencies. To account for
this complexity, the environment required for each tool is described in a
separate `Dockerfile`. All of these descriptions, along with any additional
required data, are located in the `tools` directory.

Additionally, a `docker-compose.yaml` file is provided which also describes how
these tools access the needed data.

To run the preprocessing pipelines, the docker image for each tool must first be
built. A script is provided for this:

```bash
$ ./build.sh
```

Be aware that building the images takes a while and downloads several gigabytes
of data.


### Running the Main Preprocessing Pipeline

To run the main preprocessing pipeline, execute its script:

```bash
$ ./preprocess.sh
```

### Running the CCGBank Preprocessing Pipeline

To run the CCGBank preprocessing pipeline, execute its script:

```bash
$ ./preprocess-ccgbank.sh
```

## Experiments

Experiments are implemented using scripts that call `gramr` and `s2tagger`. I
didn’t package the experiments in Docker containers because a) they only depend
on these two tools, and b) because I needed to run the experiments on machines I
did not have root access on (and thus could not run docker).

Apart from these two tools,
[ELMo](https://github.com/allenai/allennlp/blob/v0.9.0/allennlp/commands/elmo.py)
is used to precompute word vectors.

### Installing

Run the install scripts to install `gramr`, `s2tagger`, and `elmo`.

```bash
$ ./install.sh
```

### Generating ELMo vectors

Use the provided script to generate ELMo word vectors for the sentences in the
AMR corpus, which are needed for training the supertagger.

```bash
$ preprocess/elmo.sh
```

### Running

Experiments are located in the `experiments` folder. See each experiment’s
README for details on how to run it.

## Analysis

Analyses for the various experiments are collected in `experiments/analysis`,
mostly in the form of [Jupyter Notebooks](https://jupyter.org/). Some
experiments also contain some analysis scripts and results within their separate
directories. In all cases, check the corresponding README files for details.

### Running notebooks

To run the Jupyter notebooks, you need to install their requirements and make
them available as a Jupyter kernel. The following script does this for you (be
sure to pass `deps` as the target directory name!):

```bash
$ ./install-analysis.sh deps
```

This creates a Jupyter kernel named `analysis`, which you can select from within
the Jupyter interface.

If you do not have Jupyter installed, the following command will do it:

```bash
$ pip install --user jupyterlab
```

Then start Jupyter Lab using the command below and navigate to the notebook you
want to open.

```bash
$ jupyter lab
```

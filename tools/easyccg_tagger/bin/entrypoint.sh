#!/usr/bin/env bash
set -eou pipefail

# Get a supertag probability distribution from the EasyCCG tagger.
#
# Usage:
#     entrypoint.sh INPUT_FILE OUTPUT_FILE

INPUT_FILE=$1
OUTPUT_FILE=$2

/bin/add_easyccg_tags.sc "${INPUT_FILE}" > "${OUTPUT_FILE}"

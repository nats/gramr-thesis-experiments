#!/usr/bin/env bash
set -eou pipefail

java -Xmx4g -jar /gramr.jar "$@"

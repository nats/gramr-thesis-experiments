#!/usr/bin/env bash
set -eou pipefail

# Align something with jamr
#
# Usage:
#     entrypoint.sh INPUT_FILE OUTPUT_FILE [jamr flags...]

INPUT_FILE=$1
OUTPUT_FILE=$2
ARGS=( "${@:3}" )

JAMR=/jamr

source "${JAMR}/scripts/config.sh"
java -cp "${JAMR}/target/scala-2.10/jamr-assembly-0.1-SNAPSHOT.jar" edu.cmu.lti.nlp.amr.Aligner "${ARGS[@]}" < ${INPUT_FILE} > ${OUTPUT_FILE}

#!/usr/bin/env bash
set -eou pipefail

# Align something with tamr
#
# Usage:
#     entrypoint.sh [tamr flags...]

python /tamr/amr_aligner/rule_base_align.py $@

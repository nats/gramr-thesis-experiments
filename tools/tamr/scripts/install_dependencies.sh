#!/usr/bin/env bash
set -eou pipefail

pip install nltk==3.3 gensim penman==0.6.2
python -c "import nltk; nltk.download('stopwords'); nltk.download('wordnet')"
#!/usr/bin/env bash
#
# Extracts a list of words from an AMR corpus file.
#
# Usage: extract_concepts.sh AMR_FILE
#
# We use the extracted words to filter the large GloVe vector file for concepts
# that could occur in our corpus. The concept extraction is very rough and
# extracts many things that are not concepts, such as words in comments.
#
# Prints the word list to STDOUT.


set -euo pipefail

AMR_FILE=$1

cat $AMR_FILE \
    | sed -e 's/ /\n/g' \
    | sed -e 's/[#:()/]//g' \
    | sed -e 's/-[0-9][0-9]$//' \
    | sort | uniq

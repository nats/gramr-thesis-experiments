#!/usr/bin/env python2.7
# -*- encoding: utf-8 -*-
#
# Filter a glove file using a given list of words.
#
# Usage: filter_glove.py GLOVE_ZIP WORD_LIST
#
# GLOVE_ZIP is a zipped GloVe file as provided on
# https://nlp.stanford.edu/projects/glove/.
#
# WORD_LIST is a list of words to keep, one per line. All words not in WORD_LIST
# are removed from the GloVe file.
#
# Prints the filtered GloVe file to STDOUT.

import sys
from zipfile import ZipFile

def read_word_list(file_name):
    with open(file_name) as f:
        word_list = {l.strip() for l in f}
    return word_list

def filter_lines(words, in_file):
    print >> sys.stderr, 'Filtering…'
    lines_in = 0
    lines_out = 0
    for line in in_file:
        lines_in += 1
        tokens = line.split(' ', 1)
        if len(tokens) > 0:
            if tokens[0] in words:
                lines_out += 1
                sys.stdout.write(line)
        if lines_in % 10000 == 0:
            print >> sys.stderr, 'Filtering: %d lines in > %d lines out' % (lines_in, lines_out)
        
    print >> sys.stderr, 'Finished filtering'
    print >> sys.stderr, '%d lines in, %d lines out' % (lines_in, lines_out)

def main():
    glove_zip = sys.argv[1]
    word_list = sys.argv[2]

    print >> sys.stderr, 'Reading word list %s' % word_list
    words = read_word_list(word_list)
    print >> sys.stderr, '%d words in list' % len(words)

    print >> sys.stderr, 'Opening GloVe archive %s' % glove_zip
    with ZipFile(glove_zip) as archive:
        member = archive.namelist()[0]
        print >> sys.stderr, 'Opening compressed GloVe file %s' % member
        with archive.open(member) as glove_in:
            filter_lines(words, glove_in)


if __name__ == '__main__':
    main()

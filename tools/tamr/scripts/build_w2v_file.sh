#!/usr/bin/env bash
#
# Download and prepare word vectors for the tamr aligner
#
# Usage:
#   build_w2v_file DATA GLOVE_URL
#
# DATA: path to the data root, which should contain a amr_anno_1.0 directory.
# GLOVE_URL: URL to download GloVe word vector file

set -eou pipefail

DATA=$1
GLOVE_URL=$2

cachedir="/cache"
workdir="/tamr"
mkdir -p "${workdir}"

echo Creating concatenated corpus file
cat "${DATA}/amr_anno_1.0/data/split/training"/* > "${workdir}/corpus.txt"

echo Extracting concepts
./extract_concepts.sh "${workdir}/corpus.txt" > "${workdir}/concepts.txt"

echo Downloading ${GLOVE_URL}
wget -N -c "${GLOVE_URL}" -O "${cachedir}/glove.zip"
echo Filtering GloVe file
./filter_glove.py "${cachedir}/glove.zip" "${workdir}/concepts.txt" > "${workdir}/glove_filtered.txt"
python -m gensim.scripts.glove2word2vec -i "${workdir}/glove_filtered.txt" -o "${workdir}/glove_filtered.w2v"

# ISI Aligner

Since the [original download](https://isi.edu/~damghani/papers/Aligner.zip) of
the ISI aligner is offline, we include it right here in this repository.

#!/usr/bin/env python2.7
from __future__ import print_function

import sys, re

if(len(sys.argv) != 2):
    print('Usage: extract_amr_lines.py <amr corpus file>')
    print('Saves each AMR in a file onto an individual line.')
    exit(1)

amrFileName = sys.argv[1]

amrFile = open(amrFileName)

line = amrFile.readline()
expectingAmr = False
currentAmrLines = []
while line:
    if line.startswith('#'):
        if line.startswith('# ::id'):
            expectingAmr = True
            currentAmrLines = []
    else:
        content = line.strip()
        if expectingAmr:
            if len(content) > 0:
                currentAmrLines.append(content)
            elif len(currentAmrLines) > 0:
                print(" ".join(currentAmrLines))
                expectingAmr = False
    line = amrFile.readline()

amrFile.close()

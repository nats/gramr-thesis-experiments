#!/usr/bin/env bash
set -euo pipefail

# Run the ISI aligner on an AMR corpus file.
#
# Usage:
#     entrypoint.sh <AMR Input File> <Alignment Output File>
#
# The AMR input needs to have a tok attribute, containing tokenized sentences.


AMR_IN=$1
ALIGNMENTS_OUT=$2

scripts=/scripts
work=/work
ISI_ALIGNER=/isialigner
MGIZA=/mgiza

mkdir -p ${work}
${scripts}/extract_amr_lines.py ${AMR_IN} > ${work}/amr_lines
${scripts}/amr_to_raw.py tok ${AMR_IN} > ${work}/snt_lines
pushd ${ISI_ALIGNER}
echo "AMR=${work}/amr_lines" > addresses.keep
echo "ENG=${work}/snt_lines" >> addresses.keep
echo "MGIZA_SCRIPT=$MGIZA/mgizapp/scripts" >> addresses.keep
echo "MGIZA_BIN=$MGIZA/mgizapp/bin" >> addresses.keep
./run.sh
popd
mv ${ISI_ALIGNER}/Alignments.keep ${ALIGNMENTS_OUT}

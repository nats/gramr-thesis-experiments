#!/usr/bin/env bash

java -cp "*" -Xmx4g edu.stanford.nlp.pipeline.StanfordCoreNLP "$@"

#!/usr/bin/env bash
set -eou pipefail

# Parse a file with EasyCCG.
#
# Usage:
#    entrypoint.sh INPUT_FILE OUTPUT_FILE [EasyCCG args…]
#
# INPUT_FILE is a file of raw tokenized sentences, one per line.

INPUT_FILE=$1
OUTPUT_FILE=$2
ARGS=( "${@:3}" )

export EASYCCG=/easyccg
java -Xmx8g -jar "$EASYCCG/easyccg.jar" -m "$EASYCCG/model_rebank" -f "$INPUT_FILE" "${ARGS[@]}" > $OUTPUT_FILE

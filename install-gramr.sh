#!/usr/bin/env bash
# Installs gramr.

# Prerequisites:
#   - git
#   - sbt

set -e

DEPSDIR=$1

GRAMR_VERSION=306a

echo Installing gramr
git clone https://gitlab.com/nats/gramr.git ${DEPSDIR}/gramr
cd ${DEPSDIR}/gramr
git checkout v${GRAMR_VERSION}
sbt assembly
mv target/scala-2.12/gramr-assembly-${GRAMR_VERSION}.jar gramr.jar

#!/usr/bin/env bash

# Prepares the AMR 1.0 corpus for parsing experiments.
#
# Usage:
#     preprocess.sh
#
# Prerequisites:
#     - The AMR corpus (amr_anno_1.0) must be present as a subdirectory of
#       ./data/
#     - The required docker images must be present on the local machine

set -euo pipefail

base="$(realpath $(dirname $0))"
host_data="$base/data"

"$base/build.sh" preprocess-parsing
docker-compose run --rm preprocess-parsing --hostData $host_data $@

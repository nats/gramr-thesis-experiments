#!/usr/bin/env bash
set -e

# Extract ELMo vectors

SCRIPT_DIR=$(readlink -f $(dirname "$0"))
ROOT_DIR="${SCRIPT_DIR}/.."

data="${ROOT_DIR}/data"
elmo_data="${data}/resources/elmo"
venv="${ROOT_DIR}/deps/venv"

source "${venv}/bin/activate"
mkdir -p "${elmo_data}"
cd "${elmo_data}"

splits=( training dev test )
for split in "${splits[@]}"; do
    python3 -m amrcorpustools.sentences --attribute tok < "${data}/processed/${split}/amr-release-1.0-${split}-proxy.txt" > "${split}-proxy-sentences.txt"
    python3 -m amrcorpustools.sentences --attribute id < "${data}/processed/${split}/amr-release-1.0-${split}-proxy.txt" > "${split}-proxy-ids.txt"

    allennlp elmo "${split}-proxy-sentences.txt" "${split}-proxy-elmo.hdf5" --all
done

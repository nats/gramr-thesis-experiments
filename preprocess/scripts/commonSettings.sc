import java.io._

import gramr.amr.alignments.Combiners.intersectOrKeep
import gramr.amr.{AmrCorpusItem, AmrCorpusLoader, AttributeFileParser}
import gramr.ccg.{CombinatorSystem, Derivation, DerivationStep, SyntacticCategory}
import gramr.experiments.Example
import gramr.experiments.ScriptSupport._
import gramr.graph.Predicates._
import gramr.graph.lexemes.{LemmaFiller, PropbankFiller, QuotedFiller, WildcardFiller}
import gramr.graph.{AdjacencyMeaningGraph, Graph}
import gramr.learn._
import gramr.learn.util.{FeatureHasher, FeatureIndex}
import gramr.parse.chart.{Chart, State}
import gramr.parse.{ActionPredicate, ActionPredicates, and}
import gramr.text.{HasMR, Span}

import scala.collection.immutable.HashMap
import scala.util.Random


type MR = AdjacencyMeaningGraph


// ------------------------------------------------------------------
// Grammar
// ------------------------------------------------------------------

val wildcardFillers: scala.collection.immutable.Iterable[WildcardFiller] =
  scala.collection.immutable.Iterable(LemmaFiller(), QuotedFiller(), PropbankFiller())


// ------------------------------------------------------------------
// Loading data
// ------------------------------------------------------------------

type Loader = AmrCorpusLoader[AdjacencyMeaningGraph, Example[AdjacencyMeaningGraph]]

def baseLoader(implicit cs: CombinatorSystem) = AmrCorpusLoader[AdjacencyMeaningGraph, Example[AdjacencyMeaningGraph]]

val alignmentLoaders = Map(
  "gold" -> {
    loader: Loader => loader.useIsiAlignment("alignments-gold")
  },
  "jamr" -> {
    loader: Loader => loader.useJamrAlignment("alignments-jamr")
  },
  "isi" -> {
    loader: Loader => loader.useIsiAlignment("alignments-isi")
  },
  "tamr" -> {
    loader: Loader => loader.useJamrAlignment("alignments-tamr")
  },
  "jamr+isi" -> { loader: Loader =>
    loader
      .useJamrAlignment("alignments-jamr", "alignments-jamr")
      .useIsiAlignment("alignments-isi", "alignments-isi")
      .combineAlignments("alignments-jamr", "alignments-isi", intersectOrKeep)
  },
  "isi+tamr" -> { loader: Loader =>
    loader
      .useJamrAlignment("alignments-tamr", "alignments-tamr")
      .useIsiAlignment("alignments-isi", "alignments-isi")
      .combineAlignments("alignments-tamr", "alignments-isi", intersectOrKeep)
  },
  "jamr+tamr" -> { loader: Loader =>
    loader
      .useJamrAlignment("alignments-jamr", "alignments-jamr")
      .useJamrAlignment("alignments-tamr", "alignments-tamr")
      .combineAlignments("alignments-jamr", "alignments-tamr", intersectOrKeep)
  },
  "jamr+isi+tamr" -> { loader: Loader =>
    loader
      .useJamrAlignment("alignments-jamr", "alignments-jamr")
      .useIsiAlignment("alignments-isi", "alignments-isi")
      .useJamrAlignment("alignments-tamr", "alignments-tamr")
      .combineAlignments("alignments-jamr", "alignments-isi", intersectOrKeep)
      .combineAlignments("alignments", "alignments-tamr", intersectOrKeep)
  }
)

def loadInductionData(
  corpus: String,
  alignments: String,
  nExamples: Option[Int] = None,
  ccgDerivations: Option[Int] = None,
  only: Option[String] = None,
  preprocessors: Seq[AmrCorpusItem => AmrCorpusItem] = Seq()
)(implicit random: Random, cs: CombinatorSystem): Seq[Example[MR]] = {
  val alignmentLoader = alignmentLoaders(alignments)(baseLoader)
  val derivationLimitedLoader = ccgDerivations match {
    case None => alignmentLoader
    case Some(count) => alignmentLoader.addPreprocessor(item => item.limitCcgDerivations(count))
  }
  val preprocessedLoader = preprocessors.foldLeft(derivationLimitedLoader)((l, p) => l.addPreprocessor(p))
  val loader = preprocessedLoader
  val dataSet = AmrCorpusLoader.loadDataSet(loader, corpus, nExamples)

  val filteredDataSet = only match {
    case Some(id) => dataSet.filter(_.id == id)
    case None => dataSet
  }

  filteredDataSet
}

def loadTrainData(
  train: String,
  alignmentLoader: String,
  tagDir: String,
  trainSize: Option[Int],
  maxSentenceLength: Int,
  supertagFile: String = "train_tags_pred.txt"
)(implicit random: Random, cs: CombinatorSystem): Seq[Example[AdjacencyMeaningGraph]] = {
  val loader = alignmentLoaders(alignmentLoader)(baseLoader)
  val trainSupertagFile = new File(tagDir, supertagFile)
  val trainSupertagDict = AttributeFileParser.parseFile(trainSupertagFile)

  val xLoader = loader
    .extendAttributes(trainSupertagDict)

  AmrCorpusLoader.loadDataSet(xLoader, train, trainSize, Some(maxSentenceLength))
}

def loadTestData(
  test: String,
  tagDir: String,
  testSize: Option[Int],
  supertagFile: Option[String] = None
)(implicit random: Random, cs: CombinatorSystem): Seq[Example[AdjacencyMeaningGraph]] = {
  val loader = baseLoader

  val xLoader = supertagFile match {
    case Some(fileName) =>
      val valSupertagFile = new File(tagDir, fileName)
      val valSupertagDict = AttributeFileParser.parseFile(valSupertagFile)
      loader.extendAttributes(valSupertagDict)
    case _ => loader
  }

  AmrCorpusLoader.loadDataSet(xLoader, test, testSize, shuffle=false)
}


def leafCategories(example: Example[AdjacencyMeaningGraph], span: Span): Iterable[SyntacticCategory] = {
  if(span.size == 1)
    if(span.start < example.ccgTags.size)
      example.ccgTags(span.start).map(_._1)
    else Iterable("N", "N/N").map(SyntacticCategory.apply)
  else Iterable.empty
}

// ------------------------------------------------------------------
// Parsing
// ------------------------------------------------------------------

implicit def graphInstance: Graph[AdjacencyMeaningGraph] = AdjacencyMeaningGraph.graphInstance
implicit def exampleInstance: gramr.text.Example[Example[AdjacencyMeaningGraph]] = Example.ops.exampleInstance
implicit def hasMrInstance: HasMR[Example[AdjacencyMeaningGraph], AdjacencyMeaningGraph] = Example.ops.hasMRInstance

def rootCategories(derivation: Derivation[Unit]): Iterable[SyntacticCategory] = {
  def rootStepCategories(step: DerivationStep[Unit]): Iterable[SyntacticCategory] = {
    val subStepCats = step.subSteps match {
      case subStep :: Nil => rootStepCategories(subStep)
      case _ => Iterable.empty
    }
    Iterable(step.synCat.eraseFeatures) ++ subStepCats
  }
  val cats = rootStepCategories(derivation.root)
  cats
}

val actionFilter: ActionPredicate[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] = and(
  ActionPredicates.matchSyntacticArity,
  // limitNamedEntities(NamedEntities.proxyReductions),
  ActionPredicates.rootCategoryFilter(example => example.derivations.flatMap(rootCategories).toSet),
  // ActionPredicates.rootCategoryFilter(_ => Set("S", "S[dcl]", "S[wq]", "S[q]", "NP", "N").map(SyntacticCategory.apply)),
  ActionPredicates.mrPredicate(mr => checkPolarityEdges(mr) && checkConstantDegrees(mr) && checkArgEdges(mr) && checkNumberedEdgesUnique(mr) && checkInstanceConcepts(mr))
)

val evaluationFunction = (mr: AdjacencyMeaningGraph, e: Example[AdjacencyMeaningGraph]) =>
  gramr.graph.evaluate.kernelSmatch(1, 4)(mr, e)


def costFunction(state: State[AdjacencyMeaningGraph, MapFeatureVector], example: Example[AdjacencyMeaningGraph]): Double = {
  val smatch = gramr.graph.evaluate.kernelSmatch[AdjacencyMeaningGraph, Example[AdjacencyMeaningGraph]](1, 0)(state.mr, example)
  1.0 - smatch.f1
}


def mkUpdateStrategy(update: String)(implicit fv: FeatureVector[MapFeatureVector]): UpdateStrategy[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] = {
  val earlyUpdateStrategy: UpdateStrategy[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] =
    new EarlyUpdateStrategy(freeChartName = "free", oracleChartName = "oracle")
  val costUpdateStrategy: UpdateStrategy[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] =
    new CostSensitiveUpdateStrategy(costFunction)

  update match {
    case "early" =>
      earlyUpdateStrategy
    case "cost" =>
      costUpdateStrategy
    case "early+cost" =>
      UpdateStrategy.fallBack(
        UpdateStrategy.choose { (example: Example[AdjacencyMeaningGraph], charts: Map[String, Chart[AdjacencyMeaningGraph, MapFeatureVector]]) =>
          if(charts("free").outputStates.nonEmpty && charts("oracle").outputStates.nonEmpty) {
            val bestFree = charts("free").outputStates.map(state => evaluationFunction(state.mr, example).f1).max
            val bestOracle = charts("oracle").outputStates.map(state => evaluationFunction(state.mr, example).f1).max
            if(bestFree > bestOracle) costUpdateStrategy else earlyUpdateStrategy
          }
          else costUpdateStrategy
        },
        costUpdateStrategy
      )
  }
}


def mkLearner(learnerType: String, model: LinearModel)(implicit fv: FeatureVector[MapFeatureVector]): LinearLearner[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] = {
  learnerType match {
    case "adadelta" => new AdadeltaLearner[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](model)
    case "perceptron" => new PerceptronLearner[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]](model)
  }
}

def emptyLearner(learnerType: String): LinearLearner[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] = {
  implicit val featureIndex: FeatureIndex = new FeatureHasher(100000)
  implicit val fv: FeatureVector[MapFeatureVector] = MapFeatureVector.featureVectorLike
  val initialParameters: LinearModel = new LinearModel(HashMap.empty[Int, Double], _ => 0.0)
  mkLearner(learnerType, initialParameters)
}

def loadLearner(learnerType: String, modelFile: String): LinearLearner[AdjacencyMeaningGraph, MapFeatureVector, Example[AdjacencyMeaningGraph]] = {
  logger.info(s"Loading model $modelFile")
  val modelIn = new ObjectInputStream(new FileInputStream(new File(modelFile)))
  val model: LinearModel = modelIn.readObject().asInstanceOf[LinearModel]
  modelIn.close()
  implicit val featureIndex: FeatureIndex = model.featureIndex
  implicit val fv: FeatureVector[MapFeatureVector] = MapFeatureVector.featureVectorLike
  mkLearner(learnerType, model)
}

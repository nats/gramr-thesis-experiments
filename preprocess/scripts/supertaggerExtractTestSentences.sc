#!/usr/bin/env amm

import $file.corpus
import java.io.{File, FileWriter, PrintWriter}

import ammonite.ops._
import corpus.{Corpus, Example}


@main
def main(
  corpus: Path,
  output: Path
): Unit = {

  val examples = Corpus.read(corpus)

  val writer = new PrintWriter(new FileWriter(new File(output.toString)))
  examples.foreach { example =>
    writer.println(example.id)
    val tokens = example.attributes("tok").head
    writer.println(tokens)
    val tags = bestCcgTags(parseCcgTags(example), tokens.split(" ").size)
    writer.print("syncat: ")
    writer.println(tags.map(_.syncat).mkString(" "))
    writer.println()
  }
  writer.close()
}


case class CcgTagEntry(
  tokenIndex: Int,
  syncat: String,
  probability: Double
)


def parseCcgTags(example: Example, attributeName: String = "supertags"): Seq[CcgTagEntry] = {
  val attrStr = example.attributes(attributeName).head
  val entries = attrStr.split("\\|").map { entryStr =>
    val entryFields = entryStr.trim.split(" ").toSeq
    entryFields match {
      case Seq(tokenIndex, syncat, probability) =>
        CcgTagEntry(tokenIndex.toInt, syncat, probability.toDouble)
    }
  }
  entries
}

/** Select only the best CCG tag for every token.
  */
def bestCcgTags(entries: Seq[CcgTagEntry], tokenCount: Int): Seq[CcgTagEntry] = {
  val bestEntries: Map[Int, CcgTagEntry] = entries.groupBy(_.tokenIndex).mapValues { tokenEntries =>
    tokenEntries.maxBy(_.probability)
  }.toMap
  (0 until tokenCount) map { tokenIndex =>
    bestEntries.getOrElse(tokenIndex, CcgTagEntry(tokenIndex, "UNK", 1.0))
  }
}

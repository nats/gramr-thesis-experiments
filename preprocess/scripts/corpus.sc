#!/usr/bin/env amm

/** Lightweight Scala implementation of simple AMR corpus manipulations.
  */

import $file.ChunkIterator
import ChunkIterator.ChunkIterator

import scala.collection.immutable.{SeqMap, VectorMap}
import scala.io.Source
import java.io._
import ammonite.ops._

@doc("Add attributes from an attribute file to an AMR corpus.")
@main
def addAttributes(
    corpus: Path     @doc("AMR corpus file"),
    attributes: Path @doc("File containing line-separated attribute blocks starting with # ::id"),
    output: Path     @doc("Output path")
): Unit = {
    val examples = Corpus.read(corpus)
    val attrExamples = Corpus.groupById(Corpus.read(attributes))

    val modifiedCorpus = examples.map { example =>
        attrExamples.get(example.id) match {
            case Some(attrExample) =>
                example.copy(attributes = Attributes.merge(example.attributes, attrExample.attributes))
            case None =>
                example
        }
    }

    Corpus.write(modifiedCorpus, output)
}

@doc("Dummy command to enforce supplying a command name")
@main
def dummy(): Unit = {}


type Attributes = SeqMap[String, Seq[String]]

object Attributes {
    def from(tuples: Iterable[(String, String)]): Attributes = {
        tuples.foldLeft(VectorMap.empty[String, Seq[String]]) {
            case (agg, (k, v)) =>
                agg.updated(k, agg.getOrElse(k, Seq()) ++ Seq(v))
        }
    }

    def explode(attr: Attributes): Seq[(String, String)] = {
        attr.toSeq.flatMap { case (k, vs) => for(v <- vs) yield (k, v) }
    }


    def merge(attr1: Attributes, attr2: Attributes): Attributes = {
        val exploded1 = explode(attr1)
        val exploded2 = explode(attr2)
        Attributes.from((exploded1 ++ exploded2).distinct)
    }
}


case class Example(attributes: Attributes, amr: Option[String] = None) {
    def id: String = attributes("id").head

    override def toString: String = {
        val attributeString = Attributes.explode(attributes).map {
            case (k, v) =>
                s"# ::$k $v"
        }.mkString("\n") ++ "\n"
        attributeString ++ amr.getOrElse("")
    }
}


object Example {
    /** Read an example from a String iterator of lines.
      */
    def read(chunk: Iterable[String]): Option[Example] = {
        val (commentLines, amrLines) = chunk.span { line => line.startsWith("#") }
        val attribLines = commentLines.filter { line => line.startsWith("# ::") }
        
        val nameValuePairs: Seq[(String, String)] = attribLines.flatMap(parseAttributeLine).toSeq
        val attributes: Attributes = Attributes.from(nameValuePairs)

        val amr: Option[String] = if(amrLines.isEmpty) None else Some(amrLines.mkString("\n"))

        // Require that either an AMR or an ::id attribute be present
        if(attributes.contains("id") || amr.isDefined) {
            Some(Example(attributes, amr))
        }
        else {
            None
        }
    }

    def parseAttributeLine(line: String): Seq[(String, String)] = {
        val segments = line.split("::").toVector.tail
        val nameValuePairs = segments.map { segment =>
            segment.split(" ", 2).toList match {
                case attrName :: attrValue :: Nil =>
                    (attrName, attrValue.trim)
                case attrName :: Nil =>
                    (attrName, "")
            }
        }
        nameValuePairs
    }
}


type Corpus = Seq[Example]


object Corpus {
    /** Read a corpus from a file and return its examples by ID.
     */
    def read(corpus: Path): Corpus = {
        val lineIterator = Source.fromFile(new File(corpus.toString)).getLines
        val chunkIterator = new ChunkIterator(lineIterator)

        val examples: Seq[Example] = chunkIterator.map { chunk =>
            Example.read(chunk)
        }.toSeq.flatten
        
        examples
    }

    def write(corpus: Corpus, output: Path): Unit = {
        val writer = new BufferedWriter(new FileWriter(new File(output.toString)))
        corpus.foreach { example =>
            writer.write(example.toString)
            writer.write("\n\n")
        }
        writer.close()
    }

    def groupById(corpus: Corpus): Map[String, Example] = {
        corpus.groupBy(_.attributes("id").head).mapValues(_.head).toMap
    }
}

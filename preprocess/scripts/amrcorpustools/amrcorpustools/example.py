import sys
from typing import List, Dict, Optional, Tuple

from tqdm import tqdm


def load_corpus(file_name):
    with open(file_name) as f:
        lines = f.readlines()
    return parse_corpus(lines)


def parse_corpus(lines) -> List['Example']:
    print('Parsing', file=sys.stderr)
    examples = []
    index = 0

    with tqdm(total=len(lines)) as pbar:
        while index < len(lines):
            last_index = index
            example, index = Example.parse(lines, index)
            if example:
                examples.append(example)
            pbar.update(index - last_index)

    return examples


def write_corpus(examples, file):
    for example in examples:
        example.write(file)
        file.write('\n')


class Example:
    def __init__(self, attributes: Dict[str, List[str]] = None, amr: Optional[str] = None):
        self.attributes = attributes if attributes else {}
        self.amr = amr

    @staticmethod
    def parse(lines: List[str], index: int) -> Tuple[Optional['Example'], int]:
        example = Example()

        while index < len(lines) and lines[index].strip() == '':
            index += 1

        if index >= len(lines):
            return None, index

        while index < len(lines) and lines[index].strip().startswith('#'):
            line_cnt = lines[index].strip()[1:].strip()
            for entry in line_cnt.split('::')[1:]:
                name_val = entry.strip().split(' ', 1)
                name = name_val[0]
                val = name_val[1] if len(name_val) > 1 else ''  # Some attributes can occur without any value
                example.add_attribute(name, val)
            index += 1

        amr_str = ''
        while index < len(lines) and lines[index].strip() != '':
            amr_str += lines[index]
            index += 1
            example.amr = amr_str

        if not example.amr:
            return None, index

        return example, index

    def write(self, file):
        for k in self.attributes:
            for v in self.attributes[k]:
                file.write('# ::{} {}\n'.format(k, v))

        file.write(self.amr)

    def add_attribute(self, name: str, value: str):
        if name in self.attributes:
            self.attributes[name].append(value)
        else:
            self.attributes[name] = [value]

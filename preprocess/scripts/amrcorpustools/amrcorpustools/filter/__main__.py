import sys
from argparse import ArgumentParser

from amrcorpustools.example import parse_corpus, write_corpus, Example


def main():
    argparser = ArgumentParser(
        usage='Filters an AMR corpus according to a number of filter criteria.')
    argparser.add_argument('--input', '-i', required=False, help='The AMR corpus to reorder')
    argparser.add_argument('--amr', type=str, required=False,
                           help='Filters out all examples with a given AMR. E.g., all unparsed'
                                'sentences can be removed using `--amr (n / noparse)`.')

    args = argparser.parse_args()

    input_file = open(args.input) if args.input else sys.stdin
    output_file = sys.stdout

    predicate = NoFilter()
    if args.amr:
        predicate = AndFilter(predicate, AmrFilter(args.amr))

    input_examples = parse_corpus(input_file.readlines())
    filtered_examples = [e for e in input_examples if predicate(e)]
    write_corpus(filtered_examples, output_file)


class NoFilter:
    def __call__(self, example: Example):
        return True


class AndFilter:
    def __init__(self, *filters):
        self.filters = filters

    def __call__(self, example: Example):
        return all(f(example) for f in self.filters)


class AmrFilter:
    def __init__(self, amr):
        self.amr = amr.strip()

    def __call__(self, example: Example):
        return example.amr.strip() != self.amr


if __name__ == '__main__':
    main()

from amrcorpustools.example import Example, parse_corpus


def test_parse_example():
    example_lines = '''
||    # ::id 12345
||    # ::str abc
||    (a / xyz
||        :bla (b / ABC))
||    '''.split('||')

    example, rest = Example.parse(example_lines)
    assert 3 == len(example.amr.split('\n'))
    assert 1 == len(rest)
    assert example.attributes['id'][0] == '12345'
    assert example.attributes['str'][0] == 'abc'


def test_parse_corpus():
    corpus_lines = '''
||    # bla bla bla
||
||    # ::id 12345
||    # ::str abc
||    (a / xyz
||        :bla (b / ABC))
||
||    # ::id 23456
||    # ::str uiae
||    (a / xyz
||        :bla (b / ABC))
||
||    '''.split('||')

    examples = parse_corpus(corpus_lines)
    assert len(examples) == 2

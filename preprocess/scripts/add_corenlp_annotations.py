#!/usr/bin/env python3

import sys
import itertools

if(len(sys.argv) != 3):
    print('Usage: add_corenlp_annotations.py <amr corpus file> <conll annotation file>')

amrFileName = sys.argv[1]
conllFileName = sys.argv[2]

amrFile = open(amrFileName)
conllFile = open(conllFileName)

def show_span(span):
    return '{},{},{}'.format(span.start, span.end, span.label_)

def show_arc(t):
    return '{},{},{}'.format(t.head.i, t.i, t.dep_)

def print_attrs():
    lines = []
    line = conllFile.readline()
    while line.strip():
        lines.append(line.strip().split("\t"))
        line = conllFile.readline()
    
    # Columns are: wordIndex, token, lemma, POS, NER, head, depRel
    tokens = [l[1] for l in lines]
    lemmas = [l[2] for l in lines]
    pos = [l[3] for l in lines]
    ner = [l[4] for l in lines]
    
    print('# ::tok {}'.format(' '.join(t for t in tokens)))
    print('# ::lemmas {}'.format(' '.join(l for l in lemmas)))
    print('# ::pos {}'.format(' '.join(p for p in pos)))

    try:
        def depHead(headIndex, childIndex):
            if headIndex == 0:
                return childIndex
            else:
                return headIndex - 1

        dep = [(depHead(int(l[5]), i), i, l[6]) for i, l in enumerate(lines)]

        print('# ::dep {}'.format(' '.join('{},{},{}'.format(d[0], d[1], d[2]) for d in dep)))
    except ValueError:
        pass

    nerGroups = [list(g) for k, g in itertools.groupby(enumerate(ner), key=lambda x: x[1]) if k != 'O']
    def span(s):
        keys = [k for k, g in s]
        return min(keys), max(keys) + 1, s[0][1]
    nerSpans = [span(s) for s in nerGroups]
    
    print('# ::ent {}'.format(' '.join('{},{},{}'.format(s[0], s[1], s[2]) for s in nerSpans)))


line = amrFile.readline()
wasInComment = False
snt = None
while line:
    if line.startswith("# ::"):
        wasInComment = True
        if line.startswith("# ::snt"):
            snt = line[8:].strip()
    else:
        if wasInComment:
            # We're at the end of the comment section now
            print_attrs()
        wasInComment = False
    print(line, end="")
    line = amrFile.readline()

conllFile.close()
amrFile.close()

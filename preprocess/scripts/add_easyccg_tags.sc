#!/usr/bin/env amm

import ammonite.ops._
import $ivy.`org.apache.commons:commons-lang3:3.6`
val easyccgPath = Path(scala.util.Properties.envOrNone("EASYCCG").get)
interp.load.cp(easyccgPath/"easyccg.jar")

@

import java.io.File
import scala.io.Source
import scala.collection.JavaConverters._
import org.apache.commons.lang3.reflect.FieldUtils
import uk.ac.ed.easyccg
import easyccg.main.EasyCCG.InputFormat
import easyccg.syntax.{TaggerEmbeddings, InputReader}
import easyccg.syntax.SyntaxTreeNode.SyntaxTreeNodeFactory

@main
def main(infile: String): Unit = {
  val model = new File((easyccgPath/"model_rebank").toString)
  val tagger = new TaggerEmbeddings(model, 300, 0.5, 10)
  val reader = InputReader.make(InputFormat.TOKENIZED, null)

  val inputFile = Source.fromFile(infile)
  for(line <- inputFile.getLines) {
    val input = reader.readInput(line)
    val tagged = tagger.tag(input.getInputWords)
    print("# ::supertags")
    var first = true
    for((wordTags, wordIdx) <- tagged.asScala.zipWithIndex; word <- wordTags.asScala) {
      val cat = FieldUtils.readField(word, "category", true)
      val logprob = FieldUtils.readField(word, "probability", true).asInstanceOf[Double]
      val probability = Math.exp(logprob)
      if(!first) {
        print(" |")
      }
      first = false
      print(s" $wordIdx $cat $probability")
    }
    println()
  }
}

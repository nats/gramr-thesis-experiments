#!/usr/bin/env amm

import ammonite.ops._

@main
def proxy(
    data: Path = Path("/data"),
    hostData: Path = Path("/data"),
    base: Path = Path("/data/amr_anno_1.0/data/split"),
    work: Path = Path("/data/work"),
    target: Path = Path("/data/processed"),
    trainIsi: Boolean = true,
    alignIsi: Boolean = true
): Unit = {
    val p = new Preprocess(
        data = data,
        hostData = hostData,
        base = base,
        work = work,
        target = target,
        alignIsi=alignIsi)
    if(trainIsi) {
        p.createIsiAlignments()
    }
    else {
        p.useIsiAlignments()
    }
    p.processTrainFile("amr-release-1.0-training-proxy.txt")
    p.processDevFile("amr-release-1.0-dev-proxy.txt")
    p.processTestFile("amr-release-1.0-test-proxy.txt")
}

class Preprocess(
    val data: Path,
    val hostData: Path,
    val base: Path,
    val work: Path,
    val target: Path,
    val alignIsi: Boolean = true,
    var isiAlignments: Option[Path] = None,
    var tamrAlignments: Option[Path] = Some(Path("/resources/amr-release-1.0-training_fix.txt.sd_tok.tamr_alignment"))
) {

    implicit val wd = pwd

    def createIsiAlignments(): Unit = {
        val workDir = work / "isialign"
        mkdir(workDir)

        %bash("concat.sh", base)
        val combinedAmrFile = base / "all.txt"

        val annotatedAmrFile = processWithCorenlp(combinedAmrFile, workDir, Seq("tokenize", "ssplit"))

        val alignmentPath = workDir / "alignments.txt"

        docker("xian9426/isialigner", dockerPath(annotatedAmrFile).toString, dockerPath(alignmentPath).toString)
        
        // Convert the alignments so they have the proper IDs
        val convertedAlignmentFile = workDir / "converted-alignments.txt"
        %bash("-c", s"""./alignments_to_attr.py "$combinedAmrFile" "$alignmentPath" > "$convertedAlignmentFile"""")
        isiAlignments = Some(convertedAlignmentFile)
    }
    
    def useIsiAlignments(): Unit = {
        val workDir = work / "isialign"
        isiAlignments = Some(workDir / "converted-alignments.txt")
    }

    def processTrainFile(fileName: String): Unit = {
        processFile(fileName, "training", useTamrRelease=true)
    }

    def processDevFile(fileName: String): Unit = {
        val processed = processFile(fileName, "dev", useTamrRelease=false)
        extractTaggerTestData(processed, target / "dev" / fileName)
    }

    def processTestFile(fileName: String): Unit = {
        val processed = processFile(fileName, "test", useTamrRelease=false)
        extractTaggerTestData(processed, target / "test" / fileName)
    }

    def processFile(fileName: String, section: String, useTamrRelease: Boolean = false): Path = {
        val inFile = base / section / fileName
        val outFile = target / section / fileName
        println(s"Processing $section file")
        println(s"<< $inFile")
        println(s">> $outFile")

        val workDir = work / section / fileName
        mkdir(workDir)

        var intermediate = processWithCorenlp(inFile, workDir / "01corenlp")
        // tamr needs to be the first aligner as it’s sensitive to other
        // alignment attributes being present
        if(useTamrRelease) {
            intermediate = addTamrAlignments(intermediate, tamrAlignments.get, workDir / "02tamr")
        }
        else {
            intermediate = processWithTamr(intermediate, workDir / "02tamr")
        }
        if(alignIsi) {
            intermediate = addIsiAlignments(intermediate, isiAlignments.get, workDir / "03isi")
        }
        intermediate = processWithJamr(intermediate, workDir / "04jamr")
        intermediate = processWithAmrUd(intermediate, workDir / "05amr_ud")
        intermediate = processWithEasyccg(intermediate, workDir / "06easyccg")
        intermediate = processWithEasyccgTagger(intermediate, workDir / "07ccgtagger")
        
        cp.over(intermediate, target / section / fileName)

        intermediate
    }

    def processWithCorenlp(
        inAmrFile: Path,
        workDir: Path,
        annotators: Seq[String] = Seq("tokenize", "ssplit", "pos", "lemma", "ner"),
        outputFormat: String = "conll"
    ): Path = {
        println("Adding CoreNLP annotations")
        mkdir(workDir)

        val rawFile = workDir / "raw"
        extractRawAttribute(inAmrFile, rawFile)

        val outFile = workDir / "amr-annotated.txt"
        val annotatorsString = annotators.mkString(",")
        docker("xian9426/corenlp",
            "-annotators", annotatorsString,
            "-ssplit.eolonly", "true",
            "-file", dockerPath(rawFile).toString,
            "-outputFormat", outputFormat,
            "-outputDirectory", dockerPath(workDir).toString)
        val conllFile = workDir / s"raw.$outputFormat"

        %bash("-c", s"""python3 add_corenlp_annotations.py "$inAmrFile" "$conllFile" > "$outFile"""")

        outFile
    }

    def processWithEasyccg(inAmrFile: Path, workDir: Path, nbest: Int = 50): Path = {
        println("Parsing with EasyCCG")
        
        mkdir(workDir)
        val outFile = workDir / "amr-annotated.txt"
        
        val rawFile = workDir / "raw.txt"
        extractRawAttribute(inAmrFile, rawFile, attribute="tok")

        val parsesFile = workDir / "parses.txt"

        docker("xian9426/easyccg",
            dockerPath(rawFile).toString,
            dockerPath(parsesFile).toString,
            "-l", "50",
            "-i", "tokenized",
            "--supertaggerbeam", "0.0001",
            "--nbest", "50")
        
        %bash("-c", s"""./amr_add_derivations.py "$inAmrFile" "$parsesFile" > "$outFile"""")
        
        outFile
    }

    def processWithEasyccgTagger(inAmrFile: Path, workDir: Path): Path = {
        println("Tagging with EasyCCG")
        
        mkdir(workDir)
        val outFile = workDir / "amr-annotated.txt"
        
        val rawFile = workDir / "raw.txt"
        extractRawAttribute(inAmrFile, rawFile, attribute="tok")

        val tagsFile = workDir / "tags.txt"

        docker("xian9426/easyccg_tagger",
            dockerPath(rawFile).toString,
            dockerPath(tagsFile).toString)
        
        %bash("-c", s"""./amr_add_sequenced_attributes.py "$tagsFile" < "$inAmrFile" > "$outFile"""")
        
        outFile
    }

    def addIsiAlignments(inAmrFile: Path, alignmentFile: Path, workDir: Path): Path = {
        println("Adding isi alignments")
        mkdir(workDir)

        val outFile = workDir / "amr-annotated.txt"
        %bash("-c", s"""./amr_add_attribute.py "$alignmentFile" alignments-isi < "$inAmrFile" > "$outFile"""")
        outFile
    }

    def addTamrAlignments(inAmrFile: Path, alignmentFile: Path, workDir: Path): Path = {
        mkdir(workDir)
        val outFile = workDir / "amr-annotated.txt"

        println("Adding tamr alignments")
        %bash("-c", s"""./amr_add_attribute.py ${alignmentFile} alignments-tamr < "$inAmrFile" > "$outFile"""")
        
        outFile
    }

    def processWithJamr(inAmrFile: Path, workDir: Path, printNodesAndEdges: Boolean = false, renameAttribute: Option[String] = Some("jamr-alignments")): Path = {
        mkdir(workDir)
        val outFile = workDir / "amr-annotated.txt"
        val jamrFile = workDir / "aligned.txt"

        println("Processing with jamr")

        val flags = (Seq("-v", "0")
            ++ (if(printNodesAndEdges) Seq("--print-nodes-and-edges") else Seq()))
        val args = Seq(dockerPath(inAmrFile).toString, dockerPath(jamrFile).toString)
        docker("xian9426/jamr", (args ++ flags): _*)

        renameAttribute match {
            case Some(attributeName) =>
                this.renameAttribute(jamrFile, outFile, "alignments", attributeName)
            case None =>
                cp.over(jamrFile, outFile)
        }

        outFile
    }

    def processWithTamr(inAmrFile: Path, workDir: Path, cutComment: Boolean = true): Path = {
        mkdir(workDir)
        val outFile = workDir / "amr-annotated.txt"

        println("Processing with tamr")

        val jamrProcessed = processWithJamr(inAmrFile, workDir / "jamr", printNodesAndEdges = true, renameAttribute=None)

        // cut out the leading comment in the file
        val cut = if(cutComment) {
            val result = workDir / "jamr-for-tamr.cut.txt"
            %bash("-c", s"tail -n +3 $jamrProcessed > $result")
            result
        }
        else {
            jamrProcessed
        }

        val alignmentFile = workDir / "tamr-alignments.txt"
        docker("xian9426/tamr",
            "-verbose",
            "-data", dockerPath(cut).toString,
            "-output", dockerPath(alignmentFile).toString,
            "-wordvec", "/tamr/glove_filtered.w2v",
            "-trials", "10000", "-improve_perfect", "-morpho_match", "-semantic_match")
            
        val added = addTamrAlignments(inAmrFile, alignmentFile, workDir / "addTamrAlignments")
        
        cp.over(added, outFile)
            
        outFile
    }

    def processWithAmrUd(inAmrFile: Path, workDir: Path, cutComment: Boolean = true): Path = {
        mkdir(workDir)
        val outFile = workDir / "amr-annotated.txt"
        val alignmentFile = workDir / "alignments.txt"

        println("Processing with amr_ud")

        processWithCorenlp(
            inAmrFile,
            workDir / "corenlp",
            annotators = Seq("tokenize", "ssplit", "pos", "lemma", "depparse"),
            outputFormat = "conllu")
        val parses = workDir / "corenlp" / "raw.conllu"
        
        // cut out the leading comment in the file
        val cut = if(cutComment) {
            val result = workDir / "input.cut.txt"
            %bash("-c", s"tail -n +3 $inAmrFile > $result")
            result
        }
        else {
            inAmrFile
        }

        docker("xian9426/amr_ud",
            "-p", dockerPath(parses).toString,
            "-a", dockerPath(cut).toString,
            "-o", dockerPath(alignmentFile).toString)
        
        %bash("-c", s"""./amr_ud.sc --corpus $cut --alignments $alignmentFile --output $outFile""")
        
        outFile
    }

    def extractTaggerTestData(inAmrFile: Path, workDir: Path): Path = {
        mkdir(workDir)
        val outFile = workDir / "tags.txt"
        
        println("Extracting tagger test data")
        %bash("-c", s"""./supertaggerExtractTestSentences.sc --corpus $inAmrFile --output $outFile""")
        
        outFile
    }

    def docker(image: String, args: String*): Unit = {
        %docker("run", "--rm", "-v", s"$hostData:/data", image, args)
    }

    /** Extract the raw values of an attribute into a raw file.
    * 
    * The resulting file contains one line per example. Each line contains the
    * value of the selected attribute for each example.
    */
    def extractRawAttribute(inAmrFile: Path, outRawFile: Path, attribute: String = "snt") = {
        %bash("-c", s"""python3 amr_to_raw.py $attribute "$inAmrFile" > "$outRawFile"""")
    }

    def renameAttribute(inAmrFile: Path, outAmrFile: Path, from: String, to: String) = {
        %bash("-c", s"""sed 's/::$from /::$to /' < "$inAmrFile" > "$outAmrFile"""")
    }

    /** Turn a path on the local system into a path inside a docker container.
     *
     * The path must be inside the data directory. It is modified so that it is
     * relative to /data instead, which is where the data directory is mounted
     * inside the docker container.
     */
    def dockerPath(localPath: Path): Path = {
        Path("/data") / localPath.relativeTo(data)
    }

}
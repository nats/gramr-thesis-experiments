#!/usr/bin/env bash
# Usage: process_file.sh <in dir> <out dir> [training|dev|test] <file name>
# Applies the whole preprocessing pipeline to an AMR corpus file.

set -e

base=$1
out=$2
dir=$3
file=$4
work=work/${dir}/${file}
scripts=`dirname $0`

mkdir -p "$work"
mkdir -p "$out/$dir"

source ${scripts}/process_functions.sh

echo "Processing $file"
annotate_corenlp "$base/$dir/$file" "$work/amr_tokenized"
supertag_easyccg "$work/amr_tokenized" "$work/supertagged"
align_tamr_tool "$work/supertagged" "$work/amr_aligned_tamr"
align_jamr "$work/amr_aligned_tamr" "$work/amr_aligned_jamr"
align_isi "$work/amr_aligned_jamr" "$work/amr_aligned_isi"
parse_easyccg "$work/amr_aligned_isi" "$out/$dir/$file"

extract_tagger_test_input "$out/$dir/$file" "$out/$dir/$(basename -s .txt $file).sentences.txt"

#!/usr/bin/env python3

import sys

line = sys.stdin.readline()
while line:
    stripped = line.strip()
    if stripped and not stripped.startswith('#'):
        print('# ::id ' + stripped)
    else:
        print(stripped)
    line = sys.stdin.readline()

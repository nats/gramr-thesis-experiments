#!/usr/bin/env amm

/** Functionality for adding amr_ud alignments to an AMR corpus.
 */

import $file.corpus
import corpus.{Attributes, Example, Corpus}
import $file.ChunkIterator
import ChunkIterator.ChunkIterator

import scala.collection.immutable.SeqMap
import scala.io.Source
import java.io.File
import ammonite.ops._

@doc("Add amr_ud alignments to an AMR corpus.")
@main
def addAmrUdAlignments(
    corpus: Path     @doc("AMR corpus file"),
    alignments: Path @doc("File containing amr_ud output for the corpus"),
    output: Path     @doc("Output path")
): Unit = {
    val examples = Corpus.read(corpus)

    val lineIterator = Source.fromFile(new File(alignments.toString)).getLines
    val chunkIterator = new ChunkIterator(lineIterator)

    val attrExamples = chunkIterator.map(processChunk)
    val extendedExamples = examples.zip(attrExamples).map {
        case (example, attrExample) =>
            example.copy(attributes = Attributes.merge(example.attributes, attrExample.attributes))
    }

    Corpus.write(extendedExamples, output)
}


def processChunk(chunk: Seq[String]): Example = {
    // Drop sentence and header line
    val alignmentLines = chunk.drop(2)
    // Use remaining lines as attribute values
    Example(SeqMap("alignment-amr_ud" -> alignmentLines))
}


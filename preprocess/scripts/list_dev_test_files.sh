#!/usr/bin/env bash
set -e

base=$1

dirs=(dev test)
for dir in ${dirs[@]}; do
  for file in $(ls $base/$dir); do
    echo $dir $file
  done
done

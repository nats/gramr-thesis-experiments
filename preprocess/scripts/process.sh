#!/usr/bin/env bash
set -e

in=$1
out=$2

scripts=`dirname $0`
source "$scripts/config.sh"

mkdir -p work
work=$(readlink -f work)

# Create ISI alignments
${scripts}/concat.sh ${in}
${scripts}/annotate_corenlp.sh ${in}/all.txt ${work}/amr_all_tok tokenize,ssplit ${work}
${scripts}/extract_amr_lines.py ${work}/amr_all_tok > ${work}/amr_lines
${scripts}/amr_to_raw.py tok ${work}/amr_all_tok > ${work}/snt_lines
pushd ${ISI_ALIGNER}
echo "AMR=${work}/amr_lines" > addresses.keep
echo "ENG=${work}/snt_lines" >> addresses.keep
echo "MGIZA_SCRIPT=$MGIZA/mgizapp/scripts" >> addresses.keep
echo "MGIZA_BIN=$MGIZA/mgizapp/bin" >> addresses.keep
./run.sh
popd
mv ${ISI_ALIGNER}/Alignments.keep ${work}/
${scripts}/alignments_to_attr.py ${work}/amr_all_tok work/Alignments.keep > ${work}/isi_attr

# Convert tamr alignemnts
${scripts}/convert_tamr_alignments.py < /tamr_alignment > ${work}/tamr_alignment_converted

# Annotate each individual file

for file in ${in}/training/amr-*.txt; do
    ${scripts}/process_training_file.sh ${in} ${out} training $(basename ${file})
done

for file in ${in}/dev/amr-*.txt; do
    ${scripts}/process_dev_test_file.sh ${in} ${out} dev $(basename ${file})
done

for file in $in/test/amr-*.txt; do
    ${scripts}/process_dev_test_file.sh ${in} ${out} test $(basename ${file})
done

${scripts}/concat.sh ${out}


class ChunkIterator(val lineIterator: Iterator[String]) extends Iterator[Vector[String]] {

    var nextChunk: Option[Vector[String]] = readChunk()
    
    private def readChunk(): Option[Vector[String]] = {
        var chunk: Vector[String] = Vector()

        var line: String = ""
        // Eat all empty lines at the beginning
        while(line.trim.isEmpty && lineIterator.hasNext) {
            line = lineIterator.next
        }

        while(line.trim.nonEmpty) {
            chunk = chunk :+ line
            line = if(lineIterator.hasNext) lineIterator.next else ""
        }

        // The last line is guaranteed to be empty and therefore rightly swallowed

        if(chunk.nonEmpty) Some(chunk) else None
    }

    override def hasNext: Boolean = nextChunk.nonEmpty

    override def next: Vector[String] = {
        nextChunk match {
            case Some(chunk) =>
                nextChunk = readChunk()
                chunk
            case None => throw new NoSuchElementException()
        }
    }

}

#!/usr/bin/env bash

in=$1
dirs=(training dev test)
for dir in ${dirs[@]}
do
    cat $in/$dir/amr-*.txt > $in/$dir/all-$dir.txt 2>/dev/null
done

cat $in/*/all-*.txt > $in/all.txt 2>/dev/null

function annotate_spacy {
  infile=$1
  outfile=$2
  "$scripts/add_spacy_annotations.py" "$infile" > "$outfile"
}

function annotate_corenlp {
  echo Running CoreNLP pipeline on $1
  "$scripts/annotate_corenlp.sh" "$1" "$2" tokenize,ssplit,pos,lemma,ner "$work"
}

function align_jamr {
  infile=$1
  outfile=$2
  echo Adding jamr alignments to $1
  . "$JAMR/scripts/config.sh"
  java -cp "$JAMR/target/scala-2.10/jamr-assembly-0.1-SNAPSHOT.jar" edu.cmu.lti.nlp.amr.Aligner -v 0 < "$infile" | sed 's/::alignments /::alignments-jamr /' > "$outfile"

}

function align_isi {
  infile=$1
  outfile=$2
  echo Adding isi alignments to $1
  "$scripts/amr_add_attribute.py" work/isi_attr alignments-isi < "$infile" > "$outfile"
}

function align_tamr_release {
  infile=$1
  outfile=$2
  echo Adding released tamr alignments to $1
  "$scripts/amr_add_attribute.py" work/tamr_alignment_converted alignments-tamr < "$infile" > "$outfile"
}

function align_tamr_tool {
  infile=$1
  outfile=$2
  echo Aligning $1 with jamr for tamr
  . "$JAMR/scripts/config.sh"
  java -cp "$JAMR/target/scala-2.10/jamr-assembly-0.1-SNAPSHOT.jar" edu.cmu.lti.nlp.amr.Aligner -v 0 --print-nodes-and-edges < "$infile" > "$work/jamr-for-tamr"
  echo Adding tamr alignments to $1
  python /tamr/amr_aligner/rule_base_align.py \
    -verbose \
    -data \
    "$work/jamr-for-tamr" \
    -output \
    "$work/tamr_alignment" \
    -wordvec \
    "/tamr/glove_filtered.w2v" \
    -trials \
    10000 \
    -improve_perfect \
    -morpho_match \
    -semantic_match
  "$scripts/amr_add_attribute.py" "$work/tamr_alignment" alignments-tamr < "$infile" > "$outfile"
}

function supertag_easyccg {
  infile=$1
  outfile=$2
  echo Running EasyCCG tagger on $1
  "$scripts/amr_to_raw.py" tok "$infile" > "$work/raw.tok"
  "$scripts/add_easyccg_tags.sc" "$work/raw.tok" > "$work/easyccg_tags"
  "$scripts/amr_add_sequenced_attributes.py" "$work/easyccg_tags" < "$infile" > "$outfile"
}

function parse_easyccg {
  infile=$1
  outfile=$2
  echo Running EasyCCG parser on $1
  "$scripts/amr_to_raw.py" tok "$infile" > "$work/raw.tok"
  java -Xmx8g -jar "$EASYCCG/easyccg.jar" -m "$EASYCCG/model_rebank" -f "$work/raw.tok" -l 50 -i tokenized --supertaggerbeam 0.0001 --nbest 50 > "$work/auto" 2>/dev/null
  "$scripts/amr_add_derivations.py" "$infile" "$work/auto" > "$outfile"
}

function extract_tagger_test_input {
  infile=$1
  outfile=$2
  echo Extracting Tagger Test Input from $1 to $2
  java -Xmx4g -jar "$GRAMR" "${scripts}/supertaggerExtractTestSentences.sc" --test "${infile}" --out "${outfile}"
}

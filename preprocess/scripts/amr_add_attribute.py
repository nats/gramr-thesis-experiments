#!/usr/bin/env python3

import sys, re

if(len(sys.argv) != 3):
    print("Usage: amr_add_attribute.py <attribute file> <attribute name> < <amr corpus file>")
    exit(1)

attrFileName = sys.argv[1]
attrName = sys.argv[2]

attrFile = open(attrFileName)
attrLines = [line.strip() for line in attrFile.readlines() if len(line.strip()) > 0]
attrFile.close()

# Collect all the attributes and make them accessible by sentence ID.
attributes = {}

idRe = re.compile(r'::id ([^\s]+)')
attrRe = re.compile(r'# ::[a-z\-]+ (.*)$')

for i in range(0, len(attrLines), 2):
    (idLine, attrLine) = attrLines[i:i+2]
    idMatch = idRe.search(idLine)
    attrMatch = attrRe.match(attrLine)
    if not attrMatch:
        print('Invalid attribute line {0}'.format(attrLine), file=sys.stderr)
        exit(1)
    if idMatch:
        ident = idMatch.group(1)
    else:
        ident = idLine
    attr = attrMatch.group(1)
    attributes[ident] = attr

def print_attr(ident):
    print('# ::{0} {1}'.format(attrName, attributes[ident]))

lastId = None
line = sys.stdin.readline()
wasInComment = True
while line:
    if line.startswith("#"):
        m = idRe.search(line)
        if m:
            lastId = m.group(1)
        wasInComment = True
    else:
        if wasInComment and lastId != None:
            # Insert the attributes at the end of the comments section
            print_attr(lastId)
        wasInComment = False
    print(line, end="")
    line = sys.stdin.readline()




# Preprocessing for parsing experiments

Preprocesses data for use by the parser. Uses various tools to add annotations
required by the parser and for evaluation.

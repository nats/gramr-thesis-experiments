#!/usr/bin/env python3

import sys, re

if(len(sys.argv) != 3):
    print("Usage: amr_add_derivations.py <amr corpus file> <auto file>")
    exit(1)

amrFileName = sys.argv[1]
autoFileName = sys.argv[2]

autoFile = open(autoFileName)
autoLines = [line for line in autoFile.readlines() if len(line.strip()) > 0]
autoFile.close()

# Collect all the attributes and derivations and make them accessible by
# sentence ID.
attributes = {}

def add_entry(attrDict):
    key = "nw." + attrDict["ID"]
    if key not in attributes:
        attributes[key] = []
    attributes[key] = attributes[key] + [attrDict]

def print_attr(id):
    for attrDict in attributes[id]:
        if "N" in attrDict:
            print("# ::ccg-derivation-{0} {1}".format(attrDict["N"], attrDict["derivation"]))
            print("# ::ccg-correct-deps-{0} {1} ::ccg-retrieved-deps-{0} {2} ::ccg-total-deps-{0} {3}".format(attrDict["N"], attrDict["CORRECT"], attrDict["RETRIEVED"], attrDict["TOTAL"]))
            print("# ::ccg-deps-precision-{0} {1} ::ccg-deps-recall-{0} {2} ::ccg-deps-f1-{0} {3}".format(attrDict["N"], attrDict["P"], attrDict["R"], attrDict["F"]))
        elif "PARSER" in attrDict and attrDict["PARSER"] == "GOLD":
            print("# ::ccg-derivation-gold {0}".format(attrDict["derivation"]))

for i in range(0, len(autoLines), 2):
    (infoLine, derLine) = autoLines[i:i+2]
    attrDict = {}
    for attr in infoLine.split():
        (name, value) = attr.split("=")
        attrDict[name] = value
    attrDict["derivation"] = derLine.strip()
    add_entry(attrDict)

# Add the attributes to the AMR file
amrFile = open(amrFileName)

lastId = None
line = amrFile.readline()
idRe = re.compile(r"::id ([^\s]+)")
wasInComment = True
while line:
    if line.startswith("#"):
        m = idRe.search(line)
        if m:
            lastId = m.group(1)
        wasInComment = True
    else:
        if wasInComment and lastId != None:
            # Insert the attributes at the end of the comments section
            print_attr(lastId)
        wasInComment = False
    print(line, end="")
    line = amrFile.readline()

amrFile.close()
        

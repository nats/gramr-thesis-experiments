#!/usr/bin/env bash

# Prepare the consensus-dev corpus for experiments with CCGBank derivations
#
# Usage: prepare-consensus.sh HOST_DATA
#
#     HOST_DATA: Path to the data directory on the Docker host

set -euo pipefail

# Path to the data directory on the docker host
HOST_DATA=$1

CCGBANK=${DATA}/ccgbank_1_1/data/AUTO

# Command for running a docker image
dockerrun="docker run --rm -v ${HOST_DATA}:/data"
# Docker image names of tools used by this script
easyccg_image=xian9426/easyccg
jamr_image=xian9426/jamr
tamr_image=xian9426/tamr
isialigner_image=xian9426/isialigner
corenlp_image=xian9426/corenlp
amr_ud_image=xian9426/amr_ud

INFILE="${DATA}/amr_anno_1.0/data/split/dev/amr-release-1.0-dev-consensus.txt"
OUTDIR="${DATA}/processed/consensus"
mkdir -p "${OUTDIR}"
cd "${OUTDIR}"

GOLD_AUTOFILE=${OUTDIR}/consensus-dev.gold.auto
CLEAN_GOLD_AUTOFILE=${OUTDIR}/consensus-dev.gold_clean.auto
PIPEFILE=${OUTDIR}/consensus-dev.pipe
RAWFILE=${OUTDIR}/consensus-dev.raw

EASYCCG_AUTOFILE=${OUTDIR}/consensus-dev.easyccg.auto
EVAL_AUTOFILE=${OUTDIR}/consensus-dev.eval.auto

SCRIPTS=/scripts
CANDC=/candc/candc
CANDC_SCRIPTS=${CANDC}/src/scripts/ccg

# Assemble CCG derivations for the consensus-dev sentences
cat ${CCGBANK}/00/wsj_0001.auto > ${GOLD_AUTOFILE}
cat ${CCGBANK}/00/wsj_0002.auto >> ${GOLD_AUTOFILE}
cat ${CCGBANK}/00/wsj_0003.auto >> ${GOLD_AUTOFILE}
cat ${CCGBANK}/00/wsj_0004.auto >> ${GOLD_AUTOFILE}
cat ${CCGBANK}/00/wsj_0005.auto >> ${GOLD_AUTOFILE}
tail -n +5 ${CCGBANK}/00/wsj_0006.auto >> ${GOLD_AUTOFILE}
cat ${CCGBANK}/00/wsj_0007.auto >> ${GOLD_AUTOFILE}
cat ${CCGBANK}/00/wsj_0008.auto >> ${GOLD_AUTOFILE}
cat ${CCGBANK}/00/wsj_0009.auto >> ${GOLD_AUTOFILE}
cat ${CCGBANK}/00/wsj_0010.auto >> ${GOLD_AUTOFILE}
cat ${CCGBANK}/00/wsj_0011.auto >> ${GOLD_AUTOFILE}
head -n 10 ${CCGBANK}/00/wsj_0012.auto >> ${GOLD_AUTOFILE}

# Convert CCG parses to pipe format for evaluation
${CANDC_SCRIPTS}/convert_auto.sh ${GOLD_AUTOFILE} | sed -f ${CANDC_SCRIPTS}/convert_brackets.sh > ${PIPEFILE}
python ${CANDC_SCRIPTS}/extract_sequences.py -w ${PIPEFILE} > ${RAWFILE}

# Clean dependency indices from gold.auto
sed -r 's/_[0-9]+(:B)?([^0-9.])/\2/g' ${GOLD_AUTOFILE} | \
sed -r 's/(\[\w+\])(\[\w+\])/\2/g'  > ${CLEAN_GOLD_AUTOFILE}

# Copy sents from gold.raw to AMR file
# This is because the tokenisation is different between AMR and CCGbank.
${SCRIPTS}/replace_snts.py ${RAWFILE} ${INFILE} | tail -n +3 > ${OUTDIR}/consensus-dev.tok.txt

# Parse with EasyCCG
${dockerrun} ${easyccg_image} ${RAWFILE} ${OUTDIR}/easyccg.tmp.auto -l 200 -i tokenized --supertaggerbeam 0.00001 --nbest 10
${SCRIPTS}/annotate_sentence_ids.py ${GOLD_AUTOFILE} ${OUTDIR}/easyccg.tmp.auto > ${EASYCCG_AUTOFILE}

# Evaluate EasyCCG parses
mkdir -p ${OUTDIR}/sentencewise
cd ${OUTDIR}/sentencewise
${SCRIPTS}/split_sents.py ${GOLD_AUTOFILE} gold
${SCRIPTS}/split_sents.py ${EASYCCG_AUTOFILE} test

for f in *.auto; do
${CANDC_SCRIPTS}/get_deps_from_auto $f $f.deps
done

CANDC=${CANDC} ${SCRIPTS}/eval_sentencewise.sh > ${EVAL_AUTOFILE}
cd ${OUTDIR}

# Add gold and EasyCCG annotations to the corpus
${SCRIPTS}/amr_add_derivations.py ${OUTDIR}/consensus-dev.tok.txt ${EVAL_AUTOFILE} > ${OUTDIR}/consensus-dev.parsed.txt
${SCRIPTS}/amr_add_derivations.py ${OUTDIR}/consensus-dev.parsed.txt ${CLEAN_GOLD_AUTOFILE} > ${OUTDIR}/consensus-dev.goldsyn.txt

# Align with jamr
${dockerrun} ${jamr_image} ${OUTDIR}/consensus-dev.goldsyn.txt ${OUTDIR}/consensus-dev.jamr.txt -v 0 --print-nodes-and-edges
sed 's/::alignments/::jamr-alignments/' < ${OUTDIR}/consensus-dev.jamr.txt > ${OUTDIR}/consensus-dev.jamr-renamed.txt

# Align with tamr

${dockerrun} ${tamr_image} \
  -verbose \
  -data \
  "${OUTDIR}/consensus-dev.jamr.txt" \
  -output \
  "${OUTDIR}/tamr-alignment.txt" \
  -wordvec \
  "/tamr/glove_filtered.w2v" \
  -trials \
  10000 \
  -improve_perfect \
  -morpho_match \
  -semantic_match
"${SCRIPTS}/amr_add_attribute.py" "${OUTDIR}/tamr-alignment.txt" alignments-tamr < "${OUTDIR}/consensus-dev.jamr-renamed.txt" > "${OUTDIR}/consensus-dev.jamr+tamr.txt"


# Align with amr_ud

mkdir -p "${DATA}/work/amr_ud"

tail -n +3 "${RAWFILE}" > "${DATA}/work/amr_ud/consensus-dev.raw"

${dockerrun} ${corenlp_image} \
  -annotators tokenize,ssplit,pos,lemma,depparse \
  -ssplit.eolonly true \
  -outputFormat conllu \
  -file "${DATA}/work/amr_ud/consensus-dev.raw" \
  -outputDirectory "${DATA}/work/amr_ud"

${dockerrun} ${amr_ud_image} \
  -p "${DATA}/work/amr_ud/consensus-dev.raw.conllu" \
  -a "${OUTDIR}/consensus-dev.jamr-renamed.txt" \
  -o "${DATA}/work/amr_ud/alignments.txt"

"${SCRIPTS}/amr_ud.sc" --corpus "${OUTDIR}/consensus-dev.jamr+tamr.txt" --alignments "${DATA}/work/amr_ud/alignments.txt" --output "${OUTDIR}/consensus-dev.jamr+tamr+amr_ud.txt"
        

# Align with isi
"${SCRIPTS}/concat.sh" "${DATA}/amr_anno_1.0/data/split"
all_training="${DATA}/amr_anno_1.0/data/split/training/all-training.txt"
${dockerrun} ${corenlp_image} \
  -annotators tokenize,ssplit \
  -ssplit.eolonly true \
  -outputFormat conll \
  -file "$all_training" \
  -outputDirectory "${DATA}/work/isialign"

train_amr_annotated="${DATA}/work/isialign/all-training-annotated.txt"
"${SCRIPTS}/add_corenlp_annotations.py" "$all_training" "${DATA}/work/isialign/raw.conll" > ${train_amr_annotated}

BIGAMR="${DATA}/work/isialign/amr-annotated.txt"

cat "$train_amr_annotated" "${OUTDIR}/consensus-dev.tok.txt" > "$BIGAMR"

${dockerrun} ${isialigner_image} "$BIGAMR" "${OUTDIR}/Alignments.keep"
${SCRIPTS}/alignments_to_attr.py "${BIGAMR}" "${OUTDIR}/Alignments.keep" > "${OUTDIR}/isialignments.txt"

${SCRIPTS}/amr_add_attribute.py ${OUTDIR}/isialignments.txt alignments-isi < "${OUTDIR}/consensus-dev.jamr+tamr+amr_ud.txt" > "${OUTDIR}/consensus-dev.jamr+tamr+amr_ud+isi.txt"
${SCRIPTS}/amr_add_attribute.py ${SCRIPTS}/gold_alignments.txt alignments-gold < "${OUTDIR}/consensus-dev.jamr+tamr+amr_ud+isi.txt" > "${OUTDIR}/consensus-dev.annotated.txt"

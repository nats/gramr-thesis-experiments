#!/usr/bin/env python3

import re, sys

if(len(sys.argv) != 3):
    print("Splits an auto file into individual sentence files.", file=sys.stderr)
    print("Usage: split_sents <auto_file> <prefix>", file=sys.stderr)
    print("Will create <prefix>.<id>.<no> files.", file=sys.stderr)

inputFileName = sys.argv[1]
outputPrefix = sys.argv[2]

idRe = re.compile(r"ID=([^\s]+)")
def extract_id(line):
    m = idRe.search(line)
    if m:
        return m.group(1)
    else:
        None

inputFile = open(inputFileName)
inputLines = inputFile.readlines()
inputFile.close

idCounts = {}
def get_count(id):
    if id not in idCounts:
        idCounts[id] = 0
    result = idCounts[id]
    idCounts[id] = result + 1
    return result

while len(inputLines) > 1:
    entry = inputLines[0:2]
    inputLines = inputLines[2:]
    if(entry[0]):
        id = extract_id(entry[0])
        count = get_count(id)
        outFileName = "{0}-{1}-{2}.auto".format(outputPrefix, id, count)
        outFile = open(outFileName, "w")
        for line in entry:
            outFile.write(line)
        outFile.close()

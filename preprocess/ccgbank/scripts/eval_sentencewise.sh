#!/usr/bin/env bash

SCRIPTS=$CANDC/src/scripts/ccg

for f in test*.deps; do
    re=".*test-(.*)-([0-9]+)"
    if [[ $f =~ $re ]]; then
        # Find the corresponding gold file
        id="${BASH_REMATCH[1]}"
        no="${BASH_REMATCH[2]}"
        g="gold-$id-0.auto.deps"

        # Get the evaluation scores
        mapfile -t result < <($SCRIPTS/evaluate3 $g $f | egrep '^(up|ur|uf)')
        pct_re="([0-9]+\.[0-9]+)%"
        of_re="([0-9]+) of ([0-9]+)"

        if [[ ${result[0]} =~ $pct_re ]]; then
            prec="${BASH_REMATCH[1]}"
        fi
        if [[ ${result[1]} =~ $pct_re ]]; then
            rec="${BASH_REMATCH[1]}"
        fi
        if [[ ${result[2]} =~ $pct_re ]]; then
            fscore="${BASH_REMATCH[1]}"
        fi

        # precision line, "correct of retrieved"
        if [[ ${result[0]} =~ $of_re ]]; then
            correct="${BASH_REMATCH[1]}"
            retrieved="${BASH_REMATCH[2]}"
        fi
        # recall line, "correct of total"
        if [[ ${result[1]} =~ $of_re ]]; then
            total="${BASH_REMATCH[2]}"
        fi

        autofile="test-$id-$no.auto"
        mapfile -t lines < $autofile
        echo "${lines[0]} N=$no CORRECT=$correct RETRIEVED=$retrieved TOTAL=$total P=$prec R=$rec F=$fscore"
        echo "${lines[1]}"
    fi
done

#!/usr/bin/env python3

import re, sys

if(len(sys.argv) != 3):
    print("Copies the sentence ids from an auto file to another auto file with multiple parses per sentence.", file=sys.stderr)
    print("Usage: annotate_sentence_ids <gold auto file> <test auto file>", file=sys.stderr)
    exit(1)

goldFileName = sys.argv[1]
testFileName = sys.argv[2]

idRe = re.compile(r"ID=([^\s]+)")
def extract_id(line):
    m = idRe.search(line)
    return m.group(1)

goldFile = open(goldFileName)
goldLines = goldFile.readlines()
goldFile.close()

idLines = goldLines[0:-1:2]
ids = [extract_id(x) for x in idLines]

def get_id(sentenceNum):
    return ids[int(sentenceNum) - 1]

testFile = open(testFileName)
testLines = testFile.readlines()
testFile.close()

def replace_id(line):
    m = idRe.search(line)
    if m:
        matchNum = m.group(1)
        id = get_id(matchNum)
        return idRe.sub("ID={0}".format(id), line)
    else:
        return line

outLines = [replace_id(x) for x in testLines]

for line in outLines:
    print(line, end="")

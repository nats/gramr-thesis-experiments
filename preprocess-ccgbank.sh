#!/usr/bin/env bash
set -euxo pipefail

base="$(realpath $(dirname $0))"
host_data="$base/data"

"$base/build.sh" preprocess-ccgbank
docker-compose run --rm preprocess-ccgbank "$host_data" $@

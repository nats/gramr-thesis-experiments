#!/usr/bin/env bash
set -eou pipefail

export COMPOSE_DOCKER_CLI_BUILD=1
export DOCKER_BUILDKIT=1

if [[ $# == 0 ]]; then
    # Images need to be built in a specific order, which is specified here
    images=(candc easyccg easyccg_tagger corenlp jamr tamr gramr preprocess-parsing preprocess-ccgbank)
else
    images=("$@")
fi

# Parallel building isn't safe because of image interdependencies
# docker-compose build --parallel "${images[@]}"

for image in ${images[@]}; do
    docker-compose build ${image}
done
